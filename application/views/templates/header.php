<!doctype html>
<html class="no-js" lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Accès extranet</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" href="icon.png">
  <link rel="stylesheet" href="<?php echo SCSS_DIR; ?>webapp.css">
  <link rel="stylesheet" href="<?php echo CSS_DIR; ?>bootstrap.css">
  <?php if(isset($css_to_load) && $css_to_load != ""){ ?>
  <link rel="stylesheet" href="<?php echo CSS_DIR.$css_to_load; ?>">
  <?php } ?>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:300,400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <script>
  var base_url = '<?php echo base_url(); ?>';  
</script>
</head>
<body>

	<header>
		<div id="toggle-menu"></div>
		<a href="" class="logo-bg"><img class="img-fluid" src="<?php echo IMG_DIR; ?>mysecuricom.png" /></a>

		<?php if(isset($_SESSION['is_logged'])){ ?>
			<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'supercommercial' && isset($credit_sms)){ ?>
				<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
			<?php } ?>
			<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'technique' && isset($credit_sms)){ ?>
				<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
			<?php } ?>
			<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'commercial' && isset($credit_sms)){ ?>
				<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
			<?php } ?>
			<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'commercial_tsip' && isset($credit_sms)){ ?>
				<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
			<?php } ?>
			<a class="logout" href="<?php echo site_url('login/logout') ?>"><i class="fas fa-sign-out-alt"></i></a>
		<?php } ?>
	</header>

<!--    <header>
        <div id="mod-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 center">

                    </div>
                </div>
            </div>
        </div>
        <div class="container top-content">
          <div class="row">
                <div class="col-lg-12 center">
                    <?php if(isset($_SESSION['societe']) && $_SESSION['societe'] == 'tsip'){?>
                    <img style="padding:0 0 1rem 0;" class="img-fluid" src="<?php echo IMG_DIR; ?>tsip.png" />
                    <?php }else{ ?>
                    <img class="img-fluid" src="<?php echo IMG_DIR; ?>securicom_min.png" />
                    <?php } ?>
                    <h1 class="mini-spacer-top-bot">Extranet <?php if(isset($_SESSION['role'])){ echo 'accès '.$_SESSION['role']; }?></h1>
                </div>
                <div class="col-lg-12">
                <?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'technique'){ ?>
                            <a class="link-header" href="<?php echo site_url('technique/') ?>"><i class="fas fa-home"></i>Accueil</a>
                            <a class="link-header" href="<?php echo site_url('technique/interventions') ?>"><i class="far fa-clock"></i>Saisie des interventions</a>
                            <a class="link-header" href="<?php echo site_url('technique/cumuls') ?>"><i class="fas fa-chart-bar"></i>Cumuls des interventions</a>
                            <a class="link-header" href="<?php echo site_url('technique/techniciens') ?>"><i class="far fa-user-circle"></i>Techniciens</a>
                            
                <?php } ?>
                            <?php if(isset($_SESSION['is_logged'])){ ?>
                                <a class="link-header" href="<?php echo site_url('login/logout') ?>"><i class="fas fa-sign-out-alt"></i>Se déconnecter</a>
                            <?php } ?>
                </div>
            </div>
        </div>
    </header>-->
    <div class="body-content">
