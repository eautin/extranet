
</div><!--body-content-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 mentions">
                <p>2018 - 2023 <br/>https://extranet.securicom-telesurveillance.fr</p>
            </div>
        </div>
    </div>
</footer>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<?php if(isset($js_to_load) && $js_to_load != ''){ ?>

<script src="<?php echo JS_DIR.$js_to_load;?>"></script>
        
<?php } ?>

