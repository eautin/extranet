<?php
   // $url1=$_SERVER['REQUEST_URI'];
   // header("Refresh: 5; URL=$url1");
?>
<div class="container">
    <div class="row">
<?php foreach($infos_leads as $key => $value) { ?>
        <div class="col-md-3 col-12 lead-box">
            <div class="link-cub">
                <h2><?php echo $value['commercial_name']; ?></h2>
                <h3 class="alert alert-success">Devis signés : <strong><?php echo $value['devis_signe'] ?></strong></h3>
                <h3 class="alert alert-success">Devis en cours : <strong><?php echo $value['devis_en_cours'] ?></strong></h3>
                <h3 class="alert alert-danger">Demande non traitée : <strong><?php echo $value['a_traiter'] ?></strong></h3>
                <h3 class="alert alert-info">Devis sans suite : <strong><?php echo $value['devis_ne_donne_pas_suite'] ?></strong></h3>
                <h3 class="alert alert-info">Rappel sans suite : <strong><?php echo $value['rappele_ne_donne_pas_suite'] ?></strong></h3>
                <h3 class="alert alert-info">RDV sans suite : <strong><?php echo $value['rdv_ne_donne_pas_suite'] ?></strong></h3>
            </div>
        </div> 
<?php } ?>
    </div>
    <div class="row">
        <div class="col-12">
            <a class="link-cub" href="<?php echo site_url();?>/<?php echo $_SESSION['user']->role;?>"><span class="fa fa-home"></span>Retour accueil</a>
        </div>
    </div>
</div>


