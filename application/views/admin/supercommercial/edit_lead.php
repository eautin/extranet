<div class="container page">
    <?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'supercommercial') { ?>
    <div class="row">
        <div class="col-lg-12">
            <h1>Editer le lead <?php echo $lead->id;?></h1>
        </div>


		<?php

		$prospect_name = isset($smsdata['name']) ? $smsdata['name'] : NULL;
		$prospect_tel = isset($smsdata['tel']) ? $smsdata['tel'] : NULL;
		$prospect_cp = isset($smsdata['cp']) ? $smsdata['cp'] : NULL;
		$prospect_mail = isset($smsdata['email']) ? $smsdata['email'] : NULL;
		$prospect_city = isset($smsdata['city']) ? $smsdata['city'] : NULL;
		$prospect_street = isset($smsdata['street']) ? $smsdata['street'] : NULL;

		?>

        <div class="col-12">
            <div class="bg-gris">

				<div class="form-group">
					<p><?php echo $lead->informations; ?></p>
				</div>
				<div class="form-group alert-error">

				</div>
                    <?php echo form_open('supercommercial/update_lead/'.$lead->type); ?>

				<?php if(validation_errors() != ""){?>
				<div class="form-group alert-danger p-2">
					<?php echo validation_errors(); ?>
				</div>
				<?php } ?>
				<div class="row">
				<div class="col-6">
                    <div class="form-group">
                         <label for="statut">Statut actuel du lead</label>
                        <?php echo form_dropdown('statut', $status_lead, $status_id->id,'class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label for="assign_commercial">Commercial assigné au lead</label>
                        <?php echo form_dropdown('commercial', $commerciaux, $lead->commercial_id,'class="form-control"'); ?>
                    </div>

					<div class="form-group">
						<label>Nom du prospect</label>
						<input type="text" class="form-control" name="prospect_name" value="<?php echo $prospect_name;?>" />
					</div>
					<div class="form-group">
						<label>Tel du prospect</label>
						<input type="tel" class="form-control" name="prospect_tel" value="<?php echo $prospect_tel;?>" />
					</div>
					<div class="form-group">
						<label>Email du prospect</label>
						<input type="email" class="form-control" name="prospect_email" value="<?php echo $prospect_mail;?>" />
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label>Ville du prospect</label>
						<input type="text" class="form-control" name="prospect_city" value="<?php echo $prospect_city;?>" />
					</div>
					<div class="form-group">
						<label>Rue du prospect</label>
						<input type="text" class="form-control" name="prospect_street" value="<?php echo $prospect_street;?>" />
					</div>
					<div class="form-group">
						<label>Code postal</label>
						<input type="text" class="form-control" name="prospect_cp" value="<?php echo $prospect_cp;?>" />
					</div>
					<div class="form-group">
						<label id="labelDatePicker" for="date_picker">Date et heure du RDV</label>
						<input id="datetimepicker" name="date_picker" type="text" class="form-control" value="<?php echo $lead->date_rdv_confirme;?>" placeholder=""/>
					</div>
					<div class="form-group">
						<label for="adduser_prename">Notes</label>
						<textarea class="form-control form-control-lg" name="commentaire"><?php echo $lead->commentaire;?></textarea>
					</div>
					<div class="form-group">
						<?php
						echo form_checkbox('send_email_commercial', 'accept', TRUE);
						echo form_label('Envoyer une notification mail au commercial', 'send_email_commercial');
						?>
					</div>
					<div class="form-group">
						<?php
						echo form_checkbox('send_sms_commercial', 'accept', TRUE);
						echo form_label('Envoyer une notification SMS au commercial', 'send_sms_commercial');
						?>
					</div>
					<div class="form-group">
						<?php
						echo form_checkbox('send_email_administratif', 'accept', TRUE);
						echo form_label('Envoyer une notification mail assistante commercial', 'send_mail_administratif');
						?>
					</div>
                    <?php echo form_hidden('id',$lead->id); ?>
                    <?php echo form_hidden('type',$lead->type); ?>
				</div>
				</div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Mettre à jour et assigner" class="btn btn-success btn-lg"/>
                        <input type="hidden" name="type_lead" value="<?php echo $lead->type; ?>"/>
                        <input type="hidden" name="id_lead" value="<?php echo $lead->id; ?>"/>
                    </div>

                    <?php  echo form_close(); ?>
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/supercommercial/'); ?>"><i class="fas fa-arrow-left"></i>Retour</a>
            </div>
                </div>
        </div>
    </div>
     <?php }else{ ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="alert alert-danger">
                <p>Accès non autorisé.</p>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

