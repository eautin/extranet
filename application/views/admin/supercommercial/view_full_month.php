<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Chiffre <?php echo $month." ".$year;?></h1>
            <div class="bg-gris" id="secu">
                <div class="row">
                    <div class="col-3">
                        <h2>Nom</h2>
                    </div>
                    <div class="col-3">
                        <h2>chiffre location</h2>
                    </div>
                    <div class="col-3">
                        <h2>chiffre vente</h2>
                    </div>
                    <div class="col-3">
                        <h2>Nombre de signature</h2>
                    </div>
                </div>
                <?php foreach ($commerciaux as $com) { ?>
                
                <?php //var_dump($com); ?>
                
                <form>
                <div class="row" data-id="<?php echo $com->id; ?>">
                    
                    <div class="col-3">  
                        <?php echo $com->prenom.' '.$com->nom; ?>   
                    </div>
                    
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" value="<?php echo $com->total_loc; ?>" name="total_loc" class="form-control" placeholder="Chiffre location">
                        </div>
                           
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" value="<?php echo $com->total_vente; ?>" name="total_vente" class="form-control" placeholder="Chiffre vente">
                        </div>
                    </div>
                    <div class="col-3">
                         <div class="form-group">
                            <input type="text" value="<?php echo $com->total_devis; ?>" name="total_devis" class="form-control" placeholder="Total devis signé">
                        </div>
                        <div class="form-group">
                             <input type="hidden" value="<?php echo $com->id;?>" name="id_com">
                        </div>
                    </div>
                    
                </div>
                </form>
                <?php } ?>
                <input type="hidden" value="<?php echo $date_time;?>" name="month_year">
                <a href="#" class="btn btn-success btn-lg" id="save_ca_month" class="form-control">Valider</a>
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-success" style="margin-top:1rem;">
                            <p></p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="alert alert-danger">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-12">
            <div class="bg-gris" style="margin-top:2rem;" id="tsip">
             <?php foreach ($tsip_commercial as $com) { ?>
                <form>
                <div class="row" data-id="<?php echo $com->id; ?>">
                    
                    <div class="col-3">  
                        <?php echo $com->prenom.' '.$com->nom; ?>   
                    </div>
                    
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" value="<?php echo $com->total_loc; ?>" name="total_loc" class="form-control" placeholder="Chiffre location">
                        </div>
                           
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" value="<?php echo $com->total_vente; ?>" name="total_vente" class="form-control" placeholder="Chiffre vente">
                        </div>
                    </div>
                    <div class="col-3">
                         <div class="form-group">
                            <input type="text" value="<?php echo $com->total_devis; ?>" name="total_devis" class="form-control" placeholder="Total devis signé">
                        </div>
                        <div class="form-group">
                             <input type="hidden" value="<?php echo $com->id;?>" name="id_com">
                        </div>
                    </div>
                    
                </div>
                </form>
             <?php } ?>
                <input type="hidden" value="<?php echo $date_time;?>" name="month_year">
                <a href="#" class="btn btn-success btn-lg" id="save_ca_month_tsip" class="form-control">Valider</a>
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-success" style="margin-top:1rem;">
                            <p></p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="alert alert-danger">
                            <p></p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>-->
    <div class="row">
        <div class="col-12">
            <a class="link-cub" href="<?php echo site_url();?>/<?php echo $_SESSION['user']->role;?>"><span class="fa fa-home"></span>Retour accueil</a>
        </div>
    </div>
</div>
