<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 col-12">
            <h1>Saisie / edition CA mensuel commerciaux</h1>
            <div class="bg-gris">
            <?php echo form_open('supercommercial/select_month_year/'); ?>
            <h4>Année</h4>
                <?php
                $options = array(
                    
                    '2019' => '2019',
                    '2020' => '2020',
                    '2021' => '2021',
                    '2022' => '2022',
                    '2023' => '2023'
                );
                
                echo form_dropdown('year',$options,$current_year,'class="form-control"');
                ?>
            <h4>Mois</h4>
            <?php 
            
            $options = array(
              
                '01' => 'Janvier',
                '02' => 'Février',
                '03' => 'Mars',
                '04' => 'Avril',
                '05' => 'Mai',
                '06' => 'Juin',
                '07' => 'Juillet',
                '08' => 'Août',
                '09' => 'Septembre',
                '10' => 'Octobre',
                '11' => 'Novembre',
                '12' => 'Décembre'
                
            );
            echo form_dropdown('month',$options,$current_month,'class="form-control"');
            ?>
            <div class="form-group" style="margin-top:1rem;">
                <input type="submit" name="submit" value="Valider" class="btn btn-success btn-lg"/>
                <?php echo validation_errors(); echo form_close(); ?>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url();?><?php echo $_SESSION['user']->role;?>"><span class="fa fa-home"></span>Retour accueil</a>
        </div>
    </div>
</div>