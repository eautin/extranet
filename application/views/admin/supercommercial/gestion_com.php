<div class="container">
<div class="row">
        <div class="col-12">
            <?php echo form_open('supercommercial/on_off_commercial/'); ?>
            <h1>Activer / Désactiver commercial</h1>
            <div class="bg-gris">
                <?php foreach ( $commerciaux as $commercial ){ ?>
                
                <h2><?php echo $commercial->nom." ".$commercial->prenom; ?></h2>
                <?php 
                
                if($commercial->active === "1"){
                   $is_active = TRUE;
                }else{
                    $is_active = FALSE;
                }
                
                echo form_label('Actif', $commercial->id);
                echo form_radio($commercial->id,'1', $is_active);
                
                if($commercial->active === "0"){
                    $is_inactive = TRUE;
                }else{
                    $is_inactive = FALSE;
                }
                
                echo form_label('Inactif', $commercial->id);
                echo form_radio($commercial->id,'0', $is_inactive);

                } ?>
            <div class="form-group padder">
            <input type="submit" name="submit" value="Sauvegarder les changements" class="btn btn-success btn-lg d-block"/>
            <?php echo validation_errors(); echo form_close( ); ?>
            </div>
                <?php if(isset($success) && $success === true){ ?>
                <div class="form-group">
                     <div class="alert alert-success center">
                        <p>Commerciaux activés mis à jour avec succès</p>
                     </div>
                </div>
                <?php } ?>
            </div>
            </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <a class="link-cub" href="<?php echo site_url();?>/<?php echo $_SESSION['user']->role;?>"><span class="fa fa-home"></span>Retour accueil</a>
        </div>
    </div>

</div><!--container-->

</div>

