<div class="container page">
    <?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'supercommercial') { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                        <p><?php echo $error; ?></p>
            </div>
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/supercommercial/'); ?>"><i class="fas fa-arrow-left"></i>Retour tableau des leads</a>
            </div>
        </div>
    </div>
    <?php }else{ ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="alert alert-danger">
                <p>Accès non autorisé.</p>
            </div>
        </div>
    </div>
    <?php } ?>
</div>