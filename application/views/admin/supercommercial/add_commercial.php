﻿<div class="container">
    <div class="row">
        <?php if(isset($msg_success)){ ?>
        <div class="col-md-6 offset-md-3 col-12 center">
            <div class="alert alert-success">
                <p><?php echo $msg_success; ?></p>
            </div>
        </div>
        <?php } ?>
        <?php if(isset($msg_error)){ ?>
        <div class="col-md-6 offset-md-3 col-12 center">
            <div class="alert alert-danger">
                <p><?php echo $msg_error; ?></p>
                <?php echo validation_errors( ); ?>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-6 offset-md-3 col-12">
            <h1>Ajouter un commercial</h1>
        </div>
        <div class="col-md-6 offset-md-3 col-12"> 
            <div class="bg-gris">
                <?php echo form_open('supercommercial/add_commercial'); ?> 
                    <div class="form-group">
                        <?php 
                        echo form_label('Nom', 'addcom_name');
                        $data = array(
                            'name' => 'addcom_name',
                            'maxlength' => '100',
                            'size' => '50',
                            'value' => set_value('addcom_name'),
                            'class' => 'form-control form-control-lg'
                        );
                        
                        echo form_input($data);
                        ?> 
                    </div>
                    <div class="form-group">
                        <?php  echo form_label('Prénom', 'addcom_prename');
                        $data = array(
                            'name' => 'addcom_prename',
                            'maxlength' => '100',
                            'size' => '50',
                            'value' => set_value('addcom_prename'),
                            'class' => 'form-control form-control-lg'
                        );
                        
                        echo form_input($data);
                        ?> 
                    </div>
                    <div class="form-group">
                        <?php  echo form_label('Login', 'addcom_login');
                        $data = array(
                            'name' => 'addcom_login',
                            'maxlength' => '100',
                            'size' => '50',
                            'value' => set_value('addcom_login'),
                            'class' => 'form-control form-control-lg'
                        );
                        
                        echo form_input($data);
                        ?> 
                    </div>
                    <div class="form-group">
                        <?php  echo form_label('Email', 'addcom_email');
                        $data = array(
                            'name' => 'addcom_email',
                            'maxlength' => '100',
                            'type' => 'email',
                            'size' => '50',
                            'value' => set_value('addcom_email'),
                            'class' => 'form-control form-control-lg'
                        );
                         
                        echo form_input($data);
                        ?> 
                    </div>
                    <div class="form-group">
                          <?php  echo form_label('Téléphone mobile', 'addcom_phone');
                        $data = array(
                            'name' => 'addcom_phone',
                            'maxlength' => '100',
                            'type' => 'tel',
                            'size' => '50',
                            'value' => set_value('addcom_phone'),
                            'class' => 'form-control form-control-lg'
                        );
                        
                        echo form_input($data);
                        ?> 
                    </div>
                    <div class="form-group">
                             <?php  echo form_label('Mot de passe', 'addcom_pass');
                        $data = array(
                            'name' => 'addcom_pass',
                            'maxlength' => '100',
                            'size' => '50',
                            'value' => set_value('addcom_pass'),
                            'class' => 'form-control form-control-lg'
                        );
                        
                        echo form_input($data);
                        ?> 
                    </div>
                    <div class="form-group">
                        <label for="adduser_pwd">Activer l'utilisateur ?</label>
                        <div class="form-group">
                           <?php echo form_label('Oui', 'adduser_active');
                               echo form_radio('addcom_active','1', FALSE, 'disabled'); ?>
                           <?php echo form_label('Non', 'addcom_active');
                            echo form_radio('addcom_active','0', TRUE); ?> 
                        </div>
                    </div>
                    <div class="form-group">
                       
                        <?php
                        $data = array(
                            
                        'type' => 'submit',
                        'class' => 'btn btn-lg btn-success d-block',
                        'value' => 'Ajouter Commercial',
                        'id' => 'addcom_input'    
                            );
                        echo form_submit($data);?>
                    </div>
                    <div class="form-group">
                            <?php 
                            echo form_close( ); ?>
                    </div>
            </div>
        </div>
        </div>
<div class="row">
        <div class="col-md-6 offset-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url();?>/<?php echo $_SESSION['user']->role;?>"><span class="fa fa-backward"></span>Retour accueil</a>
        </div>
</div>
</div>