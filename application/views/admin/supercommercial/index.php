<div class="container page">
    <div class="row">
        <div class="col-12">
            <h1>Outils d'administration</h1>
        </div>
        <div class="col-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url('supercommercial/view_recap');?>"><span class="fas fa-chart-bar"></span>Récapitulatif leads</a>
        </div>
        <div class="col-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url('supercommercial/gestion_com');?>"><span class="far fa-user-circle"></span>Gestion des utilisateurs</a>
        </div>
        <div class="col-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url('archives');?>"><span class="fa fa-archive"></span>Leads archivés</a>
        </div>
    </div>
      <div class="row">
        <div class="col-12">
            <h1>Leads en cours</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
         <table class="table table-striped table-responsive table-borderless min-table line-center">
                <thead>
                <th>Id</th> 
                <th>Date demande</th>
                <th>Informations</th>
                <th>Statut</th>
                <th>Commercial assigné</th>
                <th>Commentaires</th>
                <th>Date de rdv</th>
                <th>Réf devis</th>
                <th>Action</th>
                </thead>
            <tbody
                <?php foreach($full_leads as $lead){    
                if($lead->date_rdv_confirme === "0000-00-00 00:00:00"){$lead->date_rdv_confirme ="non pris";}else{};
                echo '<tr>';
                echo '<td>'.$lead->id.'</td><td class="date_demande">'.$lead->date_demande.'</td><td class="info-lead">'.$lead->informations.'</td><td>'.$lead->statut.'</td><td>'.$lead->commercial_name.'</td><td>'.$lead->commentaire.'</td><td class="date_rdv">'.$lead->date_rdv_confirme.'</td><td>'.$lead->devis_confirme.'</td><td><a  class="btn btn-success btn-edit btn-lg" href="'.site_url('supercommercial/edit_lead/full/'.$lead->id).'">Editer</a></td>';
                echo '</tr>';
                } ?>
            </tbody>
            </table>
        <div id="pagination">
            <?php echo $links; ?>
        </div>
        </div>
    </div>
</div>
 
