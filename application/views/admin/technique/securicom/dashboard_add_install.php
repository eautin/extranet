
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            
            <?php if(isset($message['success'])){?>
            <div class="alert alert-success">
                <p><?php echo $message['success']; ?></p>
            </div>
            <?php }elseif(isset($message['error'])){ ?>
            <div class="alert alert-danger">
                        <p><?php echo $message['error']; ?></p>
            </div>
            <?php } ?>
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/technique/dashboard'); ?>"><i class="fas fa-arrow-left"></i>Retour aux installations</a>
            </div>
        </div>
    </div>
</div>

