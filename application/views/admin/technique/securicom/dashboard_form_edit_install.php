
<?php $install = $install[0];

?>
<div class="container">
    <div class="row">
        <div class="col-xl-12">
                     <?php echo form_open('technique/edit_install/'); ?>
                     <h2>Editer une installation</h2>
                     <div class="form-group">
                        <?php 
                           if('' != validation_errors()){ ?>
                        <div class="alert alert-danger" role="alert">
                           <?php echo validation_errors(); ?>
                        </div>
                        <?php }?>
                     </div>
                     <div class="form-group">
                        <label for="select_type">Type de prestation</label>
                        <?php echo form_dropdown('select_type', array ('installation' => 'Installation', 'SAV' => ' SAV'), $install->type,'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="nom_client">Raison sociale / Nom du client</label> 
                        <input type="text" name="nom_client" class="form-control" value="<?php echo $install->raison_sociale; ?>" placeholder=""/>
                     </div>
                     <div class="form-group">
                        <label for="nom_aff">Numéro Affaire</label>
                        <input type="text" name="num_aff" class="form-control" value="<?php echo $install->AFF; ?>" placeholder=""/>
                     </div>
                     <div class="form-group">
                        <label for="commercial">Commercial assigné</label>
                        <?php echo form_dropdown('commercial', $commerciaux, $install->commercial, 'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="date_install">Date Installation</label>
                        <input id="date_install" name="date_install" type="text" class="form-control" value="<?php echo $install->date_debut; ?>"/>
                     </div>
                     <div class="form-group">
                         <label for="jours">Durée prévisionnelle jours</label>
                           <?php echo form_dropdown('jours', array ("0" => "0 jours", "1" => "1 jours", "2" => "2 jours", "3" => "3 jours", "4" => "4 jours", "5" => "5 jours", "6" => "6 jours", "7" => "7 jours", "8" => "8 jours", "9" => "9 jours"), $install->jours ,'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="heures">Durée prévisionnelle heures</label>
                        <?php echo form_dropdown('heures', array ("0" => "0 heures", "1" => "1 heures", "2" => "2 heures", "3" => "3 heures", "4" => "4 heures", "5" => "5 heures", "6" => "6 heures", "7" => "7 heures"), $install->heures ,'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="observations">Observations</label>
                        <textarea id="observations" class="form-control form-control-lg" name="observations" size="160"><?php echo $install->observations;?></textarea>
                        <input type="hidden" value="<?php echo $install->id;?>" name="install_id"/>
                     </div>
                     <div class="form-group">
                        <input type="submit" name="submit" value="Editer l'installation" class="btn btn-success btn-lg"/>
                     </div>
                     <?php  echo form_close(); ?>
        </div>
    </div>
</div>