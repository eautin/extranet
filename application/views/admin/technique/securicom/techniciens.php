<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h2>Ajouter un technicien / Prestataire</h2>
        </div>
        <div class="col-lg-12">
             <div class="form-group">
                
                     <label for="name_presta">Nom</label>
                     <input type="text" name="name_presta" class="form-control" id="name_tech">

                     <label for="name_presta">Prénom</label>
                     <input type="text" name="name_presta" class="form-control" id="prename_tech">
             
                     <label for="statut_presta">Statut</label>
                     
                     <select class="form-control" name="statut_tech" id="statut_tech">
                         <option value="interne">Interne</option>
                         <option value="prestataire">Prestataire</option>
                     </select>
               
                     <label for="statut_presta">Entreprise référente</label>
                     
                     <select class="form-control" name="entreprise_tech" id="entreprise_tech">
                         <option value="securicom">SECURICOM</option>
                         <option value="tsip">TSIP</option>
                     </select>
              
                    <button type="button" style="margin-top:1rem;" class="btn btn-primary" id="addTech">Ajouter</button>
                </div>
            </div>
            <div style="margin-top:1rem;" class="col-lg-12" id="resp_tech">

            </div>
        </div>
     <div class="row">
        <div class="col-lg-12">
            <h2>Tous les techniciens et prestataires</h2>
            <table class="table" >
                <thead>
                <th>id</th>
                <th>Date de création</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Statut</th>
                <th>Entreprise</th>
                </thead>
                <tbody id="listTech">
                    
                </tbody>
            </table>
        </div>
        </div>

    <div class="row">
         <div class="col-lg-12">
       
        </div>
    </div>
 </div>