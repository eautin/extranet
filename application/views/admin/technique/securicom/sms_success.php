<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="alert alert-success">
                <p><?php echo $message; ?></p>
            </div>
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/technique/'); ?>"><i class="fas fa-arrow-left"></i>Retour accueil technique</a>
            </div>
        </div>
    </div>
</div>
