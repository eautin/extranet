<div class="container">
   <div class="row">
      <div class="col-xl-12">
         <h1>Installations archivées</h1>     
      </div>
   </div>
    <div class="row">
        <div class="col-xl-12">
            <?php if(isset($message)){?>
            <div class="alert alert-success" style="margin-top:1rem;">
                <p><?php echo $message; ?></p>
            </div>
            <?php }elseif(isset($message['error'])){ ?>
            <div class="alert alert-danger">
                        <p><?php echo $message['error']; ?></p>
            </div>
            <?php } ?>
        </div>
    </div>
       <div class="row">
        <div class="col-lg-12">
         <table class="table table-striped table-responsive table-borderless min-table">
                <thead>
                    <th>date création</th>
                    <th>Type</th>
                    <th>AFF</th>
                    <th>Commercial</th>
                    <th>Client</th>
                    <th>date début</th>
                    <th>durée(h)</th>
                    <th>Observations</th>
                </thead>
            <tbody>
                <?php foreach($installs as $install){ 
                    if($install->commercial != 0){
                        $install->commercial = $install->nom." ".$install->prenom;
                    }else{
                        $install->commercial = "non renseigné";
                    }
                echo '<tr>';
                echo '<td class="date_creation">'.$install->date_creation.'</td><td class="">'.$install->type.'</td><td>'.$install->AFF.'</td><td>'.$install->commercial.'</td><td>'.$install->raison_sociale.'</td><td class="date_debut">'.$install->date_debut.'</td><td>'.$install->duree_time.'</td><td>'.$install->observations.'</td>';
                echo '</tr>';
                } ?>
            </tbody>
            </table>
        <!--<div id="pagination">
            <?php //echo $links; ?>
        </div>-->
        </div>
    </div>
    <!--<div class="row">
        <div class="col-xl-12">
            <a class="link-cub" href="<?php echo site_url('technique/form_add_install') ?>"><i class="fa fa-plus-square"></i>Ajouter une installation </a>
        </div>
        <div class="col-xl-12">
            <a class="link-cub" href="<?php echo site_url('technique/page_archive_install') ?>"><i class="fa fa-archive" aria-hidden="true"></i>Archives des installations</a>
        </div>-->
    </div>
</div>

