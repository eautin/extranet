
<div class="container min-height-600">
    <div class="row">
        <div class="col-lg-12">
            <h1>Espace technique Securicom</h1>                    
        </div>
    </div>
        <div class="row">
            <div class="col-xl-12">
                <a class="link-cub" href="<?php echo site_url('technique/dashboard') ?>"><i class="fa fa-calendar"></i>Suivi des installations & SAV</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                <a class="link-cub" href="<?php echo site_url('technique/interventions') ?>"><i class="far fa-clock"></i>Saisie des interventions</a>
            </div>
            <div class="col-xl-6">
             <a class="link-cub" href="<?php echo site_url('technique/cumuls') ?>"><i class="fas fa-chart-bar"></i>Cumuls des interventions</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                <a class="link-cub" href="<?php echo site_url('technique/techniciens') ?>"><i class="far fa-user-circle"></i>Gestion des techniciens</a>
            </div>
            <div class="col-xl-6">
                <a class="link-cub" href="<?php echo site_url('technique/sms') ?>"><i class="fa fa-paper-plane"></i>Envoi de SMS</a>
            </div>
        </div>
</div>
