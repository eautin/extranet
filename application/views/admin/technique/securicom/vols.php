<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>Ensemble de vols confirmés</h2>
          
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
             <table class="table" >
                <thead>
                <th>Id</th>
                <th>Date d'intervention</th>
                <th>Client</th>
                <th>Adresse</th>
                <th>informations complémentaires</th>
                <th>Technicien référent</th>
                <th>Mettre à jour les informations</th>
                </thead>
                <tbody id="listVol">
                    
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
         <div class="col-lg-12">
       
        </div>
    </div>
    </div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalUpdateVol">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title"><span  data-id="" id="dynamicID"></span>Mettre à jour les informations de l'intervention</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="vol_client">Nom du client:</label>
                    <input type="text" class="form-control" name="vol_client" id="volnomClient"> 
                </div>
                <div class="form-group">
                    <label for="vol_client">Adresse</label>
                    <input type="text" class="form-control" name="vol_adresse" id="volAdresse"> 
                </div>
                <div class="form-group">
                    <label for="info_client">Informations complémentaires</label>
                    <textarea name="info_client" class="form-control" id="volMsg"></textarea>
                    <input type="hidden" name="vol_id" id="volID" value=""/>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="updateVol">Sauvegarder</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                
                
            </div>
            <div class="modal-footer">
                            <div id="modal-resp-vol">
                    
                </div>
            </div>

        </div>
    </div>
</div>