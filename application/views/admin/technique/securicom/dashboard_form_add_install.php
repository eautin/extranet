<div class="container">
    <div class="row">
        <div class="col-xl-12">
                     <?php echo form_open('technique/add_install/'); ?>
                     <h2>Ajouter une nouvelle installation</h2>
                     <div class="form-group">
                        <?php 
                        if('' != validation_errors()){ ?>
                        <div class="alert alert-danger" role="alert">
                           <?php echo validation_errors(); ?>
                        </div>
                        <?php }?>
                     </div>
                     <div class="form-group">
                        <label for="select_type">Type de prestation</label>
                        <?php echo form_dropdown('select_type', array ('installation' => 'Installation', 'SAV' => ' SAV'), set_value('select_type'),'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="nom_client">Raison sociale / Nom du client</label> 
                        <input type="text" name="nom_client" class="form-control" value="<?php echo set_value('nom_client'); ?>" placeholder=""/>
                     </div>
                     <div class="form-group">
                        <label for="nom_aff">Numéro Affaire</label>
                        <input type="text" name="num_aff" class="form-control" value="<?php echo set_value('num_aff'); ?>" placeholder=""/>
                     </div>
                     <div class="form-group">
                        <label for="commercial">Commercial assigné</label>
                        <?php echo form_dropdown('commercial', $commerciaux, set_value('commercial'), 'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="date_install">Date Installation</label>
                        <input id="date_install" name="date_install" type="text" class="form-control" value="<?php echo set_value('date_install'); ?>"/>
                     </div>
                     <div class="form-group">
                         <label for="jours">Durée prévisionnelle jours</label>
                           <?php echo form_dropdown('jours', array ("0" => "0 jours", "1" => "1 jours", "2" => "2 jours", "3" => "3 jours", "4" => "4 jours", "5" => "5 jours", "6" => "6 jours", "7" => "7 jours", "8" => "8 jours", "9" => "9 jours"), set_value('jours') ,'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="heures">Durée prévisionnelle heures</label>
                        <?php echo form_dropdown('heures', array ("0" => "0 heures", "1" => "1 heures", "2" => "2 heures", "3" => "3 heures", "4" => "4 heures", "5" => "5 heures", "6" => "6 heures", "7" => "7 heures"), set_value('heures') ,'class="form-control"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="observations">Observations</label>
                        <textarea id="observations" class="form-control form-control-lg" name="observations" size="160"><?php echo set_value('observations'); ?></textarea>
                     </div>
                     <div class="form-group">
                        <input type="submit" name="submit" value="Ajouter l'installation" class="btn btn-success btn-lg"/>
                     </div>
                     <?php  echo form_close(); ?>
        </div>
    </div>
</div>