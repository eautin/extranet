<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <h1>Envoi de SMS</h1>
            <div class="bg-gris">
                    <?php echo form_open('technique/prepare_sms/'); ?>
                    <h5>Remplir les informations</h5>
                    <div class="form-group">
                            <?php 
                            if('' != validation_errors( )){ ?>
                            <div class="alert alert-danger" role="alert">
                            <?php echo validation_errors( ); ?>
                            </div>
                            <?php }?>
                    </div>
                    <div class="form-group">
                         <label for="select_societe">Emetteur du SMS</label>
                        <?php echo form_dropdown('select_societe', array('securicom' => 'SECURICOM', 'tsip' => ' TSIP'), set_value('select_societe'),'class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                         <label for="select_type">Type de sms</label>
                        <?php echo form_dropdown('select_type', array ('installation' => 'Installation', 'SAV' => ' SAV'),set_value('select_type') ,'class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label for="nom_client">Nom du client</label>
                        <input type="text" name="nom_client" class="form-control" value="<?php echo set_value('nom_client');?>" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="tel_client">Téléphone du client</label>
                        <input type="text" name="tel_client" class="form-control" value="<?php echo set_value('tel_client');?>" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="date_picker">Date et heure du RDV</label>
                        <input id="date_rdv" name="date_rdv" type="text" value="<?php echo set_value('date_rdv');?>" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="adduser_prename">Message optionnel pour le client</label>
                        <textarea id="msg_client" class="form-control form-control-lg" name="msg_client" size="160"><?php echo set_value('msg_client');?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Envoyer votre SMS" class="btn btn-success btn-lg"/>
                    </div>
                    <div class="form-group">
                          
                        
                            <?php  echo form_close( ); ?>
                    </div>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <h2>Archive des sms envoyés</h2>
        </div>
        <div class="col-xl-12">
             <table class="table" >
                <thead>
                <th>Id</th>
                <th>Emetteur</th>
                <th>Type</th>
                <th>Nom Client</th>
                <th>Date Envoi</th>
                <th>Date RDV</th>
                <th>Tel Client</th>
                <th>Msg Client</th>
                </thead>
                <tbody>
                    <?php foreach($archive_sms as $value) {?>
                    <tr>
                        <?php echo '<td>'.$value->id.'</td>'.'<td>'.$value->emetteur.'</td>'.'<td>'.$value->type.'</td><td>'.$value->nom_client.'</td><td>'.$value->date_envoi.'</td><td>'.$value->date_rdv.'</td><td>'.$value->tel_client.'</td><td>'.$value->msg_client.'</td>'; ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
         <div class="col-lg-12">
       
        </div>
    </div>
    </div>
    </div>
</div>
