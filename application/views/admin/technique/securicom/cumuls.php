
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>Cumul des interventions</h2>
            <p>Séléctionnez la période pour obtenir le nombre d'intervention réalisées par technicien.</p>
        </div>
    </div>
    <div class="row">
                <div class="col-lg-6 col-sm-12">
                     <label for="dayStart">Jour début</label>
                <input type="date" name="dayStart" class="form-control" id="startDate">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <label for="dayEnd">Jour fin</label>
                <input type="date" name="dayEnd" class="form-control" id="endDate">
                </div>
                <div class="col-md-12">
                    <button type="button" style="margin-top:1rem;" class="btn btn-primary" id="getDates">Obtenir les interventions</button>
                </div>
    </div>
    <div class="row">
         <div class="col-lg-12">
        
            <div id="calendar">
                
            </div>
        </div>
    </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalCumul">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title">Nombre d'interventions entre le <span  data-day="" id="popinDayStart"></span> et le <span id="popinDayEnd"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th></th>
                    <th>SAV<br/>intrusion</th>
                    <th>SAV<br/>Incendie</th>
                    <th>SAV<br/>Crt d'accès</th>
                    <th>SAV<br/>vidéo</th>
                    <th>SAV<br/>agression</th>
                    <th>SAV<br/>SAP</th>
                    <th>Maint<br/>Intrusion</th>
                    <th>Maint<br/>Incendie</th>
                    <th>Maint<br/>Crt accès</th>
                    <th>Maint<br/>vidéo</th>
                    <th>Maint<br/>agression</th>
                    <th>Maint<br/>SAP</th>
                    <th>Inst<br/>Intrusion</th>
                    <th>Inst<br/>incendie</th>
                    <th>Inst<br/>crt d'accès</th>
                    <th>Inst<br/>vidéo</th>
                    <th>Inst<br/>agression</th>
                    <th>Inst<br/>SAP</th>
                    </thead>
                    <tbody id="formInjector">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn btn-secondary" id="closePopin" data-dismiss="modal">fermer</button>
                
                <div id="modal-resp-saisie-success">
                    
                </div>
                <div id="modal-resp-saisie-success">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalInfo">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
              
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Veuillez séléctionner une plage de jours pour obtenir le nombre d'intervention par technicien.</p>
            </div>
            <div class="modal-footer">
           
            </div>
        </div>
    </div>
</div>
