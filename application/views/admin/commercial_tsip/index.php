<div class="container page">
       <div class="row"  style="margin-top:2rem;">
        <div class="col-12">
            <h1>Résumé chiffre affaire du mois <?php echo $month;?> <?php echo $year;?></h1>
            <div class="bg-gris">
                <div class="row">
                    <div class="col-3">
                        <h2>Nom</h2>
                    </div>
                    <div class="col-3">
                        <h2>chiffre location</h2>
                    </div>
                    <div class="col-3">
                        <h2>chiffre vente</h2>
                    </div>
                     <div class="col-3">
                        <h2>Contrats signés</h2>
                    </div>
                </div>
                <?php foreach ($commercial_bilan as $com) { ?>
                <form>
                <div class="row" data-id="<?php echo $com->id; ?>">
                    
                    <div class="col-3">  
                        <?php echo $com->prenom.' '.$com->nom; ?>   
                    </div>
                    
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" disabled value="<?php echo $com->total_loc." €"; ?>" name="total_loc" class="form-control" placeholder="Chiffre location">
                        </div>
                           
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" disabled  value="<?php echo $com->total_vente." €"; ?>" name="total_vente" class="form-control" placeholder="Chiffre vente">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                          <input type="text" disabled  value="<?php echo $com->total_devis; ?>" name="total_vente" class="form-control" placeholder="">
                        </div>
                    </div>
                    
                </div>
                </form>
                
                <?php } ?>
                
                <?php foreach ($commercial_tsip as $com) { ?>
                <form>
                <div class="row" data-id="<?php echo $com->id; ?>">
                    
                    <div class="col-3">  
                        <?php echo $com->prenom.' '.$com->nom; ?>   
                    </div>
                    
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" disabled value="<?php echo $com->total_loc." €"; ?>" name="total_loc" class="form-control" placeholder="Chiffre location">
                        </div>
                           
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" disabled  value="<?php echo $com->total_vente." €"; ?>" name="total_vente" class="form-control" placeholder="Chiffre vente">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                          <input type="text" disabled  value="<?php echo $com->total_devis; ?>" name="total_vente" class="form-control" placeholder="">
                        </div>
                    </div>
                    
                </div>
                </form>  
                <?php } ?>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <h1>Gestion de vos leads</h1>
        </div>
        
    </div>
    <div class="row">
        <div class="col-xl-12">
             <table class="table table-striped table-borderless min-table">
                <thead>
                    <th>Id</th>
                    <th>Date demande</th>
                    <th>Nom</th>
                    <th>Tel</th>
                    <th>Msg</th>
                    <th>Statut</th>
                    <th>Commentaires</th>
                    <th>Date RDV</th>
                    <th>Réf devis</th>
                    <th>Date confirme devis</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach($full_leads as $lead){    
                    if($lead->date_rdv_confirme === "0000-00-00 00:00:00"){$lead->date_rdv_confirme ="non pris";}
                    echo '<tr>';
                    echo '<td>'.$lead->id.'</td><td class="date_demande">'.$lead->date_demande.'</td><td>'.$lead->nom_contact.'</td><td>'.$lead->tel.'</td><td>'.$lead->msg.'</td><td>'.$lead->statut.'</td><td>'.$lead->commentaire.'</td><td class="date_rdv">'.$lead->date_rdv_confirme.'</td><td>'.$lead->devis_confirme.'</td><td class="date_demande">'.$lead->date_devis_confirme.'</td><td><a  class="btn btn-success btn-lg" href="'.site_url('commercial_tsip/edit_tsip_lead/'.$lead->id).'">Editer</a></td>';
                    echo '</tr>';
                    } ?>
                </tbody>
            </table>
            <div id="pagination">
                <?php echo $links; ?>
            </div>
        </div>          
    </div>
</div>