<div class="container page">
    <?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'commercial_tsip' || $_SESSION['role'] === 'supercommercial') { ?>
    <div class="row">
        <div class="col-12">
            <div class="bg-gris">
                    <?php echo form_open('commercial/update_lead'); ?>
                    <div class="form-group">
                         <label for="statut">Statut actuel du lead</label>
                        <?php echo form_dropdown('statut', $status_lead, $status_id->id,'class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label id="labelCodeDevis" for="code_devis">Numéro de devis</label>
                        <input type="text" name="code_devis" class="form-control" id="code_devis" value="<?php echo $lead->devis_confirme;?>" placeholder="<?php echo $lead->devis_confirme;?>"/>
                    </div>
                    <div class="form-group">
                        <label id="labelDatePicker" for="date_picker">Date et heure du RDV</label>
                        <input id="datetimepicker" name="date_picker" type="text" class="form-control" value="<?php echo $lead->date_rdv_confirme;?>" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label id="labelDatePickerDevis" for="date_devis">Date validation du devis</label>
                        <input id="datetimepickerDevis" name="date_devis" type="text" class="form-control" value="<?php echo $lead->date_devis_confirme;?>" placeholder="<?php echo $lead->date_devis_confirme;?>"/>
                    </div>
                    <div class="form-group">
                        <label for="adduser_prename">Notes et commentaire</label>
                        <textarea class="form-control form-control-lg" name="commentaire"><?php echo $lead->commentaire;?></textarea>
                    </div>
                    <?php echo form_hidden('id',$lead->id); ?>
                    <?php echo form_hidden('type',$lead->type); ?>
          
                    <div class="form-group">
                         <?php
                        $data = array(
                            
                        'type' => 'submit',
                        'class' => 'btn btn-lg btn-success d-block',
                        'value' => 'Mettre à jour',
                        'id' => 'edit_lead'    
                            );
                        echo form_submit($data);?>
                        <input type="hidden" name="type_lead" value="<?php echo $lead->type; ?>"/>
                        <input type="hidden" name="id_lead" value="<?php echo $lead->id; ?>"/>
                    </div>
                    <div class="form-group">
                            <?php echo validation_errors(); echo form_close(); ?>
                    </div>
                    <div class="form-group">
                        <a class="btn btn-dark btn-lg" href="<?php echo site_url('/commercial/'); ?>"><i class="fas fa-arrow-left"></i>Retour tableau des leads</a>
                    </div>
                </div>
        </div>
    </div>
     <?php }else{ ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger">
                <p>Accès non autorisé.</p>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

