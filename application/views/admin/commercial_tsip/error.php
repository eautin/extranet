<div class="container page">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                        <p><?php echo $error; ?></p>
            </div>
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/commercial_tsip/'); ?>"><i class="fas fa-arrow-left"></i>Retour tableau des leads</a>
            </div>
        </div>
    </div>
</div>