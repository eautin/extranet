<div class="container page">
    <div class="row">
        <div class="col-lg-12">
            <h1>Editer lead <?php echo $lead->id;?></h1>
            <table class="table table-striped table-borderless min-table">
                <thead>
                <th>Id</th> 
                <th>Date demande</th>
                <th>Nom</th>
                <th>Tel</th>
                <th>Msg</th>
                <th>Statut</th>
                <th>Commentaire</th>
                <th>Date RDV</th>
                <th>Ref devis</th>
                <th>Date valid devis</th>
                <th></th>
                </thead>
            <tbody>
                <td>
                    <?php echo $lead->id;?>
                </td>
                <td class="date_demande">
                    <?php echo $lead->date_demande;?>
                </td>
                <td>
                    <?php echo $lead->nom_contact;?>
                </td>
                <td>
                    <?php echo $lead->tel;?>
                </td>
                <td>
                    <?php echo $lead->msg;?>
                </td>
                <td>
                    <?php echo $lead->statut;?>
                </td>
                <td>
                    <?php echo $lead->commentaire;?>
                </td>
                <td class="date_demande">
                    <?php echo $lead->date_rdv_confirme;?>
                </td>
                <td>
                    <?php echo $lead->devis_confirme;?>
                </td>
                <td class="date_demande">
                    <?php echo $lead->date_devis_confirme;?>
                </td>
            </tbody>
            </table>
        </div>
        <div class="col-xl-12">
            <p class="pink">Statut actuel du lead : <span class="statut"><?php  echo $lead->statut; ?></span></p>
        </div>
        <div class="col-lg-12"> 
                    <?php echo form_open('commercial_tsip/update_lead/'); ?>
            
                    <?php
                    // si a traiter on affiche rappel ne donne pas suite ou rappelé rdv pris. 
                    if($status_id->id == "1"){ ?>
                    <div class="form-group bg-radio"> 
                        <label for="statut1">A traiter</label>
                        <input type="radio" name="statut" value="1" id="statut1" <?php if($status_id->id === '1'){ echo 'checked="checked"'; } ?>>
                    </div>
                    <div class="form-group bg-radio"> 
                        <label for="statut1">Rappelé ne donne pas suite</label>
                        <input type="radio" name="statut" value="2" id="statut2" <?php if($status_id->id === '2'){ echo 'checked="checked"'; } ?>>
                    </div>
                    <div class="form-group bg-radio"> 
                        <label for="statut1">Rappelé rdv pris</label>
                        <input type="radio" name="statut" value="3" id="statut3" <?php if($status_id->id === '3'){ echo 'checked="checked"'; } ?>>
                    </div>
                    <?php } ?>
                    <?php 
                    // si rappelé rdv pris 
                    if($status_id->id == "3"){ ?>
                    <div class="form-group bg-radio"> 
                        <label for="statut1">Rappelé rdv pris</label>
                        <input type="radio" name="statut" value="3" id="statut3" <?php if($status_id->id === '3'){ echo 'checked="checked"'; } ?>>
                    </div>
                    <div class="form-group bg-radio"> 
                        <label for="statut1">Devis en cours</label>
                        <input type="radio" name="statut" value="4" id="statut4">
                    </div>
                    <div class="form-group bg-radio"> 
                        <label for="statut1">RDV ne donne pas suite</label>
                        <input type="radio" name="statut" value="7" id="statut7">
                    </div>
                    <?php } ?>

            
                    <?php if($status_id->id == '4'){ ?>
                        <div class="form-group bg-radio"> 
                            <label for="statut1">Devis en cours</label>
                            <input type="radio" name="statut" value="4" id="statut4" <?php if($status_id->id === '4'){ echo 'checked="checked"'; } ?>>
                        </div>
                        <div class="form-group bg-radio"> 
                            <label for="statut1">Devis validé</label>
                            <input type="radio" name="statut" value="5" id="statut5" <?php if($status_id->id === '5'){ echo 'checked="checked"'; } ?>>
                        </div>
                        <div class="form-group bg-radio">
                            <label for="statut1">Devis ne donne pas suite</label>
                            <input type="radio" name="statut" value="6" id="statut6" <?php if($status_id->id === '6'){ echo 'checked="checked"'; } ?>>
                        </div>
                    <?php } ?>
                    </div>
        <div class="col-lg-12">
                    <div class="form-group">
                        <label id="labelCodeDevis" for="code_devis">Numéro de devis</label>
                        <input type="text" name="code_devis" class="form-control" id="code_devis"/>
                    </div>
                    <div class="form-group">
                        <label id="labelDatePicker" for="date_picker">Date et heure du RDV</label>
                        <input id="datetimepicker" name="date_picker" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label id="labelDatePickerDevis" for="date_devis">Date validation du devis</label>
                        <input id="datetimepickerDevis" name="date_devis" type="text" class="form-control">
                    </div>
       
            <?php if($status_id->id == '2' || $status_id->id == '5' || $status_id->id == '6' || $status_id->id == '7' || $status_id->id == '8'){ ?>
            
                <p class="lead-block"><i class="fa fa-lock"></i>En statut <b><?php echo $lead->statut;?></b>, ce lead n'est plus éditable.</p>
                  
            <?php }else{ ?>
                
                    <div class="form-group">
                        <label for="adduser_prename">Notes et commentaire</label>
                        <textarea class="form-control form-control-lg" placeholder="" value="" name="commentaire"><?php echo $lead->commentaire;?></textarea>
                        <?php echo form_hidden('id',$lead->id); ?>
                        <?php echo form_hidden('commercial_id',$lead->commercial_id); ?>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Mettre a jour le lead" class="btn btn-success btn-lg"/>
                    </div>
            <?php } ?>
                    <div class="form-group">
                            <?php echo validation_errors(); echo form_close(); ?>
                    </div>
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/commercial_tsip/'); ?>"><i class="fas fa-arrow-left"></i>Retour tableau des leads</a>
            </div>
        </div>
    </div>
</div>
