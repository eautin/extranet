<div class="container page">
    <div class="row">
        <div class="col-12">
            <h1>Outils d'administration</h1>
        </div>
        <div class="col-12">
            <a class="link-cub" href="<?php echo site_url('generate_docs');?>"><i class="fa fa-file-word" aria-hidden="true"></i>Création de documents clients</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h1>Gestion de vos leads</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
             <table class="table table-striped table-responsive table-borderless min-table" id="stack-table">
                <thead>
                <th>Id</th> 
                <th>Date demande</th>
                <th>Informations</th>
                <th>Statut</th>
                <th>Commentaires</th>
                <th>Date de rdv</th>
                <th>Réf devis</th>
                <th>Date devis</th>
                <th>Action</th> 
                </thead>
                
                <?php //var_dump($full_leads); ?>
                <tbody>
                <?php 
                
                if($full_leads){
                    
                
                foreach($full_leads as $lead){    
                if($lead->date_rdv_confirme === "0000-00-00 00:00:00"){$lead->date_rdv_confirme ="non pris";}
                echo '<tr>';
                echo '<td>'.$lead->id.'</td><td class="date_demande">'.$lead->date_demande.'</td><td class="info-lead">'.$lead->informations.'</td><td>'.$lead->statut.'</td><td>'.$lead->commentaire.'</td><td class="date_rdv">'.$lead->date_rdv_confirme.'</td><td>'.$lead->devis_confirme.'</td><td class="date_demande">'.$lead->date_devis_confirme.'</td><td><a  class="btn btn-success btn-lg" href="'.site_url('commercial/edit_lead/full/'.$lead->id).'">Editer</a></td>';
                echo '</tr>';
                } 
                
                }
                ?>
            </tbody>
            </table>
            <div id="pagination">
                <?php echo $links; ?>
            </div>
        </div>          
    </div>
</div>
