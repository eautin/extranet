<div class="container page">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="alert alert-success">
                <p><?php echo $message; ?></p>
            </div>
            
            <div class="form-group">
                <a class="btn btn-dark btn-lg" href="<?php echo site_url('/commercial/'); ?>"><i class="fas fa-arrow-left"></i>Retour tableau des leads</a>
            </div>
        </div>
    </div>
</div>