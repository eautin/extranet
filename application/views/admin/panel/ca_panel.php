<div class="row"  style="margin-top:2rem;">
        <div class="col-12">
            <h1>Résumé chiffre affaire du mois <?php echo $month;?> <?php echo $year;?></h1>
            <div class="bg-gris">
                <div class="row">
                    <div class="col-3">
                        <h2>Nom</h2>
                    </div>
                    <div class="col-3">
                        <h2>chiffre location</h2>
                    </div>
                    <div class="col-3">
                        <h2>chiffre vente</h2>
                    </div>
                     <div class="col-3">
                        <h2>Contrats signés</h2>
                    </div>
                </div>
                <?php foreach ($commercial_bilan as $com) { ?>
                <form>
                <div class="row" data-id="<?php echo $com->id; ?>">
                    
                    <div class="col-2">  
                        <?php echo $com->prenom.' '.$com->nom; ?>   
                    </div>
                    
                    <div class="col-2">
                        <div class="form-group">
                            <input type="text" disabled value="<?php echo $com->total_loc." €"; ?>" name="total_loc" class="form-control" placeholder="Chiffre location">
                        </div>
                           
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <input type="text" disabled  value="<?php echo $com->total_vente." €"; ?>" name="total_vente" class="form-control" placeholder="Chiffre vente">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                          <input type="text" disabled  value="<?php echo $com->total_devis; ?>" name="total_vente" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                          <input type="text" disabled  value="<?php echo $com->total_devis; ?>" name="total_vente" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
                </form>
                
                <?php } ?>
                
                <?php foreach ($commercial_tsip as $com) { ?>
                <form>
                <div class="row" data-id="<?php echo $com->id; ?>">
                    
                    <div class="col-3">  
                        <?php echo $com->prenom.' '.$com->nom; ?>   
                    </div>
                    
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" disabled value="<?php echo $com->total_loc." €"; ?>" name="total_loc" class="form-control" placeholder="Chiffre location">
                        </div>
                           
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <input type="text" disabled  value="<?php echo $com->total_vente." €"; ?>" name="total_vente" class="form-control" placeholder="Chiffre vente">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                          <input type="text" disabled  value="<?php echo $com->total_devis; ?>" name="total_vente" class="form-control" placeholder="">
                        </div>
                    </div>
                    
                </div>
                </form>  
                <?php } ?>
        </div>
    </div>
    </div>