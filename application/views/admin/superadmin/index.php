<div class="container">
    <div class="row">
        <?php if(isset($msg)){ ?>
        <div class="col-md-12">
            <div class="alert alert-success">
                <p><?php echo $msg; ?></p>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-12">
            <h1>Ajouter un utilisateur</h1>
        </div>
        <div class="col-md-12"> 
                <?php echo form_open('superadmin/add_user'); ?> 
                    <div class="form-group">
                        <label for="adduser_name">Nom</label>
                        <input type="text" name="adduser_name" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <label for="adduser_prename">Prénom</label>
                        <input type="text" name="adduser_prename" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <label for="adduser_email">Email</label>
                        <input type="email" name="adduser_email" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <label for="adduser_role">Rôle</label>
                        <select class="form-control form-control-lg" name="adduser_role">
                            <option value="admin">Admin</option>
                            <option value="direction">Direction</option>
                            <option value="technique">Technique</option>
                            <option value="compta">Comptabilité</option>
                            <option value="commercial">Commercial</option>
                            <option value="assistant_comm">Assistante Commerciale</option>
                            <option value="qualite">Qualité</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="adduser_societe">Société</label>
                        <select class="form-control form-control-lg" name="adduser_societe">
                            <option value="securicom">SECURICOM</option>
                            <option value="tsip">TSIP</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="adduser_login">Nom d'utilisateur</label>
                        <input type="text" name="adduser_login" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <label for="adduser_pwd">Mot de passe</label>
                        <input type="text" name="adduser_pwd" class="form-control form-control-lg"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Ajouter utilisateur" class="btn btn-success btn-lg"/>
                    </div>
                    <div class="form-group">
                            <?php echo validation_errors(); echo form_close(); ?>
                    </div>
            </div>
        </div>
    </div>