<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert alert-danger">
                <p><?php echo $msg; ?></p>
            </div>
            <div>
                <a  class="btn btn-success" href="<?php echo site_url('superadmin/index') ?>">Réessayer</a>
            </div>
        </div>
    </div>
</div>