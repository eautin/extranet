<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-success">
                <p><?php echo $msg; ?></p>
            </div>
            <div>
                <a class="btn btn-success" href="<?php echo site_url('superadmin/index') ?>">Ajouter un utilisateur</a>
            </div>
        </div>
    </div>
</div>