<div class="container">
     <div class="row">
        <div class="col-12">
            <h1>Archives Lead</h1>
            
            <?php //var_dump($archives); ?>
            
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
         <table class="table table-striped table-responsive table-borderless min-table">
                <thead>
                <th>Id</th> 
                <th>Date demande</th>
                <th>Informations</th>
                <th>Statut</th>
                <th>Commercial assigné</th>
                <th>Commentaires</th>
                <th>Date de rdv</th>
                <th>Réf devis</th>
                <th>Action</th>
                </thead>
            <tbody
                <?php foreach($archives as $lead){    
                if($lead->date_rdv_confirme === "0000-00-00 00:00:00"){$lead->date_rdv_confirme ="non pris";}
                if(!isset($lead->commercial_name)){$lead->commercial_name = "non renseigné";}
                echo '<tr>';
                echo '<td>'.$lead->id.'</td><td class="date_demande">'.$lead->date_demande.'</td><td class="info-lead">'.$lead->informations.'</td><td>'.$lead->statut.'</td><td>'.$lead->commercial_name.'</td><td>'.$lead->commentaire.'</td><td class="date_rdv">'.$lead->date_rdv_confirme.'</td><td>'.$lead->devis_confirme.'</td><td><a  class="btn btn-success btn-edit btn-lg" href="'.site_url('supercommercial/edit_lead/full/'.$lead->id).'">Editer</a></td>';
                echo '</tr>';
                } ?>
            </tbody>
            </table>
        <div id="pagination">
            <?php echo $links; ?>
        </div>
        </div>
    </div>
        <div class="row">
        <div class="col-12">
            <a class="link-cub" href="<?php echo site_url();?>/<?php echo $_SESSION['user']->role;?>"><span class="fa fa-home"></span>Retour accueil</a>
        </div>
    </div>
</div>