<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 col-12">
            <h1>Se connecter en tant que</h1>
            <div class="bg-gris">
            <?php echo form_open('login/select_role'); ?>
                
                <?php
                
                $options = array();
                
                foreach( $roles as $role){

                    $options[$role['nom']] = $role['nom'];
                
                }
    
                ?>
                <h4>Rôle utilisateur</h4>
            
                <?php
                echo form_dropdown('role',$options, null, 'class="form-control"');
                ?>
            <div class="form-group" style="margin-top:1rem;">
                <input type="submit" name="submit" value="Valider" class="btn btn-success btn-lg"/>
                <?php echo validation_errors(); echo form_close(); ?>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url();?>/<?php echo $_SESSION['user']->role;?>"><span class="fa fa-home"></span>Retour accueil</a>
        </div>
    </div>
</div>