<!doctype html>
<html class="no-js" lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Accès extranet</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="apple-touch-icon" href="icon.png">
	<link rel="stylesheet" href="<?php echo SCSS_DIR; ?>webapp.css">
	<link rel="stylesheet" href="<?php echo CSS_DIR; ?>bootstrap.css">
	<?php if(isset($css_to_load) && $css_to_load != ""){ ?>
		<link rel="stylesheet" href="<?php echo CSS_DIR.$css_to_load; ?>">
	<?php } ?>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script>
		var base_url = '<?php echo base_url(); ?>';
	</script>
</head>
<body>
<header>
	<div id="toggle-menu"></div>
	<a href="" class="logo-bg"><img class="img-fluid" src="<?php echo IMG_DIR; ?>mysecuricom.png" /></a>
	<?php if(isset($_SESSION['is_logged'])){ ?>
		<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'supercommercial' && isset($credit_sms)){ ?>
			<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
		<?php } ?>
		<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'technique' && isset($credit_sms)){ ?>
			<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
		<?php } ?>
		<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'commercial' && isset($credit_sms)){ ?>
			<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
		<?php } ?>
		<?php if(isset($_SESSION['role']) && $_SESSION['role'] === 'commercial_tsip' && isset($credit_sms)){ ?>
			<p class="sms">Crédit sms disponible : <?php echo $credit_sms; ?></p>
		<?php } ?>
		<a class="logout" href="<?php echo site_url('login/logout') ?>"><i class="fas fa-sign-out-alt"></i></a>
	<?php } ?>
</header>
<div id="bg-login">
   <div class="container">
      <div class="row">
         <div class="col-lg-6 offset-lg-3 login-box spacer-min col-md-12">
                <?php echo form_open('login/validate_login'); ?>
                    <div class="form-group">

                        <input type="text" name="userLogin" class="form-control form-control-lg" placeholder="Utilisateur"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="userPwd" class="form-control form-control-lg" placeholder="Mot de passe"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Connexion" class="btn btn-success btn-lg" style="width:100%;"/>
                    </div>
               <div class="form-group">
				   <?php if((validation_errors() != "")){ ?>
				   <div class="alert alert-danger">
                   <?php echo validation_errors( ); ?>
				   </div>
				   <?php  } ?>

                   <?php // echo password_hash('Secu2021*',PASSWORD_DEFAULT); ?>
               </div>
                <?php if(isset($error)){ ?>
                    <div class="alert alert-danger">
                        <p><?php echo $error; ?></p>
                    </div>
                <?php }?>
			 <div class="form-group">
				 <small>V 1.1.0 - 25/01/2023</small>
			 </div>
         </div>
      </div>
   </div>
</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xl-12 mentions">
				<p>2018 - 2023 <br/>extranet.securicom-telesurveillance.fr</p>
			</div>
		</div>
	</div>
</footer>
<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>

<?php if(isset($js_to_load) && $js_to_load != ''){ ?>

<script src="<?php echo JS_DIR.$js_to_load;?>"></script>

<?php } ?>
