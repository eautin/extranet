<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Accès aux documents</h1>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'DT_02_DEVIS_INTRUSION.pdf';?>"><i class="fas fa-file-pdf"></i>Devis Intrusion (DT02)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'DT_02_DEVIS_VIDEO.pdf';?>"><i class="fas fa-file-pdf"></i>Devis vidéosurveillance (DT02)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'DT_04_CONTRAT_MAINTENANCE_INTRUSION.pdf';?>"><i class="fas fa-file-pdf"></i>Contrat maintenance Intrusion (DT04)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'DT_04_CONTRAT_MAINTENANCE_VIDEO.pdf';?>"><i class="fas fa-file-pdf"></i>Contrat maintenance Vidéo (DT04)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'DT_32_ANALYSE_DE_RISQUE.pdf';?>"><i class="fas fa-file-pdf"></i>Analyse de risque cyber sécurité (D32)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'FO_06_Etude_Intrusion.pdf';?>"><i class="fas fa-file-pdf"></i>Analyse de risque Intrusion (FO06)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'FO_06_Etude_Video.pdf';?>"><i class="fas fa-file-pdf"></i>Analyse de risque vidésurveillance (FO06)</a>
        </div>
        <div class="col-md-4 col-12">
            <a class="link-cub" target="_blank" href="<?php echo PDF_DIR.'FO_09_CONTROLE_INSTALLATION_CYBER.pdf';?>"><i class="fas fa-file-pdf"></i>Ctr installation Cyber Sécurité (FO09)</a>
        </div>      
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3 col-12">
            <a class="link-cub" href="<?php echo site_url();?><?php echo $_SESSION['user']->role;?>"><span class="fa fa-backward"></span>Retour accueil</a>
            </div>
        </div>
    </div>