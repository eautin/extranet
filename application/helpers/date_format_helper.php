<?php

if (!function_exists('readableDate')){

	function readableDate( $datetime ){
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
		$date->setTimezone(new DateTimeZone('Europe/Paris'));
		return $date->format('l d/m/Y H:i:s');
	}
}



