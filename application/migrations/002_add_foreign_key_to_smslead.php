<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_foreign_key_to_smslead extends CI_Migration
{

	public function up( )
	{
		$field = array(
			'cp' => array(
				'type' => 'VARCHAR',
				'constraint' => '6',
				'unsigned' => true,
			)
		);
		$this->dbforge->add_column('sms_lead', $field);

	}

	public function down( )
	{
		$this->dbforge->drop_column('cp');
	}
}

