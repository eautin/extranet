<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_smslead_table extends CI_Migration
{

	public function up( )
	{


	/*	$this->dbforge->add_field("id INT NOT NULL UNSIGNED AUTOINCREMENT");
		$this->dbforge->add_field("lead_id INT UNSIGNED");
		$this->dbforge->add_field("name varchar(100) NULL");
		$this->dbforge->add_field("tel varchar(100) NULL");
		$this->dbforge->add_field("msg varchar(100) NULL");
		$this->dbforge->add_field("type_lead varchar(100) NULL");
		$this->dbforge->add_field("url varchar(100) NULL");
	/*
	 *
	 *
	 *
*/$fields = array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => true,
				'auto_increment' => true,
			),
			'lead_id' => array(
				'type' => 'INT',
				'constraint' => '11',
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			),
			'tel' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			),
			'type_lead' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			),
			'url' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			),

		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('sms_lead');
		$this->dbforge->add_column('sms_lead',[
			'CONSTRAINT fk_id FOREIGN KEY(lead_id) REFERENCES leads_full(id)',
		]);
		//$this->db->query('ALTER TABLE `sms_lead` ADD FOREIGN KEY(`lead_id`) REFERENCES `leads_full`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
	}

	public function down()
	{
		$this->dbforge->drop_table('sms_lead');
	}
}
