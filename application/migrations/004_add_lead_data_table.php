<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_lead_data_table extends CI_Migration
{

	public function up( )
	{

		$field = array(
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			),
			'street' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			),
			'city' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			)
		);
		$this->dbforge->add_column('sms_lead', $field);

	}

	public function down( )
	{
		$this->dbforge->drop_table('lead_data');
	}
}
