<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_column_msg_smslead_table extends CI_Migration
{

	public function up( )
	{
		$field = array(
			'msg' => array(
				'type' => 'VARCHAR',
				'constraint' => '1000',
				'null' => TRUE,
			)
		);
		$this->dbforge->add_column('sms_lead', $field);

	}

	public function down( )
	{
		$this->dbforge->drop_column('msg');
	}
}

