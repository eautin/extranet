<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Superadmin
 *
 * @author emman
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Superadmin extends MY_Controller {
    
    public function __construct() {
        
        parent::__construct();
        
        $this->load->model('superadmin_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
     
    }
    
    
    public function index(){
               $this->load->view('templates/header');
               $this->load->view('admin/superadmin/index');
               $this->load->view('templates/footer'); 
    }
    
    public function add_user( ) {
        
        // ajouter validation form 
        
        $user = [ 
        "nom" => $this->input->post('adduser_name'),
        "prenom" => $this->input->post('adduser_prename'),
        "email" => $this->input->post('adduser_email'),
        "role" => $this->input->post('adduser_role'),
        "societe" => $this->input->post('adduser_societe'),
        "user_login" => $this->input->post('adduser_login'),
        "user_password"=> password_hash($this->input->post('adduser_pwd'),PASSWORD_DEFAULT)
         ];
        
            if($this->superadmin_model->insert_user($user)){
            
                $data = [
                    "msg" => "Utilisateur ajouté avec succès"
                ];
                
                    $this->load->view('templates/header');
                    $this->load->view('admin/superadmin/adduser_success', $data);
                    $this->load->view('templates/footer'); 
             
                }else{
            
                $data = [
                    "msg" => "Une erreur s'est produite, veuillez réessayer."
                ];
            
                    $this->load->view('templates/header');
                    $this->load->view('admin/superadmin/adduser_error', $data);
                    $this->load->view('templates/footer'); 
            
                }
    }
}
