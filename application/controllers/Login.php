<?php


/**
 * Controller login 
 * permettant le login et la redirection de l'utilisateur en fonction de son/ses rôles
 * @author emmanuel autin
 */ 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
    
    protected $data = [];
    
     public function __construct( ) {
        
        parent::__construct();
        $this->load->model('Login_model');
        $this->data = $this->data_ca( );

    }
    
    /**
     * Fonction démarre et set la session utilisateur
     *
    */
    public function start_session($valid_user, $user_data){
        
                  $this->session->set_userdata('user', $valid_user);
                  $this->session->set_userdata($user_data);
                  $this->session->set_userdata('is_logged', TRUE);
                  $this->session->set_userdata('societe', $valid_user->societe);
                  $this->session->set_userdata('role', $valid_user->role);
    }
    
    /**
     * Fonction de deconnexion
     * Détruit la session et renvoi sur la page de login
    */
    public function logout( ) {
        
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_password');
        $this->session->unset_userdata('is_logged');
        $this->session->sess_destroy( );
        $this->output->delete_cache( );
        redirect('login','index');
        
    }
    
    
        /**
	* Fonction d'affichage de la page de login
	* Charge également le panneau du résumé du chiffre d'affaire via parent
        * Détruit la session en cours
        */
    public function index( ) {


		/*
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_password');
        $this->session->unset_userdata('is_logged');
        $this->session->sess_destroy();*/

		$this->data['session'] = false;
		if(!$this->session->userdata('user_name') == ""){
			$this->data['session'] = true;
		}

        //$this->load->view('templates/header');
        $this->load->view('login/index',$this->data);
        //$this->load->view('templates/footer');
    }
    
    /**
	* Fonction de validation du couple login / mot de passe
	* @return $_SESSION['valid_user'] si valide et redirige vers la page connectée
        * en fonction du rôle utilisateur 
        * si erreur, rechargement  
	*/
    public function validate_login() {

        $this->form_validation->set_rules('userLogin', 'nom utilisateur','trim|required');
        $this->form_validation->set_rules('userPwd', 'password','trim|required');
        
        if ($this->form_validation->run() === FALSE)
        {
          // login error 
            $this->load_login_page( );
        
        }
        else
        {

            $user_data = [
                "user_name" => $this->input->post('userLogin'),
                "user_password" => $this->input->post('userPwd')
                ];
            
            $valid_user = $this->Login_model->login_validator($user_data);
           
           
            if($valid_user) {
      
                  $this->start_session($valid_user, $user_data);
                  $main_role = $valid_user->role;
                  $roles = $valid_user->roles;
                  $this->redirect_role( $main_role, $roles);
                
                  
            }else{
                
                $data["error"] = "Votre login ou mot de passe est INVALIDE, veuillez réessayer.";
                $this->load_login_page();
            
            }
        }
    }
    
    /**
     * Fonction permettant de réafficher afficher
     * la page de login après une soumission erronnée
     *
    */
    public function load_login_page( ){
            
            //$this->load->view('templates/header');
            $this->load->view('login/index', $this->data);
            //$this->load->view('templates/footer');
    }

     /**
     * Méthode récupérant les rôles
     *
    */
    public function select_role( ){
        
        $arr = [];
        $this->redirect_role($_POST['role'], $arr);
        
    }
    
    
    
    /**
     * Méthode redirgigeant l'utilisateur en fonction  de son role principal et ses
     * roles secondaires
     * @param  string $main_role rôle principal utilisateur
     * @param array $role les rôles utilisateurs
     *
    */
    
    public function redirect_role( $main_role, $roles ){
         
        if(count($roles) > 1){
           
            $this->load_role_page($roles);
            
        }else{
            
             switch($main_role){
                      
                      case 'technique': redirect('technique','index');
                      break;
                      case 'commercial': redirect('commercial','index');
                      break;
                      case 'supercommercial': redirect('supercommercial','index');
                      break;
                      case 'superadmin': redirect('supercommercial','index');
                      break;
                      case 'compta': redirect('compta','index');
                      break;
                      case 'commercial_tsip': redirect('commercial_tsip','index');
                      break;
                      case 'direction': redirect('direction','index');
                      break;
                      default: 
                          $data = ["error" => "Votre login ou mot de passe est INVALIDE, veuillez réessayer."];
                          $this->load_login_page($data);
                      
                  }
        }
         
    }
    
  
     /**
     * Méthode redirgigeant l'utilisateur si utilisateur assigné a plusieurs rôles
     * @param  array $roles lesrôles utilisateurs
     * @return null
     *
    */
    
    public function load_role_page($roles){
        
        
        $CI = &get_instance( );
        $CI->load->model('Role_model');
        
        $arr_roles = [ ];
        foreach($roles as $role){
           
            $role = $CI->Role_model->get_role_by_id($role['roleid']);
            
            array_push($arr_roles,$role);
        }
        
        $data['roles'] = $arr_roles;
        $this->load->view('templates/header');
        $this->load->view('login/choose_role',$data);
        $this->load->view('templates/footer');
        
    }
}
