<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supercommercial extends MY_Controller {
    
    private $db_prefix = "leads_";
    private $_email_technique = "technique@groupsecuricom.fr";
    private $_email_administratif = "administratif@groupsecuricom.fr";
    private $_email_webmarketing = "dsi@groupsecuricom.fr";
    private $_email_direction = "direction@groupsecuricom.fr";
    public $_credit_sms;
    
    public $_securicom_role_id = COMMERCIAL_ROLE_ID;
    public $_tsip_role_id = TSIP_ROLE_ID;

	public $_send_mail_commercial = false;
	public $_send_mail_admin = false;
	private $_send_sms_commercial = false;
	private $_accept = "accept";
    
      public function __construct( ) {
        
        parent::__construct( );
        $this->load->library('pagination');
        $this->load->model('supercommercial_model');

		$this->load->model('sms_lead_model');
        $this->load->library('email');
        $this->load->library('smsenvoi');
        $sms_credit = new Smsenvoi( );
        $total = $sms_credit->checkCredits( );
        $total_credit = $total['sms']['PREMIUM'];
        $this->credit_sms = $total_credit;

    }
    
    
    public function get_num_pagination( ){
        
        $rows = $this->supercommercial_model->get_num_rows_leads( );
        return $rows;

    }
    
    public function set_commercial_to_lead($leads){
        
        foreach($leads as $lead){
           
                $lead->commercial_name = $this->supercommercial_model->get_commercial_by_id($lead->commercial_id);
        }
        
        return $leads;
    }


		private function checkNotify( ){

			if(
				isset($_POST['send_email_commercial'])
				&& $_POST['send_email_commercial'] == $this->_accept)
			{
				$this->_send_mail_commercial = TRUE;
			}

			if(
				isset($_POST['send_email_administratif'])
				&& $_POST['send_email_administratif'] == $this->_accept)
			{
				$this->_send_mail_admin = TRUE;
			}
			if(
				isset($_POST['send_sms_commercial'])
				&& $_POST['send_sms_commercial'] == $this->_accept)
			{
				$this->_send_sms_commercial = TRUE;
			}

		}
    
    
      public function index( ) {

        $data = parent::data_ca( );
        $config['base_url'] = base_url( ).'index.php/supercommercial/index';
        $config['total_rows'] = $this->get_num_pagination( );
        $total_leads = $config['total_rows'];
        $config['per_page'] = 10;
        $config['num_links'] = $config['total_rows'];
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $config['next_link'] = 'Suivant';
        $config['prev_link'] = 'Precedent';
        $config['uri_segment'] = 3;
        $data['commerciaux'] = $this->supercommercial_model->get_commerciaux_new( );
        
        $data['credit_sms'] = $this->credit_sms;
        $data['full_leads'] = $this->supercommercial_model->get_current_leads(10, $start_index);

        if($data['full_leads'] != false){
			$this->set_commercial_to_lead($data['full_leads']);
		}

        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links( );
        $data['js_to_load'] = "supercommercial/index/index.js";
        $this->load_single_template($data, 'index');
              
        }
        
        public function other_service( ){
           
            $service = $_POST['service'];
            $lead_id = $_POST['id'];

            switch($service) {
                case "technique":
                    $email = $this->_email_technique;
                    break;
                case "administratif":
                    $email = $this->_email_administratif;
                    break;
                case "webmarketing":
                   $email = $this->_email_webmarketing;
                    break;
                case "direction": 
                    $email = $this->_email_direction;
                    break;
            }
            
            $type ="full";
            $lead_data = $this->supercommercial_model->get_lead_by_type($type, $lead_id);
               
            if($this->notify_service($email,$lead_data,$service)){
                
                $lead_data->statut = "demande non commerciale";
                
                if($this->supercommercial_model->update_lead( $lead_data )){
                          log_message("error", "maj du lead ".$lead_id." et assignation par mail au service ".$service);
                          $data['message'] = "Demande transfere. Le service ".$service." a recu un email.";
                          $this->load_success_template( $data );  
                
                }else{
                     log_message("error", "echec du transfert au service".$service);
                     $data['error'] = 'Echec du transfert la notification au service '.$service.'.';
                     $this->load_error_template( $data );
                }
                
            }else{
               log_message("error", "echec du transfert au service".$service);
               $data['error'] = 'Echec de la notification au service '.$service.' , veuillez reessayer.';
               $this->load_error_template( $data );
            }
            
        }
        
        public function load_single_template($data,$template){
             $data['credit_sms'] = $this->credit_sms;
             $this->load->view('templates/header',$data);
             $this->load->view('admin/supercommercial/'.$template, $data);
             $this->load->view('templates/footer');
        }
        
         public function edit_lead($type, $id){

		   	$lead_data = $this->sms_lead_model->get_sms_lead_data($id);
            $data['js_to_load'] = "supercommercial/edit_lead/edit_lead.js";
            $data['css_to_load'] = "jquery.datetimepicker.min.css";
            $data['lead'] = $this->supercommercial_model->get_lead_by_type($type, $id);
            $data['status_id'] = $this->supercommercial_model->get_status_by_name($data['lead']->statut);
            $data['commerciaux'] = $this->prepare_dropdown($this->supercommercial_model->get_commerciaux_new());
            $data['status_lead'] = $this->supercommercial_model->get_status();
            $data['type'] = $type;
			$data['smsdata'] = $lead_data;
            $this->load_edit_template($data);

        }
       
        public function prepare_dropdown($datas){ 
            $arr_data = [];
            foreach($datas as $data){
                $arr_data[$data->id] = $data->nom." ".$data->prenom;
            }
            $arr_data['0'] = "Non renseigné";
            return $arr_data;
        }
        
        public function load_edit_template($lead) {
             $lead['credit_sms'] = $this->credit_sms;
             $this->load->view('templates/header',$lead);
             $this->load->view('admin/supercommercial/edit_lead', $lead);
             $this->load->view('templates/footer');
        }
        
        
        public function compare_com($new,$old){
            
           if($new === $old){
               return FALSE;
           }
               return TRUE;
        }
        
        public function update_lead( ){

		  // VALIDATION

			$this->load->library('form_validation');

			$config = array(
				array(
					'field' => 'prospect_name',
					'label' => 'Nom du prospect',
					'rules' => 'required'
				),
				array(
					'field' => 'prospect_tel',
					'label' => 'Téléphone du prospect',
					'rules' => 'required',
				),
				array(
					'field' => 'prospect_email',
					'label' => 'Email du prospect',
					'rules' => 'required'
				),
				array(
					'field' => 'prospect_city',
					'label' => 'Ville du prospect',
					'rules' => 'required'
				),
				array(
					'field' => 'prospect_cp',
					'label' => 'Code Postal du prospect',
					'rules' => 'required'
				),
				array(
					'field' => 'prospect_street',
					'label' => 'Rue du prospect',
					'rules' => 'required'
				)
			);

			$this->form_validation->set_rules( $config );

			if (!$this->form_validation->run())
			{

				$this->edit_lead('full',$_POST['id_lead']);
				return;
			}


			// check si envoie mail admin + sms commercial et mail commercial
			$this->checkNotify( );
            //recupere maj lead;
           $lead_update = $_POST;
           $new_com_id = $_POST['commercial'];


           
            //if archive
            if($_POST['statut'] === "10"){
                if($this->supercommercial_model->update_lead($lead_update)){
                    
                            $data['message'] = "Lead archivé";
                            $this->load_success_template( $data ); 
                            return;
                }
            }
           
           
           
           //recupere ancien commercial
           $last_com_id = $this->supercommercial_model->get_lead_last_commercial_id($_POST['id']);
           $is_new_commercial = $this->compare_com($new_com_id, $last_com_id);


		  // dd($lead_update);

           if($this->supercommercial_model->update_lead($lead_update)){
               
               $lead_data = $this->supercommercial_model->get_lead_by_type($lead_update['type_lead'],$lead_update['id_lead']);
               $commercial_id = $lead_update['commercial'];

               if($commercial_id != 0){
                   
                    $commercial_email = $this->supercommercial_model->get_commercial_email($lead_update['commercial']);
                    $commercial_email = $commercial_email->email;
                    
                    $commercial_num = $this->supercommercial_model->get_commercial_phone($commercial_id);
                    $commercial_num = $commercial_num->user_tel;
                    
                 	// OLD WAY
               //     $data_sms = $lead_data->informations;

					$sms_lead_data = $this->sms_lead_model->get_sms_lead_data($lead_update['id_lead']);

				   	$data_sms = "Rapp: " . $sms_lead_data['name'] . " au: " . $sms_lead_data['tel'] . " Msg: " . $sms_lead_data['msg'];

                    $sms_max_size = 159;
                    
                    if(strlen($data_sms) >= $sms_max_size){
                        $data_sms = substr($data_sms,0,$sms_max_size);
                    }

                    if($is_new_commercial){
						//$notify =  $this->notify_lead('emmanuel.autin@gmail.com',$lead_data);
                      	$notify =  $this->notify_lead($commercial_email,$lead_data);
                        $type = "PREMIUM";
                        $sender_label = "SECURICOM";

						//$sms = $this->notify_by_sms('0770503800', "I Love you Shoon.. Have a nice afternoon :) BizooBelle",$type, 'Ton Noors');

						$sms = false;
						if($this->_send_sms_commercial){
							$sms = $this->notify_by_sms($commercial_num, $data_sms,$type, $sender_label);
						}

                        if($notify && $sms){
                            log_message("error", "MAJ du lead ".$lead_update['id_lead']." SMS & mail");
                            $data['message'] = "Succes de la mise a jour. SMS et Mail envoye au commercial.";
                            $this->load_success_template( $data ); 
                          
                        }elseif(!$notify && $sms){
                           
                            $data['error'] = "Mise a jour du lead ".$lead_update['id_lead']."  effectue, cependant:  SMS envoye. Echec notification email";
                            $this->load_error_template( $data ); 
                            log_message("error", "la fonction notify_lead retourne false");
                        }elseif(!$sms && $notify){
                            
                            $data['error'] = "Mise a jour du lead ".$lead_update['id_lead']."  effectue, cependant: Echec envoi sms. Mail envoye";
                            $this->load_error_template( $data ); 
                             log_message("error", "la fonction notify_by_sms retourne false");
                        }elseif(!$sms && !$notify){
                            
                            $data['error'] = "Mise a jour du lead ".$lead_update['id_lead']."  effectue, cependant: Echec sms. Echec envoi mail";
                            $this->load_error_template( $data ); 
                            log_message("error", "la fonction notify_lead et notify_by_sms retourne false");
                        }
                    }else{
                        
                        $data['message'] = "Mise a jour du lead ".$lead_update['id_lead']."  effectue. Le commercial assigne n'est pas notifie.";
                        $this->load_success_template( $data ); 
                    }
                    
               }else{
                    $data['credit_sms'] = $this->credit_sms;
                        $data['message'] = "Mise a jour du lead ".$lead_update['id_lead']."  effectue. Cependant, aucun commercial n'est encore assigne au lead";
                        $this->load_success_template( $data ); 
               }

            }else{
                
               $data['error'] = 'Echec de la maj du LEAD (update lead), veuillez reessayer.';
               $this->load_error_template( $data );
               log_message("error", "echec de la function update_lead sur lead  ".$lead_update['id_lead']);
            }
        }
        

        public function notify_by_sms($commercial_num, $data_sms, $type, $sender_label){
            
            if(ACTIVE_SMS){

                $sms_credit = new Smsenvoi( );
                $total = $sms_credit->checkCredits( );
                $total_credit = $total['sms']['PREMIUM'];
                          
                    if($total_credit != 0){
						$this->send_sms($commercial_num, $data_sms, $type, $sender_label);
                        return TRUE;
                    }
                        return FALSE;
            }
                return FALSE;
        }


		public function clean_date( $date ){

			setlocale(LC_ALL, 'fr_FR.UTF8', 'fr_FR','fr','fr','fra','fr_FR@euro');
			return strftime("%A %d %B %Y %H:%M:%S", strtotime($date));
		}
              
        public function notify_lead($commercial_email,$lead_data) {

            $from_email = "dsi@groupsecuricom.fr";

			$config = [
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html'
			];

			$this->email->initialize($config);


            $this->email->from($from_email, 'Web Securicom');
            $this->email->to($commercial_email);

			$this->email->bcc('eautin@groupsecuricom.com');

			if($this->_send_mail_admin){
				$this->email->cc('cschejbalt@groupsecuricom.com');
			}

            $this->email->subject('Nouveau RDV lead web');


			$data['notes'] = $lead_data->commentaire;
			$data['commercial_name'] = $lead_data->commercial_name;
			$data['date_demande'] = $this->clean_date($lead_data->date_demande);
			$data['date_rdv'] = $this->clean_date($lead_data->date_rdv_confirme);

			if(is_array($lead_data)){
				$id = $lead_data['id'];
			}else{
				$id = $lead_data->id;
			}
			$data['sms_lead'] = $this->sms_lead_model->get_sms_lead_data($id);

			$message = $this->load->view('emails/notify_lead_template',$data, TRUE);

            $this->email->message($message);

			if($this->_send_mail_commercial){
				if($this->email->send()){
					return TRUE;
				}
			}

                return FALSE;
        }
        
        
        public function notify_service($email,$lead_data,$service){
            
            $from_email = "cgourio@groupsecuricom.fr"; 
            $this->email->from($from_email, 'Nouvelle demande web pour le service '.$service);
            $this->email->to($email);
            $this->email->subject('Nouvelle demande web pour le service '.$service);
            $this->email->message
('Bonjour,
une demande web a été réalisée concernant votre service:
Date de la demande : '.$lead_data->date_demande.'
Message : '.$lead_data->informations.'
Merci de bien vouloir traiter cette demande rapidement.');
            if($this->email->send()){
                return TRUE;
            }else{
                return FALSE;
            }
            
        }
        
        public function send_sms($phone_num, $message, $type, $sender_label){
            
            $smsenvoi = new smsenvoi();
            //$smsenvoi->debug = true;
            
            if($smsenvoi->sendSMS($phone_num, $message, $type, $sender_label)){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        
        public function load_success_template( $data ) {
             $data['credit_sms'] = $this->credit_sms;
             $this->load->view('templates/header',$data);
             $this->load->view('admin/supercommercial/success', $data);
             $this->load->view('templates/footer');
        }
        
        public function load_error_template( $error ){
             $data['credit_sms'] = $this->credit_sms;
             $this->load->view('templates/header',$data);
             $this->load->view('admin/supercommercial/error', $error);
             $this->load->view('templates/footer');
        }
        
        public function view_recap( ){
            
             $CI = & get_instance( );
             $CI->load->model('Curl_handler_model');
             //$com_ids =  $CI->Curl_handler_model->get_commerciaux_id_array_active_inactive( );
             $com_ids = $CI->Curl_handler_model->get_commerciaux_id_array_no_deleted( );



			// FIX CRADE POUR AJOUTER CGOURIO DANS LE RECAPITULATIF LEADS
             	array_push($com_ids, 50);



             $CI->load->model('Lead_model');
             
             $arr_data = [];
             
             foreach($com_ids as $id){
                 
                 array_push($arr_data, array(
                     
                     'commercial_name' => $com_name = $this->supercommercial_model->get_commercial_by_id( $id ), 
                     'a_traiter' => $this->Lead_model->get_lead_by_statut_and_com_id('a traiter', $id),
                     'rappele_ne_donne_pas_suite' => $CI->Lead_model->get_lead_by_statut_and_com_id('rappelé ne donne pas suite', $id),
                     'rappele_rdv_pris' => $CI->Lead_model->get_lead_by_statut_and_com_id('rappelé rdv pris', $id),
                     'devis_en_cours' => $this->Lead_model->get_lead_by_statut_and_com_id('devis en cours', $id),
                     'devis_signe' => $CI->Lead_model->get_lead_by_statut_and_com_id('devis signé', $id),
                     'devis_ne_donne_pas_suite' => $CI->Lead_model->get_lead_by_statut_and_com_id('devis ne donne pas suite', $id),
                     'demande_non_commerciale' => $CI->Lead_model->get_lead_by_statut_and_com_id('demande non commerciale', $id),
                     'rdv_ne_donne_pas_suite' => $CI->Lead_model->get_lead_by_statut_and_com_id('devis ne donne pas suite', $id)
                     )
                         );
             }
             
             $data['infos_leads'] = $arr_data;
             
             $CI->load->model('Commercial_model');
             $data['best_commercial'] = $CI->Commercial_model->get_commercial_by_lead_max_statut( 'devis signé' );
             $this->load->view('templates/header');
             $this->load->view('admin/direction/index', $data);
             $this->load->view('templates/footer');
            
        }
        
        
        public function gestion_com( ){
            
        //     $data['commerciaux'] = $this->supercommercial_model->get_commerciaux( );
             $data['commerciaux'] = $this->supercommercial_model->get_active_coms( );
             
             $data['credit_sms'] = $this->credit_sms;
             $this->load->view('templates/header',$data);
             $this->load->view('admin/supercommercial/gestion_com', $data);
             $this->load->view('templates/footer');
        }
        
        
        public function on_off_commercial( ){
            
            $update_active = $_POST;
            if($this->supercommercial_model->update_active($update_active)){
                
                    $data['commerciaux'] = $this->supercommercial_model->get_active_coms( );
                    $data['credit_sms'] = $this->credit_sms;
                    $data['success'] = true;
                    $this->load->view('templates/header',$data);
                    $this->load->view('admin/supercommercial/gestion_com', $data);
                    $this->load->view('templates/footer');
            }
                
            
        }
        
        
        public function add_commercial_view( ){
            
                    $data['commerciaux'] = $this->supercommercial_model->get_commerciaux( );
                    $data['credit_sms'] = $this->credit_sms;
                    
                    $this->load->view('templates/header',$data);
                    $this->load->view('admin/supercommercial/add_commercial', $data);
                    $this->load->view('templates/footer');
        }
        
        
        public function add_commercial( ) {
        
        $this->form_validation->set_rules('addcom_name', 'Nom', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('addcom_prename', 'Prenom', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('addcom_login', 'Login', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('addcom_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('addcom_phone', 'TÃ©lÃ©phone', 'required|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('addcom_pass', 'Password', 'trim|required|min_length[5]');
        

        // POST DATE ADD_COMMERCIAL 
        $user = [ 
        "nom" => strtoupper($this->input->post('addcom_name')),
        "prenom" => $this->input->post('addcom_prename'),
        "email" => $this->input->post('addcom_email'),
        "role" => 'commercial',
        "societe" => 'securicom',
        "user_tel" => $this->input->post('addcom_phone'),
        "user_login" => $this->input->post('addcom_login'),
        "user_password"=> password_hash($this->input->post('addcom_pass'),PASSWORD_DEFAULT),
        "active" => $this->input->post('addcom_active')
         ];
        
        if($this->form_validation->run() === TRUE ){
            
            //check if user already exists
            
            $already_exists = $this->supercommercial_model->check_user($user);
            
            if(!$already_exists){
                //
            
            // INSERT DB 
            if($this->supercommercial_model->insert_commercial($user)){
            
                $data = [
                    "msg_success" => "Ajout commercial ok, demandez à l'administrateur d'activer le commercial."
                ];
                
                    $this->load->view('templates/header');
                    $this->load->view('admin/supercommercial/add_commercial', $data);
                    $this->load->view('templates/footer'); 
             
                }else{
            
                $data = [
                    "msg_error" => "Une erreur s'est produite, veuillez réessayer."
                ];
                
                }
                
            }else{
                $data = [
                    "msg_error" => "l'utilisateur existe déjà en base de donnée"
                    ];
                    
                    $this->load->view('templates/header');
                    $this->load->view('admin/supercommercial/add_commercial', $data);
                    $this->load->view('templates/footer'); 
            }
                
                }else{
            
                    $data = [
                    "msg_error" => "Donnees non valides:"
                    ];
                    
                    $this->load->view('templates/header');
                    $this->load->view('admin/supercommercial/add_commercial', $data);
                    $this->load->view('templates/footer'); 
            
                }
                
        
                
        }
       
    public function view_commercial_month_panel( ){
        
		  $data['current_year'] = date('Y');
		  $data['current_month'] = date('m');
                    $this->load->view('templates/header');
                    $this->load->view('admin/supercommercial/view_commercial_month_panel',$data);
                    $this->load->view('templates/footer'); 
        
    }
      
    public function select_month_year( ){
        
        $data['year'] = $_POST['year'];
        $data['month'] = $_POST['month'];
        
        $date  = $data['year'].'-'.$data['month'].'-01';
        $data['date_time'] = $date;
        
        $data['commerciaux'] = $this->supercommercial_model->get_commerciaux_and_ca_by_month_year( $date, $this->_securicom_role_id );
        
        $data['tsip_commercial'] = $this->supercommercial_model->get_commerciaux_and_ca_by_month_year( $date, $this->_tsip_role_id );
        
        $data['js_to_load'] = "supercommercial/view_commercial_month_panel/ajax_actions.js"; 
        $this->load->view('templates/header',$data);
        $this->load->view('admin/supercommercial/view_full_month',$data);
        $this->load->view('templates/footer',$data); 
        
    }
    
    public function validate_month_ca( $data, $date){
        
        foreach ($data as $commercial){
           
             //var_dump($commercial);
             $total_vente = (int)$commercial['total_vente'];
             $total_loc = (int)$commercial['total_loc'];
             $total_devis = (int)$commercial['total_devis'];
             
             $id = $commercial['id'];
             $this->supercommercial_model->save_month_ca($id, $total_loc, $total_vente, $total_devis, $date);
        }
                
    }
    
    public function ajax_save_ca( ){
        
        if(isset($_POST['chiffre'])){
            
            $date = $_POST['date'];
            $this->validate_month_ca($_POST['chiffre'], $date );
            
            $msg = "Mise à jour avec succès des données";
            
                $response = json_encode($msg);
                echo $response;
        }else{
                $message = array('une erreur est survenue dans la sauvegarde en base de donnée');
                $response = json_encode($message);
                echo $response;
        }
                
    }
    
    public function ajax_save_ca_tsip( ){
        
        if(isset($_POST['chiffre'])){
            
            $date = $_POST['date'];
            $this->validate_month_ca($_POST['chiffre'], $date );
            
            $msg = "Mise à jour avec succès des données";
            
                $response = json_encode($msg);
                echo $response;
        }else{
                $message = array('une erreur est survenue dans la sauvegarde en base de donnée');
                $response = json_encode($message);
                echo $response;
        }
                
    }
 
    
}
