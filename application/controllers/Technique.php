<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Technique
 *
 * @author emman
 */

if ( ! defined('BASEPATH')) exit( 'No direct script access allowed' );

class Technique extends MY_Controller {
    
        protected $_entreprise;
        public $_credit_sms;
        //put your code here
        public function __construct() {
        
        parent::__construct( );
        
        // load db model :
        $this->load->library('session');
        $this->_entreprise = $this->session->userdata['societe'];
        $this->load->model('technique_model');
        
        // load helper and libs :
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('smsenvoi');
        
       
        $sms_credit = new Smsenvoi( );
        $total = $sms_credit->checkCredits( );
        $total_credit = $total['sms']['PREMIUM'];
        $this->credit_sms = $total_credit;
        
    } 
    
    public function index( ){

        $data['credit_sms'] = $this->credit_sms;
        $this->load->view('templates/header',$data);
        $this->load->view('admin/technique/securicom/index');
        $this->load->view('templates/footer'); 
        
    }
    
    public function sms( ){
        
        $data['credit_sms'] = $this->credit_sms;
        $data['js_to_load'] = 'technique/sms/sms.js';
        $data['css_to_load'] = 'jquery.datetimepicker.min.css';
        $data['archive_sms'] = $this->technique_model->get_archive_sms();
        $this->load->view('templates/header', $data);
        $this->load->view('admin/technique/securicom/sms',$data);
        $this->load->view('templates/footer'); 
    }
    
    public function dashboard( $message = false ){
        
        if($message){
            $data['message'] = $message;
        }
        
        $data['credit_sms'] = $this->credit_sms;
        $data['js_to_load'] = "technique/dashboard/dashboard.js";
        $data['css_to_load'] = 'jquery.datetimepicker.min.css';
        $data['installs'] = $this->technique_model->get_installs_commercial( );
        $data['commerciaux'] = $this->prepare_dropdown($this->technique_model->get_commerciaux( ));
        $this->load->view('templates/header', $data);
        $this->load->view('admin/technique/securicom/dashboard');
        $this->load->view('templates/footer'); 
    }
      
    public function techniciens( ) {
        $data['credit_sms'] = $this->credit_sms;
        $data['js_to_load'] = "technique/techniciens/techniciens.js";
        $this->load->view('templates/header',$data); 
        $this->load->view('admin/technique/'.$this->_entreprise.'/techniciens');
        $this->load->view('templates/footer',$data);
        
    }
    
    
    public function page_archive_install(){
        $data['credit_sms'] = $this->credit_sms;
        $data['js_to_load'] = "technique/dashboard/dashboard.js";
        $data['css_to_load'] = 'jquery.datetimepicker.min.css';
        $data['installs'] = $this->technique_model->get_archives_commercial( );
        $data['commerciaux'] = $this->prepare_dropdown($this->technique_model->get_commerciaux( ));
        $this->load->view('templates/header', $data);
        $this->load->view('admin/technique/securicom/archiveboard');
        $this->load->view('templates/footer'); 
    }
    
    public function interventions( ) {
        $data['credit_sms'] = $this->credit_sms;
        $data['js_to_load'] = "technique/interventions/interventions.js";
        $data['css_to_load'] = "calendar.css";
        $this->load->view('templates/header',$data); 
        $this->load->view('admin/technique/'.$this->_entreprise.'/interventions');
        $this->load->view('templates/footer', $data);
    }
    
    public function cumuls( ) {
        $data['js_to_load'] = "technique/cumuls/cumuls.js";
        $data['css_to_load'] = "calendar.css";
        $data['credit_sms'] = $this->credit_sms;
        $this->load->view('templates/header',$data); 
        $this->load->view('admin/technique/'.$this->_entreprise.'/cumuls');
        $this->load->view('templates/footer',$data);
    }
    
    public function vols( ) {
        $data['js_to_load'] = "technique/vols/vols.js";
        $data['credit_sms'] = $this->credit_sms;
        $this->load->view('templates/header',$data); 
        $this->load->view('admin/technique/'.$this->_entreprise.'/vols');
        $this->load->view('templates/footer',$data);
    }
    
    public function load_tpl_install($message){
        
        $data['message'] = $message;
        $data['credit_sms'] = $this->credit_sms;
        $this->load->view('templates/header', $data);
        $this->load->view('admin/technique/securicom/dashboard_add_install');
        $this->load->view('templates/footer'); 
    }
    
    public function send_sms($phone_num, $message,$type,$societe){
            
         if(ACTIVE_SMS){
             
            $smsenvoi = new smsenvoi( );
            $smsenvoi->debug = true;
            
            if($smsenvoi->sendSMS($phone_num, $message,$type,$societe)){
                return TRUE;
            }else{
                return FALSE;
            }
            
        }else{
               
                return FALSE;
        }
    }
    
    public function prepare_dropdown($datas){ 
            $arr_data = [];
            foreach($datas as $data){
                $arr_data[$data->id] = $data->nom." ".$data->prenom;
            }
            $arr_data['0'] = "Non renseigné";
            return $arr_data;
        }
    
   
    public function get_installs( ){

        return $this->technique_model->get_installs( );
               
    }
    
    public function prepare_sms( ){
        
        //var_dump($_POST);
        
        $data_sms = [   
            'select_societe' => $_POST['select_societe'],
            'select_type' => $_POST['select_type'],
            'nom_client' => $_POST['nom_client'],
            'tel_client' => $_POST['tel_client'],
            'date_rdv' => $_POST['date_rdv'],
            'msg_client'=> $_POST['msg_client']
        ];
        
        
        $datetime = new DateTime($_POST['date_rdv']);

        $dateRDV = $datetime->format('Y-m-d');
        
       // var_dump($date);
            
        $timeRDV = $datetime->format('H:i:s');
        
      // var_dump($time);
        
        $this->load->helper(array('form', 'url'));
        
        $this->form_validation->set_rules('select_societe', 'société', 'required');
        $this->form_validation->set_rules('select_type', 'type de prestation', 'required');
        $this->form_validation->set_rules('nom_client', 'nom du client', 'required');
        $this->form_validation->set_rules('tel_client', 'tel du client', 'required|max-length[10]');
        $this->form_validation->set_rules('date_rdv', 'date de rdv', 'required');
        $this->form_validation->set_rules('msg_client', 'message pour client', 'trim|max-length[100]');
        
        if ($this->form_validation->run() == FALSE)
        {
            $data['credit_sms'] = $this->credit_sms;
            $data['js_to_load'] = "technique/sms/sms.js";
            $data['css_to_load'] = 'jquery.datetimepicker.min.css';
            $data['archive_sms'] = $this->technique_model->get_archive_sms( );
            $this->load->view('templates/header',$data); 
            $this->load->view('admin/technique/'.$this->_entreprise.'/sms',$data);
            $this->load->view('templates/footer',$data);
        }
        
        else
        {
         
            $message = "Bonjour nous vous confirmons notre rdv pour votre ".$data_sms['select_type']." le ".$dateRDV.' à partir de '.$timeRDV.'. '.$data_sms['msg_client'];
            if($this->send_sms($data_sms['tel_client'], $message, 'PREMIUM',$data_sms['select_societe'])){
                
                 $this->technique_model->save_sms($data_sms);
                 $data['credit_sms'] = $this->credit_sms;
                 $data['message'] = "Votre SMS a été correctement envoyé.";
                 $this->load->view('templates/header',$data); 
                 $this->load->view('admin/technique/'.$this->_entreprise.'/sms_success');
                 $this->load->view('templates/footer',$data);
            }else{
                 $data['credit_sms'] = $this->credit_sms;
                 $data['message'] = "Echec de votre envoi sms, verifiez que les envois SMS sont bien activées (ACTIVE_SMS) ainsi que le format du numéro de téléphone.";
                 $this->load->view('templates/header',$data); 
                 $this->load->view('admin/technique/'.$this->_entreprise.'/sms_error');
                 $this->load->view('templates/footer',$data);
            } 
        }
        
    }
     
    public function rerouteAjax( ){

    if(isset($_POST['check_day_inter'])){

            $date = $_POST['date'];
            $response = array (
                'date' => $date,
                'statut' => "",
                'tech_data' => "",
                'tech_inter'=> ""
            );
  
            
            if($this->checkDate($date)) {

                //intervention a cette date.
               $response['statut'] = true;
               $response['tech_data'] = $this->getTechData();
               $response['tech_inter'] = $this->getDayData($date);

            }else{

                $tech_data = $this->getTechData();
                
                $response['statut'] = false;
                $response['tech_data'] = $tech_data;
                $response['tech_inter'] = null;
                
            }

               $response = json_encode($response);

               echo $response;
   }


   if(isset($_POST['savenewintervention'])){
       
      $interventions = $_POST['intervention'];
      $dayInter = $_POST['date'];
      $this->insertInterventions($interventions, $dayInter);
      
   }

   if(isset($_POST['saveeditedinformation'])){

      $interventions = $_POST['intervention'];
      $dayInter = $_POST['date'];
      $resp = $this->updateInterventions($interventions, $dayInter);
     
     $resp = json_encode($resp);
     echo 'test: '.$resp;
   }

   // pour les cumuls : 
   //si un jour cliqué
   if(isset($_POST['getDayInter'])){

       $dayStart = $_POST['dayStart'];
       $this->getDayData($date);

   }
   
   if(isset($_POST['getInterventions'])){

       $dayStart = $_POST['dayStart'];
       $dayEnd = $_POST['dayEnd'];
       $cumuls = $this->get_cumul_interventions($dayStart, $dayEnd);
       echo json_encode($cumuls);

   }

   if(isset($_POST['addpresta']) && isset($_POST['prestaname'])){

       $presta_name = $_POST['prestaname'];

       if(isset($_POST['prestamsg'])){
           $presta_msg = $_POST['prestamsg'];
       }else{
           $presta_msg = "non renseigné";
       }

       if($this->add_presta($presta_name, $presta_msg)){

           $resp = $this->get_prestas();

           echo json_encode($resp);
       }
   }


   if(isset($_POST['addtech']) && isset($_POST['techname']) && isset($_POST['techprename'])){

       $tech_data = array( );

       $tech_data['name'] = $_POST['techname'];
       $tech_data['prename'] =  $_POST['techprename'];
       $tech_data['date_creation'] = date('Y-m-d');
       $tech_data['statut'] = $_POST['statut'];
       $tech_data['entreprise'] = $_POST['entreprise'];


       if($this->add_tech($tech_data)){

           $resp = $this->get_techniciens();

           echo json_encode($resp);
       }
    
    }


        if(isset($_POST['getallpresta'])){

            $prestas = $this->get_prestas( );
            $json_prestas = json_encode($prestas);
            $prestataires = array();
            $prestataires['prestataires'] = $this->get_prestas( );

            echo json_encode($prestataires);

        }


        if(isset($_POST['getalltech'])){

            $techniciens = array();
            $techniciens['techniciens'] = $this->get_techniciens( );
            
            echo json_encode($techniciens);

        }


        if(isset($_POST['getallvols'])){

            $vols = array();
            $vols['vols'] = $this->get_vols( );
            echo json_encode($vols);
        }


        if(isset($_POST['updatevol']) && isset($_POST['data'])) {

            //var_dump($_POST['data']['idVol']);
            $vol_update = $_POST['data'];
            $resp = array( );
            $resp['resp_update'] = $this->update_vol($vol_update);
            $resp['vols'] = $this->get_vols( );

            echo json_encode($resp);
        }
    }
    
    
    public function checkDate($date) {
        
        if($this->technique_model->checkdate($date)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function getTechData(){
        
         
         $tech_data = $this->technique_model->getTechData();
         return $tech_data;
    }
    

    public function insertInterventions($interventions, $dayInter){
        
        
      if($this->technique_model->insertInterventions($interventions, $dayInter)){
          
            return true;
        }
    }
    
    
     public function updateInterventions($interventions, $dayInter){

       if($this->technique_model->updateInterventions($interventions, $dayInter)){
            return true;
        }
        
    }
    
   
    public function getDayData($day) {
        
        $dayDatas = $this->technique_model->getDayData($day);
        
        return $dayDatas; 
    }
    
    
    public function getPlageInterventions($day_start, $day_end){
        
        $interventions = $this->technique_model->get_plage_interventions($day_start, $day_end);
        return $interventions;
    }
    
    
    public function get_cumul_interventions($dayStart, $dayEnd){
        
        $cumuls['interventions'] = $this->technique_model->get_cumul_interventions($dayStart, $dayEnd);
        $cumuls['techniciens'] = $this->technique_model->get_techniciens();
        return $cumuls;
    }
    
    
    public function get_prestas(){
        
        $prestas = $this->technique_model->get_prestas();
        
        return $prestas;
    }
    
      public function get_techniciens(){
        
        $techs = $this->technique_model->get_techniciens();
        
        return $techs;
    }
    
    
    public function add_presta($presta_name, $presta_msg){
        
        
        $test = $this->technique_model->add_presta($presta_name, $presta_msg); 
        
        return $test;
    }
    
     public function add_tech($tech_data){
        
        
        $insert_tech = $this->technique_model->add_tech($tech_data); 
        
        return $insert_tech;
    }
    
    public function get_vols( ){
        
        $vols = $this->technique_model->get_vols();
        
        return $vols;
    }
    
    
    public function update_vol($vol){
        
        $update = $this->technique_model->update_vol($vol);
        
        return $update;
    }
    
    public function load_dashboard_assets(){
        
            $data['credit_sms'] = $this->credit_sms;
            $data['js_to_load'] = "technique/dashboard/dashboard.js";
            $data['css_to_load'] = 'jquery.datetimepicker.min.css';
            $data['commerciaux'] = $this->prepare_dropdown($this->technique_model->get_commerciaux());
            return $data;
    }
    
    public function form_add_install(){
        
            $data = $this->load_dashboard_assets();
            $this->load->view('templates/header',$data); 
            $this->load->view('admin/technique/'.$this->_entreprise.'/dashboard_form_add_install',$data);
            $this->load->view('templates/footer',$data);
    }
    
    public function form_edit_install($id){
        
            $data = $this->load_dashboard_assets();
            $data['install'] = $this->technique_model->get_install_by_id($id);
            $time = $this->convert_second_to_hours_days($data['install'][0]->duree);
            $data['install'][0]->jours = $time['jours'];
            $data['install'][0]->heures = $time['heures'];
            $this->load->view('templates/header',$data); 
            $this->load->view('admin/technique/'.$this->_entreprise.'/dashboard_form_edit_install',$data);
            $this->load->view('templates/footer',$data);
    }
  
    public function add_install( ){
             
        $this->load->helper(array('form', 'url'));
        $this->form_validation->set_rules('select_type', 'type de prestation', 'required');
        $this->form_validation->set_rules('nom_client', 'Raison sociale / Nom du client', 'required');
        $this->form_validation->set_rules('commercial', 'commercial', 'required');
        $this->form_validation->set_rules('date_install', 'date d\'installation', 'required');
        $this->form_validation->set_rules('jours', 'jours', 'required');
        $this->form_validation->set_rules('heures', 'heures', 'required');
        $this->form_validation->set_rules('observations', 'observations', 'trim');
 
        
        if ($this->form_validation->run( ) != FALSE)
        {
                      
            $nb_jour = (int)$_POST['jours'];
            $day_to_hours = $nb_jour * 8;
            $nb_hours = (int)$_POST['heures'];
            $nb_hours_to_sec = $nb_hours * 3600;
            $total_sec = $day_to_hours * 3600;
            
     
            $tot_hours = $day_to_hours + $nb_hours;
            $full_sec = $total_sec + $nb_hours_to_sec;
            $time = $tot_hours.':00:00'; 
            
            $data_install = [
            'type' => $_POST['select_type'],
            'date_creation' => date('Y-m-d H:i:s'),
            'AFF' => $_POST['num_aff'],
            'commercial' => $_POST['commercial'],
            'raison_sociale' => $_POST['nom_client'],
            'date_debut' =>$_POST['date_install'],
            'duree' => $full_sec,
            'duree_time' => $time,
            'observations' => $_POST['observations'],
            'statut' => 'active'
            ];
              
           if($this->technique_model->add_install($data_install)){
               
               $message['success'] = "Installation ajoutée avec succès";
   
           }else{
               
               $message['error'] = "Erreur lors de la création de l'installation";
               
           }
           $this->load_tpl_install($message);
        }else{
            
           $this->form_add_install();
        }
    }
    
    public function edit_install_page($id){
        
        $data = $this->load_dashboard_assets();
        $data['install'] = $this->technique_model->get_install_by_id($id);
        $time = $this->convert_second_to_hours_days($data['install'][0]->duree);
        
        
        $data['install'][0]->jours = $time['jours'];
        $data['install'][0]->heures = $time['heures'];
        
        $this->load->view('templates/header',$data); 
        $this->load->view('admin/technique/'.$this->_entreprise.'/dashboard_form_edit_install',$data);
        $this->load->view('templates/footer',$data);
        
    }
    
    
    public function archive_install($id){
      
        if($this->technique_model->archive_install($id)){
            
            $message = "Installation archivée avec succès";
            $this->dashboard($message);
            
        }else{
            $message['error'] = "erreur, impossible d'archive cette installation.";
            $this->load_tpl_install($message);
        }
         
    }
    
    
    public function edit_install( ){

        $this->load->helper(array('form', 'url'));
        $this->form_validation->set_rules('select_type', 'type de prestation', 'required');
        $this->form_validation->set_rules('nom_client', 'Raison sociale / Nom du client', 'required');
        $this->form_validation->set_rules('commercial', 'commercial', 'required');
        $this->form_validation->set_rules('date_install', 'date d\'installation', 'required');
        $this->form_validation->set_rules('jours', 'jours', 'required');
        $this->form_validation->set_rules('heures', 'heures', 'required');
        $this->form_validation->set_rules('observations', 'observations', 'trim');
        $id = (int)$_POST['install_id'];
        
        if ($this->form_validation->run( ) != FALSE)
        {
            $nb_jour = (int)$_POST['jours'];
            $day_to_hours = $nb_jour * 8;
            $nb_hours = (int)$_POST['heures'];
            $nb_hours_to_sec = $nb_hours * 3600;
            $total_sec = $day_to_hours * 3600;
            
     
            $tot_hours = $day_to_hours + $nb_hours;
            $full_sec = $total_sec + $nb_hours_to_sec;
            $time = $tot_hours.':00:00'; 
            
            
            
        $data = [
            
            'type'=> $_POST['select_type'],
            'AFF' => $_POST['num_aff'],
            'commercial' => $_POST['commercial'],
            'date_debut' => $_POST['date_install'],
            'duree' => $full_sec,
            'duree_time' => $time,
            'raison_sociale' => $_POST['nom_client'],
            'observations' => $_POST['observations']
        ];
        
         if($this->technique_model->update_install( $data, $id )){
               
               $message['success'] = "Installation mise à jour avec succès";
   
           }else{
               
               $message['error'] = "Erreur lors de la mise à jour de l'installation";
               
           }
           $this->load_tpl_install($message);
    }else{
        
        $this->form_edit_install( $id );
    }
    
    }
    
    // un jour de technicien = 8heures de boulot
    public function convert_second_to_hours_days($time){
        
        // 8h en seconde
        if($time >= 28800){
            $jours = floor($time/28800);
            $reste = $time%28800;
            $heures = $reste/3600;
        }else{
            $jours = 0;
            $heures = floor($time/3600);
        }
        
        $data['jours'] = $jours;
        $data['heures'] = $heures;
        
        return $data;
 
    }
    
}
