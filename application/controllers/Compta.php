<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Compta
 *
 * @author emman
 */
class Compta extends MY_Controller {
   
       public function __construct() {
        
        parent::__construct( );
        
        $this->load->model('compta_model');
       
        }
        
        public function index( ){
         
             $this->load->view('templates/header');
             $this->load->view('admin/comptabilite/index.php');
             $this->load->view('templates/footer');
             
        }
}
