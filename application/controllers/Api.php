<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of API
 *
 * @author emman
 */
class Api extends MY_Controller{
    //put your code here
    public function __construct( ){
        
        parent::__construct();
        
        // load db model :
        $this->load->model('Lead_model'); 
    }
    
    public function index( ){
       // echo "toto";
    }
    
        public function get_lead($requete){
        // Recherche dans la base de donnees avec la requete effectuee depuis l'application
            $data['lead'] = $this->Lead_model->get_lead_data( $requete );
        
            /* S'il n'y a pas de resultat dans la base de donnees, on retourne une erreur pour notifier l'utilisateur qu'il n'y a pas de resultat*/
            if($data == NULL){
            $message = [
                'message' => 'Erreur'
            ];
            }

        // Sinon, on definit un message JSON avec toutes les informations nécessaires
            
        
            // Envoi du message JSON
            $this->send_json($data);
        
        }
        
        
        public function get_leads( ){
            
            $data['lead'] = $this->Lead_model->get_leads_data( );
            $this->send_json($data);
            
        }
         
        
        public function send_json($data){
            
            //add the header here
            header('Content-Type: application/json');
            echo json_encode($data);
            
        }
        
        public function get_leads_by_status ($statut){
            $data['lead'] = $this->Lead_model->get_leads_by_status( $statut );
            $this->send_json($data);
        }
        
}
