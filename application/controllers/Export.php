<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends MY_Controller {
    
    
    public function __construct( ) {
        
        parent::__construct( );
        
        $this->is_supercommercial();
    }
    
    
    public function is_supercommercial( ){
        
       // var_dump($this->session);
        
        if($this->session->userdata['user']->role !== "supercommercial"){
            exit;
        }
    }
    
    public function index( ){
        
             $this->load->view('templates/header');
             $this->load->view('export/index');
             $this->load->view('templates/footer');
    }
}