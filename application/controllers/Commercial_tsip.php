<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commercial_tsip
 *
 * @author emman
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commercial_tsip extends MY_Controller{
    
        protected $id;
        protected $name;
        protected $prename;
        public $email;
        protected $societe;
        
        private $_securicom_role_id = COMMERCIAL_ROLE_ID;
        private $tsip_role_id = TSIP_ROLE_ID;
        
        private $db_prefix = "leads_";
        public $_credit_sms;
        
        public function __construct( ) {
        
        parent::__construct();
        $this->load->model('commercial_tsip_model');
        $this->load->library('email');
        
        $this->load->library('pagination');
        
        $this->load->library('smsenvoi');
        
        $sms_credit = new Smsenvoi( );
        $total = $sms_credit->checkCredits( );
        $total_credit = $total['sms']['PREMIUM'];
        
        $this->credit_sms = $total_credit;
        
        
        $this->id = $_SESSION['user']->id;
        $this->name = $_SESSION['user']->nom;
        $this->prename = $_SESSION['user']->prenom;
        $this->email = $_SESSION['user']->email; 
        $this->societe = $_SESSION['user']->societe;
        
        }
        
    
        public function index( ){
            
            $data = parent::data_ca();
            $config['base_url'] = base_url( ).'index.php/commercial_tsip/index';
            $config['total_rows'] = $this->commercial_tsip_model->get_rows( $this->id );
            
          //  var_dump($config);
            
            $total_leads = $config['total_rows'];
            $config['per_page'] = 5;
        
            $config['num_links'] = $config['total_rows'];
        
            $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $config['next_link'] = 'Suivant';
        
        
            $config['prev_link'] = 'Précédent';
            $config['uri_segment'] = 3;
            
            $this->pagination->initialize($config);
            $data['links'] = $this->pagination->create_links( );
            
            
             $CI = &get_instance();
        
             $CI->load->model('supercommercial_model');
             $commerciaux = $CI->supercommercial_model->get_commerciaux_new( );
             
          //   var_dump($commerciaux);
        
            //$infos = $this->supercommercial_model->get_user_roles(55);
        
            //var_dump( $infos );
        
             $current_date = date('Y-m-01');
             $data['commercial_bilan'] = $CI->supercommercial_model->get_commerciaux_and_ca_by_month_year( $current_date,$this->_securicom_role_id );
             
          //   var_dump( $data );
             
             $data['year'] = date('Y');
             $data['month'] = date('M');
            
             $data['js_to_load'] = "commercial_tsip/index/index.js";
             $data['css_to_load'] = "jquery.datetimepicker.min.css";
             $data['credit_sms'] = $this->credit_sms;
             //$data['full_leads'] = $this->commercial_tsip_model->get_leads($this->db_prefix.'tsip',$this->id);
             $data['full_leads'] = $this->commercial_tsip_model->get_current_leads(5, $start_index, $this->id);
             $this->load_single_template($data,'index');
             
        }
        
        public function load_single_template($data,$template){
        
             $this->load->view('templates/header',$data);
             $this->load->view('admin/commercial_tsip/'.$template, $data);
             $this->load->view('templates/footer');
        }
        
         public function edit_tsip_lead($id){
            $data['credit_sms'] = $this->credit_sms; 
            $data['js_to_load'] = "commercial/edit_lead/edit_lead.js";
            $data['css_to_load'] = "jquery.datetimepicker.min.css";
            $lead = $this->commercial_tsip_model->get_lead_by_id($id);
            $data['status_id'] = $this->commercial_tsip_model->get_status_by_name($lead->statut);
            $data['lead'] = $lead; 
            $data['status_lead'] = $this->commercial_tsip_model->get_status( );
            $this->load_single_template($data, 'edit_lead');
            
        }
        
        public function update_lead( ){
              
            
           $lead_update = $_POST;
           $status_lead = $_POST['statut']; 

           if($this->commercial_tsip_model->update_lead($status_lead, $lead_update)){
               
                $this->load_success_template( );
                    
               }else{
                $data['error'] = 'Lead pas mis à jour correctement';
                $this->load_error_template( $data );  
               }
          
        }
        
        public function load_success_template( ) {
             $data['credit_sms'] = $this->credit_sms;
             $data['message'] = "Les status du lead sont correctement mis à jour.";
             $this->load->view('templates/header',$data);
             $this->load->view('admin/commercial_tsip/success', $data);
             $this->load->view('templates/footer');
        }
        
        public function load_error_template( ) {
            $data['credit_sms'] = $this->credit_sms;
             $data['error'] = "Erreur lors de la sauvegarde";
             $this->load->view('templates/header',$data);
             $this->load->view('admin/commercial_tsip/error', $data);
             $this->load->view('templates/footer');
        }
}