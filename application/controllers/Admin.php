<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author emman
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller{
    
    public function __construct( ) {
        
        parent::__construct( );
        
        // load db model :
        $this->load->model('admin_model');

    }
    
    public function index( ) {
        
        $this->load->view('templates/header');
        $this->load->view('admin/index');
        $this->load->view('templates/footer'); 
    }
    
    public function display_tech_calendar( ){
   
      $tech_id =   $this->input->post('tech_select');
      $this->load->view('templates/header');
      $this->load->view('admin/single_calendar_tech');
      $this->load->view('templates/footer'); 
    }
}
