<?php

if ( ! defined('BASEPATH')) exit( 'No direct script access allowed' );

class Curl_handler extends MY_Controller {

	public function __construct( ) {

		parent::__construct( );

		$this->load->library('pagination');
		$this->load->model('Curl_handler_model');
		$this->load->library('email');
		$this->load->library('smsenvoi');

	}

	public function lead_tsip($data){

		$lead_id = $data['lead_id'];

		if(isset($data['msg'])){
			//limit msg field to fit sms
			$lead_msg = substr($data['msg'],0,80);
		}
		if(isset($data['nom'])){
			$lead_nom = $data['nom'];
		}
		if(isset($data['tel'])){
			$lead_tel = $data['tel'];
		}

		if(AUTO_ASSIGN === false) {

			$this->update_lead_tsip_commercial($lead_id, 0);
			//get lead data
			$lead_data = $this->Curl_handler_model->get_lead_tsip_by_id($lead_id);
			$this->notify_lead_tsip($lead_data);


		}else {

			//get random commercial
			$commercial = $this->get_random_tsip_commercial();
			//assign lead to commercial in db.
			$com_tel = $commercial->user_tel;
			$com_mail = $commercial->email;
			$com_id = $commercial->id;
			$com_prenom = $commercial->prenom;
			$com_nom = $commercial->nom;
			$full_name = $com_nom . " " . $com_prenom;

			//get lead data
			$lead_data = $this->Curl_handler_model->get_lead_tsip_by_id($lead_id);

			// assign the random commercial to lead in extranet
			$this->update_lead_tsip_commercial($lead_id, $com_id);

			// envoyer à admin tsip un email de notif
			$this->notify_lead_tsip($lead_data);

			$sms = new Smsenvoi();
			$total = $sms->checkCredits();
			$total_credit = $total['sms']['PREMIUM'];

			$data_sms = "Rappeler: " . $lead_nom . " au: " . $lead_tel . " MSG: " . $lead_msg;

			// limit the sms size
			$sms_max_size = 159;

			if (strlen($data_sms) >= $sms_max_size) {

				$data_sms = substr($data_sms, 0, $sms_max_size);
			}


			if ($total_credit != 0) {

				$this->send_sms($com_tel, $data_sms, 'PREMIUM', 'SECURICOM');

			}
		}
	}



	public function check_if_active($commercial_id){

	}


	public function index( ){


		// start TSIP
		if(isset($_POST['curl_key']) && $_POST['curl_key'] === "keyTSIP2019"){

			$data = $_POST;
			$this->lead_tsip($data);

		}

		//start securicom
		if(isset($_POST['curl_key']) && $_POST['curl_key'] === "randkey27bA3267"){

			$lead_id = $_POST['lead_id'];

			if(isset($_POST['msg'])){
				//limit msg field to fit sms
				$lead_msg = substr($_POST['msg'],0,80);
			}
			if(isset($_POST['nom'])){
				$lead_nom = $_POST['nom'];
			}
			if(isset($_POST['tel'])){
				$lead_tel = $_POST['tel'];
			}
			if(isset($_POST['type_lead'])){
				$lead_type = $_POST['type_lead'];
			}
			if(isset($_POST['page_url'])){
				$lead_url = $_POST['page_url'];

			}

			// $last_lead = $this->Curl_handler_model->get_before_last_row_lead( );
			if(AUTO_ASSIGN === false) {

				$this->Curl_handler_model->update_lead_before_sms( $lead_id, 0 );

				if(isset($lead_url)){
					$this->notify_admin($lead_url);
				}else{
					$this->notify_admin('non renseignée');
				}

			}else {

				$coms_list = $this->Curl_handler_model->get_commerciaux_id_array();
				$last_lead = $this->Curl_handler_model->get_before_last_row_lead();
				$last_id = (int)$last_lead->commercial_id;
				$current_id = (int)$this->get_next_commercial_id($last_id, $coms_list);
				$commercial = $this->Curl_handler_model->get_commercial_by_id($current_id);


				//assign lead to commercial in db.

				$com_tel = $commercial->user_tel;
				$com_mail = $commercial->email;
				$com_id = $commercial->id;
				$com_prenom = $commercial->prenom;
				$com_nom = $commercial->nom;
				$full_name = $com_nom . " " . $com_prenom;

				//get lead data
				$lead_data = $this->Curl_handler_model->get_lead_by_id($lead_id);

				// assign the random commercial to lead in extranet
				$this->Curl_handler_model->update_lead_before_sms($lead_id, $com_id);

				// send mail with full info to commercial
				$this->notify_lead($com_mail, $lead_data, $full_name);

				$sms = new Smsenvoi();
				$total = $sms->checkCredits();
				$total_credit = $total['sms']['PREMIUM'];

				$data_sms = "Rappeler: " . $lead_nom . " au: " . $lead_tel . " MSG: " . $lead_msg;

				// limit the sms size
				$sms_max_size = 159;

				if (strlen($data_sms) >= $sms_max_size) {
					$data_sms = substr($data_sms, 0, $sms_max_size);
				}

				if ($total_credit != 0) {
					$this->send_sms($com_tel, $data_sms, 'PREMIUM', 'SECURICOM');

				}
			}

		}
	}


	public function update_lead_tsip_commercial( $id, $commercial_id ){

		$update = $this->Curl_handler_model->update_lead_tsip_sms( $id, $commercial_id );

		return $update;

	}

	public function get_random_tsip_commercial( ){

		$coms = $this->Curl_handler_model->get_commerciaux_tsip_id( );
		$rand_key = array_rand($coms);
		$com_id = $coms[$rand_key]->id;

		$com_data = $this->Curl_handler_model->get_commercial_tsip_by_id($com_id);

		return $com_data;
	}

	public function get_next_commercial_id( $last_com_id , $com_list ){

		end($com_list);
		$last_key = key($com_list);
		reset($com_list);

		if($last_com_id == $com_list[$last_key]){

			$current_id = $com_list[0];

			return $current_id;

		}else{

			foreach ($com_list as $key => $value){

				if($last_com_id == $value){

					$current_id = $com_list[$key+1];

					return $current_id;

				}

			}

		}
	}

	public function get_random_commercial( ){


		// get last commercial id

		$coms = $this->Curl_handler_model->get_commerciaux_id( );

		$rand_key = array_rand($coms);
		$com_id = $coms[$rand_key]->id;

		$com_data = $this->Curl_handler_model->get_commercial_by_id($com_id);

		return $com_data;

	}

	public function send_sms($phone_num, $message, $type, $name){

		if(ACTIVE_SMS){

			$smsenvoi = new smsenvoi( );
			$smsenvoi->debug = true;

			if($smsenvoi->sendSMS($phone_num, $message, $type, $name)){
				return TRUE;
			}else{
				return FALSE;
			}

		}else{

			return FALSE;
		}
	}

	public function notify_lead_tsip($lead){

		$nom = $lead->nom_contact;
		$tel = $lead->tel;
		$msg = $lead->msg;


		$from_email = "dsi@groupsecuricom.fr";
		$this->email->from($from_email, 'DSI Securicom');
		$this->email->to('ngomes@tsip-securite.fr');
		$cc = ['smence@tsip-securite.fr','eautin@groupsecuricom.fr'];
		$this->email->cc($cc);
		$this->email->subject('Demande devis alarme particulier');
		$this->email->message(
			'Nouvelle demande en ligne sur tsip-telesurveillance.fr
                 nom : '.$nom.'
                 msg : '.$msg.'
                 tel : '.$tel.'');

		if($this->email->send( )){
			return TRUE;
		}else{
			return FALSE;
		}

	}


	public function notify_tsip_admin($url){

		$from_email = "dsi@groupsecuricom.fr";
		$this->email->from($from_email, 'DSI Securicom');
		// $this->email->to('cgourio@groupsecuricom.fr');
		$this->email->to('eautin@groupsecuricom.fr');
		$this->email->subject('Nouveau lead sur tsip');
		$this->email->message('Un nouveau lead web a été reçu. En attente de traitement sur extranet. URL de provenance: '.$url.'');

		if($this->email->send( )){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function notify_admin( $url ){

		$from_email = "dsi@groupsecuricom.fr";
		$this->email->from($from_email, 'DSI Securicom');
		// $this->email->to('cgourio@groupsecuricom.fr');
		$this->email->to('ricardo.stecca@sfr.fr');
		$cc = ['rstecca@groupsecuricom.fr','eautin@groupsecuricom.fr', 'soeuvray@groupsecuricom.fr'];
		$this->email->cc($cc);
		$this->email->subject('Nouveau lead web ! en attente de traitement.');
		$this->email->message('Un nouveau lead web a été reçu. En attente de traitement sur extranet. URL de provenance: '.$url.'');

		if($this->email->send( )){
			return TRUE;
		}else{
			return FALSE;
		}

	}

	public function notify_lead($commercial_email, $lead_data, $com_name) {

		//notify commercial
		$from_email = "dsi@groupsecuricom.fr";
		$this->email->from($from_email, 'DSI Securicom');
		$this->email->to($commercial_email);


		$this->email->subject('Un nouveau lead web vous est attribué');
		$this->email->message
		('Bonjour, '.$com_name.'
            Vous avez un nouveau lead web. Voici les informations de ce lead: Date de la demande : '.$lead_data->date_demande.'
            Rendez vous dans votre espace web pour gérer ce lead. Vous avez également reçu un sms contenant l\ensemble des informations.
            https://extranet.securicom-telesurveillance.fr');

		if($this->email->send()){

			//$this->notify_admin($lead_data->url);
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
