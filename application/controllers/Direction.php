<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Direction
 *
 * @author emman
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Direction extends MY_Controller{
    
    public function __construct( ){
        
         parent::__construct( );
        
        // load db model :
        $this->load->model('Lead_model');
        
    }
    
    public function index( ){
        
             $CI = & get_instance( );
             $CI->load->model('Curl_handler_model');
             $com_ids =  $CI->Curl_handler_model->get_commerciaux_id_array( );
             $CI->load->model('Supercommercial_model');
             $arr_data = [ ];
             
             foreach($com_ids as $id){
                 
                 array_push($arr_data, array(
                     
                     'commercial_name' => $com_name = $CI->Supercommercial_model->get_commercial_by_id( $id ), 
                     'a_traiter' => $this->Lead_model->get_lead_by_statut_and_com_id('a traiter', $id),
                     'rappele_ne_donne_pas_suite' => $this->Lead_model->get_lead_by_statut_and_com_id('rappelé ne donne pas suite', $id),
                     'rappele_rdv_pris' => $this->Lead_model->get_lead_by_statut_and_com_id('rappelé rdv pris', $id),
                     'devis_en_cours' => $this->Lead_model->get_lead_by_statut_and_com_id('devis en cours', $id),
                     'devis_signe' => $this->Lead_model->get_lead_by_statut_and_com_id('devis signé', $id),
                     'devis_ne_donne_pas_suite' => $this->Lead_model->get_lead_by_statut_and_com_id('devis ne donne pas suite', $id),
                     'demande_non_commerciale' => $this->Lead_model->get_lead_by_statut_and_com_id('demande non commerciale', $id),
                     'rdv_ne_donne_pas_suite' => $this->Lead_model->get_lead_by_statut_and_com_id('devis ne donne pas suite', $id)
                     )
                         );
             }
             
             $data['infos_leads'] = $arr_data;
             
             $CI->load->model('Commercial_model');
             $data['best_commercial'] = $CI->Commercial_model->get_commercial_by_lead_max_statut( 'devis signé' );
             $this->load->view('templates/header');
             $this->load->view('admin/direction/index', $data);
             $this->load->view('templates/footer');
    }
}
