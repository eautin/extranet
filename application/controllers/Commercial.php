<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of commercial
 *
 * @author emman
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commercial extends MY_Controller {

        protected $id;
        protected $name;
        protected $prename;
        public $email;
        protected $societe;
        
        private $db_prefix = "leads_";
        public $_credit_sms;
        public $is_mobile; 
        public $_secu_id = COMMERCIAL_ROLE_ID;
        public $_tsip_id = TSIP_ROLE_ID;
        
        public function __construct( ) {
        
        parent::__construct( );
        
        
        $this->load->model('commercial_model');
        $this->load->library('email');
        $this->load->library('pagination');
        $this->load->library('smsenvoi');
        $this->load->library('user_agent');
        
        $this->is_mobile = $this->agent->is_mobile( ); 

		try{
			$sms_credit = new Smsenvoi( );
			$total = $sms_credit->checkCredits( );
			$total_credit = $total['sms']['PREMIUM'];
			$this->credit_sms = $total_credit;
		}catch(Exception $e){
			var_dump( $e );
			$this->credit_sms = "Problème de connexion avec la librairie SmsEnvoi";
		}

        $this->id = $_SESSION['user']->id;
        $this->name = $_SESSION['user']->nom;
        $this->prename = $_SESSION['user']->prenom;
        $this->email = $_SESSION['user']->email; 
        $this->societe = $_SESSION['user']->societe;
        
        }
        
        public function get_num_pagination( ){
        
            $rows = $this->commercial_model->get_num_rows_leads( $this->id );
            return $rows;
            
        }
        
        

        
        public function leads_part( ){
            
             $this->load->view('templates/header');
             $this->load->view('admin/commercial/leads_part');
             $this->load->view('templates/footer');
             
          
        }
        
        
        public function index( ){
            
            // add pagination 
            
            $data = parent::data_ca( );
            
            $config['base_url'] = base_url( ).'index.php/commercial/index';
            $config['total_rows'] = $this->get_num_pagination( );
             
            $total_leads = $config['total_rows'];
            $config['per_page'] = 5;
        
            $config['num_links'] = $config['total_rows'];
        
            $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $config['next_link'] = 'Suivant';
        
        
            $config['prev_link'] = 'Précédent';
            $config['uri_segment'] = 3;
            
            $this->pagination->initialize($config);
            $data['links'] = $this->pagination->create_links( );
        
           
            $data['js_to_load'] = "commercial/index/index.js";
            $data['css_to_load'] = "jquery.datetimepicker.min.css";
            $data['credit_sms'] = $this->credit_sms;
             
            $data['full_leads'] = $this->commercial_model->get_current_leads(5, $start_index, $this->id);
           // $data['best_commercial'] = $this->commercial_model->get_commercial_by_lead_max_statut( 'devis signé' );
            $this->load_single_template($data,'index');
             
             
        }
    
        public function load_single_template($data,$template){
        
             $this->load->view('templates/header',$data);
             $this->load->view('admin/commercial/'.$template, $data);
             $this->load->view('templates/footer');
        }
        
        
        public function edit_lead($type, $id){
            
            $data['credit_sms'] = $this->credit_sms;
            $data['js_to_load'] = "commercial/edit_lead/edit_lead.js";
            $data['css_to_load'] = "jquery.datetimepicker.min.css";
            $lead = $this->commercial_model->get_lead($type, $id);
            $data['status_id'] = $this->commercial_model->get_status_by_name($lead->statut);
            $data['lead'] = $lead; 
            $data['type'] = $type;
            $data['status_lead'] = $this->commercial_model->get_status( );
            
            $this->load_single_template($data,'edit_lead');
        }
        
        public function update_lead( ){

           if($this->commercial_model->update_lead($this->input->post( ))){
               
                    $this->load_success_template( );
                    
               }else{
                $data['error'] = 'Lead pas mis à jour correctement';
                $this->load_error_template( $data );  
               }
          
        }
        
       
        public function load_success_template( ) {
             $data['message'] = "Les status du lead sont correctement mis à jour.";
             $this->load->view('templates/header');
             $this->load->view('admin/commercial/success', $data);
             $this->load->view('templates/footer');
        }
        
        public function load_error_template( $data = null ) {

			if(!isset($data['error'])){
				$data['error'] = "Une erreur est survenue lors de la sauvegarde.";
			}
             $this->load->view('templates/header');
             $this->load->view('admin/commercial/error', $data);
             $this->load->view('templates/footer');
        }
}
