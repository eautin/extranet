<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Archives extends MY_Controller {
    
    
    public function __construct( ) {
        
        parent::__construct( );
        
        $this->is_supercommercial();
        $this->load->library('pagination');
        $this->load->model('lead_model');
        
    }
    
    
    public function is_supercommercial( ){
                
        if($this->session->userdata['user']->role !== "supercommercial"){
            exit;
        }
    }
    
    public function index( ){
        
        //$data['archives'] = $this->lead_model->get_leads_by_status('archive',10);
                  
        $config['base_url'] = base_url( ).'index.php/archives/index';
        $config['total_rows'] =  $this->lead_model->get_num_rows_leads( 'archive' );
        
        $total_leads = $config['total_rows'];
        $config['per_page'] = 10;
        
        $config['num_links'] = $config['total_rows'];
 
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $config['next_link'] = 'Suivant';
        $config['prev_link'] = 'Precedent';
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links( );
        
        $data['archives'] = $this->lead_model->get_current_archives('archive', 10, $start_index);
        $this->set_commercial_to_archive($data['archives']); 
         
        
        //var_dump($data);
        
        $this->load->view('templates/header');
        $this->load->view('archives/index', $data);
        $this->load->view('templates/footer');
            
    }
    
    
     public function set_commercial_to_archive($archives){
        
         $CI = &get_instance( );
         $CI->load->model('supercommercial_model');
        foreach($archives as $archive){
           
                $archive->commercial_name = $CI->supercommercial_model->get_commercial_by_id($archive->commercial_id);
        }
        
        return $archives;
    }
}