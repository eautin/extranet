<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login_model
 *
 * @author emman
 */
class Login_model extends CI_Model {
    //put your code here
    
    public function __construct() {
        
        $this->load->database();
        
    }
    
    public function login_validator($data) {
        
       
        $hash = $this->get_hash($data['user_name'], $data['user_password']);
        
        if(isset($hash['hash']) && isset($hash['password_input'])){
            
            $user = $this->verify($hash['hash'], $hash['password_input'], $data);  
            return $user;
            
        }
            return FALSE;
    }
    
    public function get_hash($user_name, $user_pass){
        $condition = "user_login =" . "'" . $user_name."'";
        $this->db->select('user_password');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $get_hash = $query->result();
        
        if(!empty($query->result())){
            
            $data['hash'] = $get_hash[0]->user_password;
            $data['password_input'] = $user_pass;
            return $data;
        }else{
            return FALSE;
        }
    }
    
    
    public function verify($hash, $password_input, $data){
        
        if(password_verify($password_input, $hash)){

               // $condition = "user_login =" . "'" . $data['user_name'] . "' AND " . "user_password =" . "'" . $hash . "'";
                
                $condition = array(
                   'user_login' => $data['user_name'],
                   'user_password' => $hash,
                   'deleted' => '0'
                );
                $this->db->select('*');
                $this->db->from('users');
                $this->db->where($condition);
                //$this->db->limit(1);
              //  $this->db->join('users_roles','users_roles.userid = users.id','left');
               // $this->db->join('users_roles', 'users.id = users_roles.userid','left');
                $query = $this->db->get( );
              //  print_r($this->db->last_query( ));
               // var_dump($query);
                if ($query->num_rows() == 1) {
            
                    $user = $query->result_object( );
                    $resp = $user[0]; 
                    
                   $resp->roles = $this->get_roles($resp->id);

                } else {
                     $resp = FALSE;
                }
                
                 
                  return $resp;
                  
                  
        
         }else{
             
             return FALSE;
             
         }
    }
    
    public function get_roles($id){
        
        $condition = array(
            'userid' => $id 
        );
        $this->db->select('roleid');
        $this->db->from('users_roles');
        $this->db->where($condition);
        $query = $this->db->get( );
        return  $query->result_array();
    }
    
}
