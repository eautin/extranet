<?php


/**
 * Description of EtudeVideo
 *
 * @author emmanuel autin
 * Crée une entité étudevideo
 */

class EtudeVideo extends CI_Model {
  
    protected $nom;
    protected $adresse = array('rue'=>'','cp' =>'','ville' => '');
    protected $tel; 
    protected $date_visite;
    protected $plan_id;
    protected $activite;
    protected $env = [
        
        'urbain'=> false, 
        'rural' => false, 
        'indus' => false, 
        'commercial' => false, 
        'isolé' => false, 
        'voisin' => false, 
        'autre' => '', 
        'autorisation_pref' => false
        
    ]; 
        
    protected $acces = "";
    protected $abords = "";
    protected $p_human_nb = 0;
    protected $_human_travailleur_isole = 0; 
    protected $p_human_livraison_secteur; 
    protected $p_human_horaire_livraison_start;
    protected $p_human_horaire_livraison_end;
    protected $p_human_autre;
    
    protected $exigences = [
        
        'pas_exigence' => false,
        'declaration_n82' => false, 
        'ref_cahier_charges' => false, 
        'secteurs' => '',
        'autre' => '',
        'nb_jour_stockage' => '',
        'nb_heure_stockage' => '', 
       
    ];
    
    protected $propositions_complementaires = [
      
        'prepa_dossier' => false, 
        'genie_civil' => '',
        'support_spe' => '',
        'dispo_eclairage' => '',
        'autre' =>  '',
        'surface_site' => '',
        'surface_surv' =>  '',
        'niveau_exigence' => [
            'standard' => false,
            'renforce' => true
        ],
        
        'type_ligne' => [
            'RTC' => false,
            'IP' => false,
            'GSM' => false, 
            'Autre' => ''
        ],
        
        'installation_cyber_securite_D32' => false,
        'client_cyber_self' => false, 
        'installateur_cyber' => false,
        'installateur_niveau_securite' => [1,2,3,4],
        
        'secteurs' => [
            
            '0' => 'designation',
            '1' => 'designation'
            
            
        ],
        'cameras' => [
            
            [''],
            [''],
            
        ],
                
        
        
    ];


    public function __construct( ) {
        
       parent::__construct( );
       $this->load->database( );
       
    }
    
      
}
