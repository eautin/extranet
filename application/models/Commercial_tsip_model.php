<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commercial_tsip_model
 *
 * @author emman
 */
class Commercial_tsip_model extends CI_Model{
    
    protected $db_prefix = 'leads_';
    
    public function __construct() {
        
        $this->load->database( );     
    }
    
    public function get_leads($type, $id){
        
        $table = $type;    
        $condition = "commercial_id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $this->db->order_by("date_demande", "desc");
        $leads = $this->db->get( )->result();
        return $leads;
        
    }
    
    
    public function get_lead_by_id($id){
        
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from('leads_tsip');
        $this->db->where($condition);
        $leads = $this->db->get( )->row();
        return $leads;
    }
    
     public function get_status_by_name($name){
        
        $condition = "nom =" . "'" . $name . "'";
        $this->db->select('id');
        $this->db->from($this->db_prefix.'statut');
        $this->db->where($condition);
        $query = $this->db->get( );
        $status_id =  $query->row();
      
        return $status_id;
     }
     
      public function get_status(){
        
        $this->db->select('nom');
        $this->db->from($this->db_prefix.'statut');
        $query = $this->db->get( );
        $leads_status = $query->result_array();
        $status_container = [];
        
        foreach ($leads_status as $status){
            
            array_push($status_container, $status['nom']);
            
        }
        array_unshift($status_container,"");
        unset($status_container[0]);
        
        
        return $status_container;
     }
     
     public function update_lead($lead_status, $lead){
 
    
        $table = $this->db_prefix.'tsip';
        
        $statut = $this->get_status_by_id($lead['statut']);
        
        // a traiter
        if($lead_status == '1'){
            
            $data = array(
                'statut' => $statut->nom,
                'commentaire' => $lead['commentaire']
            );
        }
        // rappelé ne donne pas suite 
        if($lead_status == '2'){
            
            $data = array(
                'statut' => $statut->nom,
                'commentaire' => $lead['commentaire']
            );
        }
        //rappelé rdv pris
        if($lead_status == '3'){
            
            $data = array (
              'statut' => $statut->nom,
              'date_rdv_confirme' => $lead['date_picker'],
              'commentaire' => $lead['commentaire']
            );
        }
        // devis encours 
        if($lead_status == '4'){
            
            $data = array(
                
               'statut' => $statut->nom,
               'devis_confirme' => $lead['code_devis'], 
               'commentaire' => $lead['commentaire']
                
            );
            
        }
        // devis validé
        if($lead_status == '5'){
            
             $data = array (
              'statut' => $statut->nom,
              'date_devis_confirme' => $lead['date_devis'],
              'commentaire' => $lead['commentaire']
            );
        }
        
        // devis ne donne pas suite
        if($lead_status == '6'){
            
            $data = array (
                
                'statut' => $statut->nom,
                'commentaire' => $lead['commentaire']
            );
        }
        
        // rdv ne donne pas suite 
        if($lead_status == '7'){
            
            $data = array (
                
                'statut' => $statut->nom,
                'commentaire' => $lead['commentaire']
            );
        }
        
            $this->db->set($data);
            
            $this->db->where('id', $lead['id']);
            if($this->db->update($table, $data)){
                return TRUE;
            }else{
                return FALSE;
            }
            
    }
    
    public function get_status_by_id($id){
        
                $condition = "id =" . "'" . $id ."'";
                $this->db->select('nom');
                $this->db->from($this->db_prefix.'statut');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get( );
                $lead_status = $query->row( );
                return $lead_status;
    }
    
    public function get_rows( $com_id){
        
        $condition = array(
            'commercial_id' => $com_id
        );
        
        $this->db->select('*');
        $this->db->from($this->db_prefix.'tsip');
        $this->db->where($condition);
        $num_result = $this->db->count_all_results( );
       
        return $num_result;
    }
    
    
    public function get_current_leads($limit_per_page, $start_index, $com_id){
        
       
        $condition = "commercial_id =" . "'" . $com_id ."'";
        $this->db->select('*');
        $this->db->from($this->db_prefix.'tsip');
        $this->db->where($condition);
        $this->db->limit($limit_per_page, $start_index);
        $this->db->order_by("date_demande", "desc");
       
        $query = $this->db->get( );

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    
    
}
