<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of superadmin_model
 *
 * @author emman
 */
class Superadmin_model extends CI_Model {
    
     public function __construct() {
        
        $this->load->database();
        
    }
     
    public function insert_user($user) {
             
       if($this->db->insert('users', $user)) {
           return TRUE;
       }
    }
}
