<?php


class Sms_lead_model extends CI_Model {

	public function __construct( ) {

		$this->load->database( );

	}


	public function get_sms_lead_data( $lead_id ){

		$cond = ['lead_id' => $lead_id];
		$this->db->select('*');
		$this->db->from('sms_lead');
		$this->db->where($cond);
		$query = $this->db->get( );
		$lead = $query->row_array( );
		return $lead;

	}





}
