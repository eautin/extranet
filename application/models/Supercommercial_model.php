<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of supercommecial_model
 *
 * @author emman
 *
 * 
 */

class Supercommercial_model extends CI_Model {

        protected $db_prefix = 'leads_';
        
        public function __construct( ) {
            $this->load->database( );
        }
            
       public function get_status( ) {
        
        $this->db->select('nom');
        $this->db->from($this->db_prefix.'statut');
        $query = $this->db->get( );
        $leads_status = $query->result_array();
        $status_container = [];
        
        foreach ($leads_status as $status){
            array_push($status_container, $status['nom']);
        }
            array_unshift($status_container,"");
            unset($status_container[0]);
            return $status_container;
     }
     
     
     public function save_month_ca($id_com, $total_loc, $total_vente, $total_devis, $month_year){
         

          $table = 'com_ca'; 
          $arr_cond = [
                  'id_com' => $id_com,
                  'date' => $month_year
              ];
          
                $data = array(
               
                'id_com' => $id_com,
                'date' => $month_year,
                'total_loc' => $total_loc,
                'total_vente' => $total_vente,
                'total_devis' => $total_devis
              
                );
         
         // check if exists : 
         
          $this->db->select('id');
          $this->db->from($table);
          $this->db->where($arr_cond);
          $query = $this->db->get();
         
         if($query->num_rows() > 0 ){
             
             $this->db->where($arr_cond);
             $this->db->update($table, $data);
             
         }else{
             
             
             $this->db->insert($table, $data);
             
             
         }
           
           
            
            
        }
        
     
     
     public function get_status_by_name($name){
         
        $condition = "nom =" . "'" . $name . "'";
        $this->db->select('id');
        $this->db->from($this->db_prefix.'statut');
        $this->db->where($condition);
        $query = $this->db->get( );
        $status_id =  $query->row();
      
        return $status_id;
     }
     
     
     
     public function get_commerciaux_new( ){
         
         // ne retourne que les commericaux au role_id commercial
         $this->db->select('*');
         $this->db->from('users');
         $this->db->join('users_roles', 'users.id = users_roles.userid');
         $this->db->where('users_roles.roleid = '.COMMERCIAL_ROLE_ID.'');
         $this->db->where('users.deleted = 0');
         $q = $this->db->get();         
         $t =  $q->result();
         
         
        //var_dump($t);
         return $t;
       
   
     }
     
     
     public function get_user_roles($id){
         
        $sql = 'SELECT users.id, users.nom, GROUP_CONCAT(role.nom) as role, GROUP_CONCAT(roleid) as roleid FROM users LEFT JOIN users_roles ON (users_roles.userid = users.id) LEFT JOIN role ON (users_roles.roleid = role.id) WHERE users.id = '.$id.''; 
        $query = $this->db->query( $sql );
        $resp = $query->result( );
        $roles = explode(',', $resp[0]->roleid );
        
        return $roles;
     
     }
    
    public function get_commerciaux( ){

        $condition = array(
            "role" => 'commercial',
            "deleted" => '0',
            'active' => '1'
        );
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        //$this->db->limit(1);
        $query = $this->db->get( );
        $commerciaux = $query->result();
        
       // var_dump($commerciaux);
        return $commerciaux; 
    }
    
    
    
    public function get_active_coms( ){
         
          $condition = array('deleted' => '0');
          $this->db->select('*');
          $this->db->from('users');
          $this->db->where($condition);
          $this->db->join('users_roles', 'users.id = users_roles.userid');
          $this->db->where('users_roles.roleid = '.COMMERCIAL_ROLE_ID.'');
         // $this->db->where('users.active = 1');
           $query = $this->db->get( );
        $commerciaux = $query->result();
        
       // var_dump($commerciaux);
        
        return $commerciaux;
    }
    /*old
    public function get_active_coms( ){
        
        $condition = array('role'=> 'commercial','deleted' => '0', 'active' => '1');
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        //$this->db->limit(1);
        $query = $this->db->get( );
        $commerciaux = $query->result();
        
        return $commerciaux;
    } */
    
    
      public function get_full_month_ca( ){

         $date = date('Y-m-01');
         $query =  $this->db->get_where('com_ca', array('date' => $date) );
          
         $cas = $query->result( );
         
         $full_ca = 0;
         
         foreach($cas as $ca){
             
             $full_ca += $ca->total_loc;
             $full_ca += $ca->total_vente;
             
         }
         
         return $full_ca;
          
      }
      
      public function get_commerciaux_and_ca_by_month_year( $date, $role_id ){
        
        $this->db->select('users.id, users.nom, users.prenom, com_ca.total_loc, com_ca.total_vente, com_ca.total_devis');
        $this->db->from('users');
        $this->db->join('com_ca', ' ( users.id = com_ca.id_com  AND  com_ca.date = "'.$date.'"','left');
        $this->db->join('users_roles', 'users.id = users_roles.userid');
        //$this->db->where('users_roles.roleid = '.$role_id.' AND users.deleted = 0 AND users.nom != "MARQUANT"');
		  $this->db->where('users_roles.roleid = '.$role_id.' AND users.deleted = 0');
        $query = $this->db->get( );
        
        $commerciaux = $query->result( );
        
        foreach($commerciaux as $commercial){
            
            $commercial->total_month = $commercial->total_loc + $commercial->total_vente;
        }
       
        return $commerciaux; 
        
    }
    
    
    public function get_commerciaux_and_ca_tsip( $date ){
        
        
        
        
    }
    
    public function get_leads( $type ){
        
        $this->db->order_by("date_demande", "desc");
        $this->db->limit(5);
        $leads = $this->db->get($type)->result( ); 
        return $leads;
    }
    
    
    public function get_num_rows_leads( ){

		$condition = "statut != archive";

		$this->db->select('*');
		$this->db->from($this->db_prefix.'full');
		$this->db->where_not_in('statut', 'archive');
		$rows = $this->db->count_all_results( );
		//var_dump($rows);
        //$num_rows = $this->db->count_all_results($this->db_prefix.'full');
        // return $num_rows;

		return $rows;
    }
   
    public function get_commercial_email($id){
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('email');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $email = $query->row( );
        return $email;
    }
    
    public function get_lead_by_type($type, $id){
        
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from($this->db_prefix.$type);
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $lead = $query->row( );
        $lead->commercial_name = $this->get_commercial_by_id($lead->commercial_id);
        if($lead->statut == ""){
            $lead->statut = "a traiter";
        }
        $lead->type =  $type;
        return $lead;
    }
    
    
    public function get_commercial_by_id($id) {
        
        if($id != 0){
                $val = "commercial";
                $condition = "id =" . "'" . $id ."'";
                $this->db->select('nom, prenom');
                $this->db->from('users');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get( );
            $commercial_name = $query->row();
            if($commercial_name != NULL){
               return $commercial_name->nom." ".$commercial_name->prenom; 
            }
            
        }else{
            $commercial_name = "Non renseigné";
        }
        return $commercial_name;
        
    }
    
    
    public function get_status_by_id($id){
        
                $condition = "id =" . "'" . $id ."'";
                $this->db->select('nom');
                $this->db->from($this->db_prefix.'statut');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get( );
                $lead_status = $query->row( );
                return $lead_status;
    }
    
    public function update_lead($lead){

        // a corriger parfois objet parfois array ! 
        if(is_array($lead)){
          $table = $this->db_prefix.$lead['type_lead']; 
          $statut = $this->get_status_by_id($lead['statut']);

		 // dd($lead);

          $data = array(
                'statut' => $statut->nom,
                'commercial_id' => $lead['commercial'],
                'date_rdv_confirme' => $lead['date_picker'],
                'date_devis_confirme' => isset($lead['date_devis']) ? $lead['date_devis'] : NULL,
                'commentaire' => $lead['commentaire'],
                'devis_confirme' => isset($lead['code_devis']) ? $lead['code_devis'] : NULL
                );
        
            $this->db->where('id', $lead['id_lead']);


            
        }else{
            // dans le cas notif par email le statut 
            $table = $this->db_prefix.$lead->type; 
            //$statut = $this->get_status_by_id($lead->statut);  
            
             $data = array(
                'statut' => $lead->statut,
                'commercial_id' => $lead->commercial_id,
                'date_rdv_confirme' => $lead->date_rdv_confirme,
                'date_devis_confirme' => $lead->date_devis_confirme,
                'commentaire' => $lead->commentaire,
                'devis_confirme' => $lead->devis_confirme
                );
        
            $this->db->where('id', $lead->id);
        }
        
            if($this->db->update($table, $data)){

		// mettre à jour les données dans table sms_lead :

				if($this->updateTable_sms_lead($lead)){
					return TRUE;
				}
            }else{
                return FALSE;
            }
    }


	private function updateTable_sms_lead( $lead ){

		$table = 'sms_lead';

		$data = [
			'name' => $lead['prospect_name'],
			'tel' => $lead['prospect_tel'],
			'cp' => $lead['prospect_cp'],
			'street'=> $lead['prospect_street'],
			'city' => $lead['prospect_city'],
			'email' => $lead['prospect_email']
		];

		$ql = $this->db->select('*')->from('sms_lead')->where('lead_id',$lead['id_lead'])->get();

		if($ql->num_rows() > 0){

			$this->db->where('lead_id', $lead['id_lead']);
			if($this->db->update($table, $data)){

				return TRUE;
			}
		} else {
			$data['lead_id'] = $lead['id_lead'];
			if($this->db->insert($table, $data)){
				return TRUE;
			}

		}
		return FALSE;
	}
    
    
    public function get_archive_leads( $limit_per_page, $start_index ){
        
        $this->db->limit($limit_per_page,$start_index);
        $this->db->order_by("date_demande", "desc");
        $query = $this->db->get($this->db_prefix.'full');
        $this->db->where('statut ==', 'archive');
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
        
    }
    
    public function get_current_leads($limit_per_page, $start_index){
        
        $this->db->limit($limit_per_page,$start_index);
        $this->db->order_by("date_demande", "desc");
        
        //$this->db->where('statut !=', 'archive');
        $this->db->where_not_in('statut', 'archive');
        $query = $this->db->get($this->db_prefix.'full');
       //print_r($this->db->last_query());
        
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_commercial_phone($commercial_id){
        
        $condition = "id =" . "'" . $commercial_id . "'";
        $this->db->select('user_tel');
        $this->db->from('users');
        $this->db->where($condition);
        $query = $this->db->get( );
        $commercial_phone =  $query->row();
      
        return $commercial_phone;
    }

	protected function get_commercial_name( $email ){


		$condition = "email =" . "'" . $email . "'";
		$this->db->select('nom, prenom');
		$this->db->from('users');
		$this->db->where($condition);
		$query = $this->db->get( );
		$com_name =  $query->row();

		return $com_name;


	}
    
    
    public function get_lead_last_commercial_id($lead_id){
        
        $condition = "id=" . "'". $lead_id . "'";
        $this->db->select('commercial_id');
        $this->db->from($this->db_prefix.'full');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $commercial_id =  $query->row();
        $id = $commercial_id->commercial_id;
        return $id;
    }
    
    public function get_commercial_by_lead_max_statut( $statut ){
        
       $CI = & get_instance( );
       $CI->load->model('Commercial_model');
       $commercial = $CI->Commercial_model->get_commercial_by_lead_max_statut( $statut );
       return $commercial;

    }
    
    public function update_active( $status ){
        
        
        $result_query = array();
        
        foreach($status as $id => $active_status){
            
            
            $table = 'users';  
            $data = array(
                'active' => $active_status
             
                );
            $this->db->where('id', $id);
            $result =  $this->db->update($table, $data);
            
          array_push($result_query, $result);
            
        }
        
        foreach($result_query as $key => $value){
            
            if($value !== true)
                return false;
        }
        
        return true;
        
    }
    
    
    public function insert_commercial($user) {
             
       if($this->db->insert('users', $user)) {
           
           // on ajoute le rôle commercial au user 
           $last_id = $this->db->insert_id();
           
           $data = array(
               'userid' => $last_id,
               'roleid' => COMMERCIAL_ROLE_ID
           );
           
           if($this->db->insert('users_roles',$data)){
               
               return TRUE;
           }else{
               throw new Exception('Insertion users_roles fails');
             //  return FALSE;
           }
           
           return TRUE;
       }else{
           throw new Exception('Insertion commercial fails');
           // return FALSE;
       }
    }
    
    
    public function check_user( $user ){
        
        $condition = array(
            'user_login' => $user['user_login'],
            'email' => $user['email']
        );
        
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $query = $this->db->get(); 
       
        if($query->num_rows() > 0){
          return true;  
        }else{
            return false;
        }
    }
    
    
     
}
