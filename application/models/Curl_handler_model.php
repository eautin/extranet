<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curl_handler_model extends CI_Model {
        
    public function __construct( ) {
        
       $this->load->database( );
       
    }
    
    /**
    * Démarre une session de bidulage.
    *
    * @param /
    *
    * @throws /
    *
    * @return array id commerciaux active = 1 et role commercial dans la table users_role
    */
    
     public function get_commerciaux_id_array( ){
         
         $this->db->select('id');
         $this->db->from('users');
         $this->db->join('users_roles', 'users.id = users_roles.userid');
         $this->db->where('users_roles.roleid = '.COMMERCIAL_ROLE_ID.'');
         $this->db->where('users.active = 1');
         $q = $this->db->get( );         
         $commerciaux =  $q->result_array();
         $coms = [];
         foreach($commerciaux as $key => $value){
        
                foreach($value as $key => $value){
                    
                    array_push($coms, (int)$value);
                }
        }
        
       // var_dump( $coms );
        
        return $coms;
       
   
     }
     
    /* OLD function
    public function get_commerciaux_id_array( ){
        
        $val = "commercial";
        $condition = "role =" . "'" . $val . "'";
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->where_not_in('nom','GOURIO');
        $this->db->where_not_in('active','0');
        $query = $this->db->get( );
        $commerciaux = $query->result_array( );
        
        $coms = [];
        foreach($commerciaux as $key => $value){
        
                foreach($value as $key => $value){
                    
                    array_push($coms, (int)$value);
                }
        }
        
       // var_dump( $coms );
        
        return $coms;
    }
    */
    
    public function get_commerciaux_id_array_active_inactive( ){
        
        $val = "commercial";
        $condition = "role =" . "'" . $val . "'";
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where($condition);
        $query = $this->db->get( );
        $commerciaux = $query->result_array( );
        
        $coms = [];
        foreach($commerciaux as $key => $value){
        
                foreach($value as $key => $value){
                    
                    array_push($coms, (int)$value);
                }
        }
           
        return $coms;
    }
    
    
    public function get_commerciaux_id_array_no_deleted( ){
        
        $cond = array(
            'role'=> 'commercial',
            'deleted' => '0'
        );
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where($cond);
        $query = $this->db->get( );
        $commerciaux = $query->result_array( );
        
        $coms = [];
        foreach($commerciaux as $key => $value){
        
                foreach($value as $key => $value){
                    
                    array_push($coms, (int)$value);
                }
        }
           
        return $coms;
    }
    
    public function get_commerciaux_id( ){
        
        $val = "commercial";
        $condition = "role =" . "'" . $val . "'";
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->where_not_in('nom','GOURIO');
        $query = $this->db->get( );
        $commerciaux = $query->result( );
        
        return $commerciaux; 
        
    }
    
    public function get_before_last_row_lead( ){
        
        
        // cette fonction récupère l'avant dernier lead
        // check si le commercial assigné est actif
        // tant que le commercial assigné n'est pas actif, on remonte au lead précédent
        //si commercial actif, alors on retourne la row concernée
      
        $active = false; 
        $offset = 1; 
        
        do{
            
            //var_dump($active);
            
            //récupération de l'avant derniere entrée lead
            $result = "";
            
                $last_row =  $this->db->order_by('id', 'desc')
                ->limit(1,$offset)
                ->get('leads_full')
                ->row();
            
            // on regarde si le commercial est actif
            $last_row_id = $last_row->id;
            $id_commercial = (int)$last_row->commercial_id;
            $active = $this->is_active($id_commercial);
            var_dump($active);
            
            if($active){
                //si actif on retourne la row 
                return $last_row;
                
            }else{
                
                 $offset = $offset +1;
                
                // sinon, on parcours le lead précédent et on vérifie si actif
              /*  $condition = "id <"."'".$last_row_id."'";
                $this->db->select('*');
                $this->db->from('leads_full');
                $this->db->where($condition);
                $this->db->order_by('id', 'desc')->limit(1);
                $query = $this->db->get( );
                $result = $query->row( ); */
                
                

            } 
        }while($active === false);
        
    }
    
    public function is_active($com_id){
                
                $condition = array('id' => $com_id);
                $this->db->select('active');
                $this->db->from('users');
                $this->db->where($condition);
                $query = $this->db->get( );
                
                $active =  $query->row_array( );
                
                if($active['active'] === "1"){
                    return true;
                }else{
                    return false;
                }
    }
    
    public function get_last_row_lead( ) {
        
       $last_row =  $this->db->order_by('id', 'desc')
                ->limit(1)
                ->get('leads_full')
                ->row( );
        
        return $last_row;
        
    }
    
    public function get_commerciaux_tsip_id( ){
        
        $val = "commercial_tsip";
        $condition = "role =" . "'" . $val . "'";
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where($condition);
        $query = $this->db->get( );
        $commerciaux = $query->result();
        
        return $commerciaux; 
        
    }
    
    
    public function update_lead_before_sms($lead_id, $commercial_id){
        
            $data = array(
                    'commercial_id' => $commercial_id
            );
        
            $table = 'leads_full';
            
            $this->db->where('id', $lead_id);
             if($this->db->update($table, $data)){
                return TRUE;
            }else{
                return FALSE;
            }
            
    }
    
    public function update_lead_tsip_sms($lead_id, $commercial_id){
        
            $data = array(
                    'commercial_id' => $commercial_id
            );
        
            $table = 'leads_tsip';
            
            $this->db->where('id', $lead_id);
             if($this->db->update($table, $data)){
                return TRUE;
            }else{
                return FALSE;
            }
            
    }
    
    public function get_commerciaux( ){

        $val = "commercial";
        $condition = "role =" . "'" . $val . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        //$this->db->limit(1);
        $query = $this->db->get( );
        $commerciaux = $query->result();
        return $commerciaux; 
    }
    
    public function get_commercial_by_id($id) {
        
        if($id != 0){
            
                $val = "commercial";
                $condition = "id =" . "'" . $id ."'";
                $this->db->select('*');
                $this->db->from('users');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get( );
                $commercial = $query->row();
        
            
        }else{
            $commercial = "non renseigne";
        }
        
        return $commercial; 
        
    }
    
    public function get_commercial_tsip_by_id($id) {
        
        if($id != 0){
            
                $val = "commercial_tsip";
                $condition = "id =" . "'" . $id ."'";
                $this->db->select('*');
                $this->db->from('users');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get( );
                $commercial = $query->row( );
        
            
        }else{
            $commercial = "non renseigne";
        }
        
        return $commercial; 
        
    }
    
    public function get_commercial_email($id){
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('email');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $email = $query->row( );
        return $email;
    }
    
    
    public function get_lead_by_id($id){
        
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from('leads_full');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $lead = $query->row( );
        $lead->commercial_name = $this->get_commercial_by_id($lead->commercial_id);
        if($lead->statut == ""){
            $lead->statut = "a traiter";
        }
        
        return $lead;
    }
    
    public function get_lead_tsip_by_id($id){
        
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from('leads_tsip');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $lead = $query->row( );
        $lead->commercial_name = $this->get_commercial_tsip_by_id($lead->commercial_id);
        if($lead->statut == ""){
            $lead->statut = "a traiter";
        }
        
        return $lead;
    }
    
}
