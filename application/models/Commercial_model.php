<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of commercial_model
 *
 * @author emman
 */
class Commercial_model extends CI_Model {
    
    protected $db_prefix = 'leads_';
    
    public function __construct() {
        parent::__construct( );
        $this->load->database( );     
    }
    
    public function get_leads($type, $id){
        
        $table = $type;    
        $condition = "commercial_id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $this->db->order_by("date_demande", "desc");
        $leads = $this->db->get( )->result();
        return $leads;
        
    }
    
    public function get_status_by_name($name){
        
        $condition = "nom =" . "'" . $name . "'";
        $this->db->select('id');
        $this->db->from($this->db_prefix.'statut');
        $this->db->where($condition);
        $query = $this->db->get( );
        $status_id =  $query->row();
      
        return $status_id;
     }
     
     
     public function get_status(){
        
        $this->db->select('nom');
        $this->db->from($this->db_prefix.'statut');
        $query = $this->db->get( );
        $leads_status = $query->result_array();
        $status_container = [];
        
        foreach ($leads_status as $status){
            
            array_push($status_container, $status['nom']);
            
        }
        array_unshift($status_container,"");
        unset($status_container[0]);
        
        
        return $status_container;
     }
    
     public function get_lead($type, $id){
        
        $condition = "id =" . "'" . $id ."'";
        $this->db->select('*');
        $this->db->from($this->db_prefix.$type);
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $lead = $query->row( );
        if($lead->statut == ""){
            $lead->statut = "a traiter";
        }
        $lead->type =  $type;
        return $lead;
    }
    
    
     public function update_lead( $lead ){

		if(isset($lead['type']) && isset($lead['statut'])) {
			$table = $this->db_prefix . $lead['type'];
			$statut = $this->get_status_by_id($lead['statut']);

			$data = array(

				'statut' => $statut->nom,
				'devis_confirme' => $lead['code_devis'],
				'date_rdv_confirme' => $lead['date_picker'],
				'date_devis_confirme' => $lead['date_devis'],
				'commentaire' => $lead['commentaire']
			);

			try {
				$this->db->set($data);
				$this->db->where('id', $lead['id']);
				if ($this->db->update($table, $data)) {
					return TRUE;
				}
			} catch (Exception $e) {
				log_message('error', $e->getMessage());
				return FALSE;
			}
		}
		return FALSE;
    }
    
    
     public function get_status_by_id($id){
        
                $condition = "id =" . "'" . $id ."'";
                $this->db->select('nom');
                $this->db->from($this->db_prefix.'statut');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get( );
                $lead_status = $query->row( );
                return $lead_status;
    }
    
    public function get_num_rows_leads( $com_id ){
       
        $condition = "commercial_id =" . "'" . $com_id ."'";
        $this->db->select('*');
        $this->db->from($this->db_prefix.'full');
        $this->db->where($condition);
        $num_result = $this->db->count_all_results( );
       
        return $num_result;
    }
    
    public function get_current_leads($limit_per_page, $start_index, $com_id){
        
       
        $condition = "commercial_id =" . "'" . $com_id ."'";
        $this->db->select('*');
        $this->db->from($this->db_prefix.'full');
        $this->db->where($condition);
        $this->db->limit($limit_per_page, $start_index);
        $this->db->order_by("date_demande", "desc");
       
        $query = $this->db->get( );
        
        //$toto = $query->row();

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    
    
    
    public function get_commercial_by_lead_max_statut( $statut ){
        
       $CI = & get_instance( );
       $CI->load->model('Curl_handler_model');
       $ids = $CI->Curl_handler_model->get_commerciaux_id_array( );
       $nb_statut_by_com = [];
       
   
      foreach($ids as $id){
     
          $multi_cond = ['commercial_id' => $id, 'statut' => $statut];
          $this->db->select('*');
          $this->db->from($this->db_prefix.'full');
          $this->db->where($multi_cond);
          $count = $this->db->count_all_results( );
          
          array_push($nb_statut_by_com, [$id => $count]);
          
      }
      
        $arr_value = [];
    
      foreach ($nb_statut_by_com as $key => $value){
          foreach ($value as $id => $nb_lead){
              array_push($arr_value, $nb_lead);
              
          }
      }
      
    
         
      $max_lead = max($arr_value);
      $id_com = 0;
      $is_twice = false;
      $id_coms =  [];
      
      foreach ($nb_statut_by_com as $key => $value){
          foreach ($value as $id => $nb_lead){
              
              if($nb_lead === $max_lead){
                  
                  if(!$is_twice && $max_lead != 0){
                      
                        $is_twice = true;
                        $id_com = $id;
                        array_push($id_coms,$id);
                        
                  }elseif($is_twice){
                     
                      array_push($id_coms,$id);
                  }
                
              }
          }
      }
       
      $check = count($id_coms);
      
      if($check === 1){
          
          $commercial = $this->get_com_name_by_id( $id_com );
          $commercial->nb_lead = $max_lead;
          $commercial->type_lead = $statut;
          return $commercial;
          
      }elseif($check > 1){
          
          $commerciaux = [];
          foreach($id_coms as $key => $value){
              
              $commercial = $this->get_com_name_by_id( $value );
              $commercial->nb_lead = $max_lead;
              $commercial->type_lead = $statut;
              
              array_push($commerciaux, $commercial);
          }
          
          return $commerciaux;
      }
      
    }
    
    
     public function get_com_name_by_id( $id ){
         
         $cond = ['id' => $id];
         $this->db->select('nom, prenom');
         $this->db->from('users');
         $this->db->where($cond);
         $query = $this->db->get( );
         $name = $query->row( );
         
         return $name;
     }
     
}
