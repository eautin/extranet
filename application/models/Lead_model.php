<?php


class Lead_model extends CI_Model {
    
    public function __construct( ) {
       parent::__construct( );
       $this->load->database( );
       
    }
    
    public function get_lead_by_statut_and_com_id($statut, $com_id){
        
          $multi_cond = ['commercial_id' => $com_id, 'statut' => $statut];
          $this->db->select('*');
          $this->db->from('leads_full');
          $this->db->where($multi_cond);
          $count = $this->db->count_all_results( );
          return $count;
    }
    
    public function get_lead_data($lead_id){
        
          $cond = ['id' => $lead_id];
          $this->db->select('*');
          $this->db->from('leads_full');
          $this->db->where($cond);
          $query = $this->db->get( );
          $lead = $query->row_array( );
          return $lead;
    }
    
    public function get_leads_data( ){
        
          $this->db->select('*');
          $this->db->from('leads_full');
          $query = $this->db->get( );
          $lead = $query->result_array( );
          return $lead;
    }
    
    public function get_leads_by_status($statut){
        
          $cond = ['statut' => $statut];
          $this->db->select('*');
          $this->db->from('leads_full');
          $this->db->where($cond);
          $query = $this->db->get( );
          $leads = $query->result( );
          return $leads;
        
    }
    
    
   
    
        public function get_num_rows_leads( $statut ){
        
         $cond = array(
             "statut" => $statut
         );
         $this->db->select('id');
         $this->db->from('leads_full');
         $this->db->where($cond);
         $num_rows = $this->db->count_all_results( );   
         return $num_rows;
        }
        
        
        public function get_current_archives($statut, $limit_per_page, $start_index){
        
        $this->db->limit($limit_per_page,$start_index);
        $this->db->order_by("date_demande", "desc");
        $this->db->where_in('statut', $statut);
        $query = $this->db->get('leads_full');
    
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    
      
}
