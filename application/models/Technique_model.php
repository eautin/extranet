<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of technique_model
 *
 * @author emman
 */
class Technique_model extends CI_Model {
    
    public function __construct() {
       
         $this->load->database( );   
        
    }
    
   public function checkdate($date) {
       
        $condition = "date_intervention =" . "'" . $date."'";
        $this->db->select('*');
        $this->db->from('intervention');
        $this->db->where($condition);
        $this->db->limit(100);
        $query = $this->db->get( );  
        $inter = $query->result( );
 
        if(!empty($inter)){
            return TRUE;
        }else{
            return FALSE;
        }
  
    }
    
    public function getTechData( ){
        
            $this->db->select('*');
            $this->db->from('technicien');
            $tech_data = $this->db->get( )->result_array( ); 
            return $tech_data;  
    }
    
    public function insertInterventions($interventions,$dayInter) {
        
         
         foreach($interventions as $value){
            
             $tech_id = $value['techid'];
             $type_inter = $value['name'];
             $nb_inter = $value['value'];

             $data = array (
                 'date_intervention' => $dayInter,
                 'type_intervention' => $type_inter,
                 'nb_intervention' => $nb_inter,
                 'id_technicien' => $tech_id
                 
             );
             
             if($this->db->insert('intervention',$data)){
                 
                 
                 // AJOUT DES VOLS : refaire le code sql pour codeigniter ! 
                 
                /*if($type_inter === "vol_confirme"){
                        
                        
                        $arr_size = intval($nb_inter); 
                        $empty_arr = array_fill(0, $arr_size,NULL);
                        
                        foreach($empty_arr as $inter){
                            
                            $req = $this->db->prepare('SELECT max(id) as max_id from intervention');
                            
                            
                            $this->db->select_max('id');
                            $this->db->from('intervention');
                            
                            if($this->db->get_result()){
                                
                                $toto ="PLOGU";
                                var_dump($toto);
                            }
                            
                            if($req->execute()){
                                $last_id = $req->fetch(PDO::FETCH_ASSOC);
                                $this->insertVolConfirme($value, $dayInter, $last_id);
                            }
                            
                        }
                    } */
 
             }
       
         }
    } 
    
    
    public function insertVolConfirme($data_vol, $dayInter, $last_id) {
        
             $data = array(
                 
               'date' => $dayInter,
               'client' => 'a renseigner',
               'adresse' => 'a renseigner',
               'informations' => 'a renseigner',
               'id_tech' => $data_vol['tech_id'],
               'id_intervention' => $last_id['max_id'], 
                        
             );
             $this->db->insert('vol_confirme',$data);
             
    }
    
    
     public function updateInterventions($interventions,$dayInter) {
       
         // récupérer les id des interventions à update

         foreach($interventions as $inter){
             
             $tech_id = $inter['techid'];
             $type_inter = $inter['name'];
             $nb_inter = $inter['value'];
             
             $this->db->from('intervention');
             $this->db->select('*');
             
             $this->db->where('date_intervention',$dayInter);
             $this->db->where('id_technicien',$tech_id);
             $this->db->where('type_intervention', $type_inter);
                     
             $query = $this->db->get( );
                  
             if($query->num_rows() > 0){

                $result = $query->result_array();

                foreach($result as $key => $value){
  
                    $id_inter_to_update = $value['id'];
                    $nb_intervention = $value['nb_intervention'];
                    
                    // si nb inter vaut zero, alors on delete la ligne
                    if($nb_inter == 0){
                        $this->db->delete('intervention',array('id' => $id_inter_to_update));
                    }else {

                        $data = array(
                            'nb_intervention' => $nb_inter
                        );
                        
                        $this->db->where('id', $id_inter_to_update);
                        $this->db->update('intervention',$data);
                        
                      
                    }
                    
                }
                
             }else{
                 
                 // si val interventon différent de 0, on insère .
                 
                 if($nb_inter != 0){
                                 
                    $data = array(
                        'date_intervention' => $dayInter,
                        'type_intervention' => $type_inter,
                        'nb_intervention' => $nb_inter,
                        'id_technicien' => $tech_id,
                        
                    );
                    $this->db->insert('intervention',$data);
                    
                 
             }
         } 
    } 
      }
    
    public function getDayData($day) {
            
        $condition = "date_intervention =" . "'" . $day ."'";
        $this->db->select('*');
        $this->db->from('intervention');
        $this->db->where($condition);
        $day_intervention_data = $this->db->get( )->result_array();
        return $day_intervention_data;     
    }
    
    public function get_plage_interventions($day_start, $day_end){
        
        $this->db->select('*');
        $this->db->from('intervention');
        $this->db->where("date_intervention BETWEEN '$day_start' AND '$day_end'");
        $inter_btw_date = $this->db->get( )->result_array();
        
        return $inter_btw_date;
    }
    
    
    public function get_type_interventions(){
        
        
        $this->db->select('*');
        $this->db->from('type_intervention');
        $types_inter = $this->db->get()->result_array();
        
        return $types_inter;
    }
    
    
    public function get_techniciens(){
        
        $this->db->select('*');
        $this->db->from('technicien');
        $techniciens = $this->db->get()->result_array();
        return $techniciens;
    }
    
    public function get_prestas(){
        
        $this->db->select('*');
        $this->db->from('technicien');
        $this->db->where('statut','prestataire');
        $req->execute();
        $prestas = $this->db->get( )->result_array();
        
        return $prestas;
    }
    
    public function get_cumul_interventions($dayStart, $dayEnd){
       
        $cumul_interventions = array( );
        $types_inter = $this->get_type_interventions( ); 
        $techniciens = $this->get_techniciens( );
        
        // pour chaque type intervention
        foreach($types_inter as $key_type => $type) {
            
            // pour chaque tech
            foreach($techniciens as $key_tech => $technicien) {
                     
                $this->db
                ->select('id_technicien, type_intervention')
                ->select_sum('nb_intervention')
                ->where('id_technicien',$technicien['id'])
                ->where('type_intervention',$type['type'])
                ->where("date_intervention BETWEEN '$dayStart' AND '$dayEnd'")
                ->having('count(*) > 0');
                $query = $this->db->get('intervention');
                
                    if($query->num_rows() > 0){
                        $result =  $query->result_array();
                        
                        if(!empty($result)){
                            array_push($cumul_interventions, $result);
                        }
                
                }
                    
            }
        }
           
            return $cumul_interventions;
    }
    
    
    public function add_tech($tech_data) {
        
        
             $data = array (
                 'date_creation' => $tech_data['date_creation'],
                 'nom' => $tech_data['name'],
                 'prenom' => $tech_data['prename'],
                 'statut' => $tech_data['statut'],
                 'entreprise' => $tech_data['entreprise']
                 
             );
             
         $resp = $this->db->insert('technicien',$data);
             
             return $resp;
    }
    
    
    
    public function add_install($data_install){
        
         $resp = $this->db->insert('tech_install_sav',$data_install);
         
         return $resp;
    }
    
    
    public function get_vols(){

        $this->db->select('*');
        $this->db->from('vol_confirme');
        $vols = $this->db->get( )->result_array();
        return $vols;
    }
    
    
    public function update_vol($vol){
             
        $data = array (
        
            'client' => $vol['volnomClient'],
            'adresse' => $vol['volAdresse'],
            'informations' => $vol['volMsg'],
            'id' => $vol['idVol']
            
        );
        
        
        $this->db->update('vol_confirme',$data);
        
    }
    
    
    public function save_sms($data_sms){

            $data_sql = [
                'emetteur' => $data_sms['select_societe'],
                'type' => $data_sms['select_type'],
                'nom_client' => $data_sms['nom_client'],
                'date_envoi' => date('Y-m-d H:i:s'),
                'date_rdv' => $data_sms['date_rdv'],
                'tel_client' => $data_sms['tel_client'],
                'msg_client' => $data_sms['msg_client']
            ];
            
            $resp = $this->db->insert('archive_sms',$data_sql);
            return $resp;
        
    }
    
    public function get_archive_sms( ){
        $this->db->select('*');
        $this->db->from('archive_sms');
        $vols = $this->db->get( )->result();
        return $vols;
    }
    
    public function get_installs( ){
        
        $this->db->select('*');
        $this->db->from('tech_install_sav');
        $this->db->order_by("date_creation", "desc");
        $installs = $this->db->get( )->result( );
       return $installs;
        
    }
    
    public function get_archives(){
        
    }
    
    public function get_installs_commercial( ){
        
        $query = $this->db->query('select t1.*, t2.nom, t2.prenom FROM tech_install_sav t1 LEFT JOIN users t2 ON t1.commercial = t2.id WHERE t1.statut = "active" ORDER BY t1.date_creation DESC');
        $installs = $query->result();
        return $installs;
    }
    
    
    public function get_archives_commercial(){
        $query = $this->db->query('select t1.*, t2.nom, t2.prenom FROM tech_install_sav t1 LEFT JOIN users t2 ON t1.commercial = t2.id WHERE t1.statut = "archive" ORDER BY t1.date_creation DESC');
        $installs = $query->result();
        return $installs;
    }
    
     public function get_commerciaux( ){

        $val = "commercial";
        $condition = "role =" . "'" . $val . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        //$this->db->limit(1);
        $query = $this->db->get( );
        $commerciaux = $query->result();
        return $commerciaux; 
    }
    
    public function get_install_by_id($id){
        
        $condition = "id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('tech_install_sav');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get( );
        $install = $query->result_object();
        return $install;
    }
    
    
    public function update_install($install,$id){
        
        $query = $this->db->update('tech_install_sav',$install,array('id'=>$id));
        
        return $query;
        
    }
    
    
    public function archive_install($id){
        
        $data = [
            'statut' => 'archive'
        ];
        
        return $this->db->update('tech_install_sav',$data,array('id'=>$id));
        
    }
}
