<?php


/**
 * Controller parent de tous les autres. 
 *
 * @author emmanuel autin
 */
class MY_Controller extends CI_Controller {
    
    public function __construct( )
    {    
        parent::__construct( );
        $this->load->database( );
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $is_home = $this->uri->segment(1); 
        
        
        if($is_home === null || $is_home === 'login' || $is_home === "curl_handler" || $is_home === "api"){
            //do nothing
       
        }else{
            // dans tous les autres cas de non logging = redirect login
            if(!isset($_SESSION['is_logged'])){
               redirect('login', 'index');
            }
        }
        
    }
    
    /*
     * Méthode récupérant le chiffre d'affaire 
     * du mois courant de chaque commerciaux tsip et securicom
     * @param null
     * @return array $data
     */
    protected function data_ca( ){
        
        $CI = &get_instance( );

		$this->load->helper('date');
        
        $CI->load->model('supercommercial_model');
        $commerciaux = $CI->supercommercial_model->get_commerciaux_new( );      
        $current_date = date('Y-m-01');
        $secu_id = COMMERCIAL_ROLE_ID;
        $data['commercial_bilan'] = $CI->supercommercial_model->get_commerciaux_and_ca_by_month_year( $current_date, $secu_id );
        $tsip_id = TSIP_ROLE_ID;
        $data['commercial_tsip'] = $CI->supercommercial_model->get_commerciaux_and_ca_by_month_year( $current_date, $tsip_id );
        $data['year'] = date('Y');
        $data['month'] = date('M');

        $data['last_month'] = date('M', strtotime('first day of last month'));

        $data['full_month_ca'] = $CI->supercommercial_model->get_full_month_ca( );
        return $data;
    }
 
}
