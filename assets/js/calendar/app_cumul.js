
$(function(){


                    
  $('#getDates').click(function(){
      
      
      var dayStart = $('#startDate').val();
      var dayEnd = $('#endDate').val(); 
      getResults(dayStart, dayEnd); 
      
      
  });
                    
     $('#calendar').fullCalendar({
                // put your options and callbacks here
                locale: 'fr',
                showNonCurrentDates: false,
                selectable: true,
                dayClick: function (date) {
                    date = date.format( );
                    
                    console.log(date);
                },
                select: function(dayStart, dayEnd){
                    dayStart = dayStart.format( );
                    dayEnd = dayEnd.subtract('1','days');
                    dayEnd = dayEnd.format();
                    if(dayStart === dayEnd){
                        
                        $('#modalInfo').modal( );

                    }else{
                        
                        getResults(dayStart, dayEnd); 
                          
                        
                    }

                },
                unselect: function(date){
                    
                }  
    });
     
    
    $('#closePopin').click(function(){
        $('#popinDayStart').empty( );
        $('#popinDayEnd').empty( );
        $('#formInjector').empty();
    });
    
    
    function getResults(dayStart, dayEnd){
        
        $.ajax({
                        type: "POST",
                        data: {
                            getInterventions: "récupère les interventions entre deux dates",
                            dayStart: dayStart,
                            dayEnd: dayEnd
                        },
                        url: urlAjax,
                        success: function(response) {
                            
                             $('#popinDayStart').empty( );
                             $('#popinDayEnd').empty( );
                             $('#formInjector').empty();
                             
                            var response = JSON.parse(response);
                            
                            
                            console.log(response);
                                var typeInter = ["sav_intrusion", "sav_incendie", "sav_controle_acces", "sav_video", "sav_agression","sav_suite", "maintenance_intrusion", "maintenance_incendie", "maintenance_controle_acces", "maintenance_video", "maintenance_agression","maintenance_suite", "installation_intrusion", "installation_incendie", "installation_controle_acces", "installation_video", "installation_agression","installation_suite"];

                    
                                // pour chaque tech
                                $.each(response.techniciens, function(cle, tech){
                                    
                                    var row = "<tr>";
                                    var name = tech.nom;
                                    var prename = tech.prenom;
  
                                    console.log(tech);
                                    
                                    row += "<td>"+name+" "+prename+"</td>";
                                    
                                    var store = [
        '<td><input type="number" data-id="" name="sav_intrusion" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="sav_incendie" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="sav_controle_acces" value="0" class="form-control" disabled  min="0"></td>',
        '<td><input type="number" data-id="" name="sav_video" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="sav_agression" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="sav_suite" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="maintenance_intrusion" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="maintenance_incendie" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="maintenance_controle_acces" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="maintenance_video" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="maintenance_agression" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="maintenance_suite" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="installation_intrusion" value="0" class="form-control"disabled  min="0"></td>',
        '<td><input type="number" data-id="" name="installation_incendie" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="installation_controle_acces" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="installation_video" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="installation_agression" value="0" class="form-control" disabled min="0"></td>',
        '<td><input type="number" data-id="" name="installation_suite" value="0" class="form-control" disabled min="0"></td>'];
                                        
                                        // chaque type d'inter 
                                        $.each(typeInter, function(cle, val){
                                                
                                                   $.each(response.interventions, function(key, inter){
                                                       
                                                       console.log(inter);
                                                       if(val === inter.type_intervention && tech.id === inter.id_technicien){
                                                           store[cle] = '<td><input type="number" data-id="'+inter.id_technicien+'" name="'+val+'" value="'+inter.nb_intervention+'" class="form-control" disabled min="0"></td>';
                                                       }
                                                   });
                                        });
                                        
                                        row += store[0]+store[1]+store[2]+store[3]+store[4]+store[5]+store[6]+store[7]+store[8]+store[9]+store[10]+store[11]+store[12]+store[13]+store[14]+store[15]+store[16]+store[17];
                                        row += '</tr>';
                                        $('#formInjector').append(row);
                                 
                                });
                                
   
                            
                            $('#modalCumul').modal( );
                            $('#popinDayStart').html(moment(dayStart).format('dddd Do MMMM YYYY'));
                            $('#popinDayEnd').html(moment(dayEnd).format('dddd Do MMMM YYYY'));
                            
                        },
                        error: function(err){
                            
                            console.log(err);
                        }

                        });  
        
    }
    
    });
    