$(function( ){ 
    
    
   
    
    populateListVols( ); 
    
    function populateListVols(){
        
        var listVol = $('#listVol');
         $.ajax({ 
                  type: "POST",
                  data: {
                      getallvols: "get all vols"
                  },
                  url: urlAjax,
                  success: function(vols) {
                     
                    console.log(vols);
                      
                    var vols = $.parseJSON(vols);
 
                    var volListDOM = "";
                      
                    $.each(vols, function(key,volCollection) {
                        
                        $.each(volCollection, function(kk, vol){
                            
                            var dateFR = moment(vol.date).format('dddd Do MMMM YYYY');
                            volListDOM +="<tr>";
                            volListDOM += "<td>"+vol.id+"</td>";
                            volListDOM += "<td>"+dateFR+"</td>";
                            volListDOM += "<td>"+vol.client+"</td>";
                            volListDOM += "<td>"+vol.adresse+"</td>";
                            volListDOM += "<td>"+vol.informations+"</td>";
                            volListDOM += "<td>"+vol.id_tech+"</td>";
                            volListDOM += "<td><a class='edit-vol btn btn-primary btn-sm' data-id='"+vol.id+"'>Mettre à jour les informations</a></td>";
                            volListDOM += "</tr>";
                            
                        });
                        
                    }); 
                    
                   
                      
                      $msg = '<div class="alert alert-success"><strong>Prestataire ajouté avec succès</strong></div>';
                  
                      
                      listVol.append(volListDOM);
                    
                  },
                  error: function(err){
                      
                      console.log(err);
                      $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                      containerResp.append($msg);
                  }

              });
        
    }
    
    
            $(document).on('click','.edit-vol',function(){
                
                var idVol = $(this).data('id');
                $('#modalUpdateVol #volID').val(idVol);
                $('#modalUpdateVol').modal();
                
            });
    
            $('#updateVol').click(function( ){
                
                
                var idVol = $("#volID").val( );
                var volnomClient = $("#volnomClient").val( );
                var volAdresse = $("#volAdresse").val( );
                var volMsg = $("#volMsg").val( );
                var data = {idVol,volnomClient,volAdresse,volMsg};
                
                if(idVol !== "" && volnomClient !== "" && volAdresse !== "" && volMsg !== ""){
                      $.ajax({ 
                  type: "POST",
                  data: {
                      updatevol: "update vol",
                      data: data
                  },
                  url: urlAjax,
                  success: function(vols) {
                     
                    console.log(vols);
                    
                    var listVol = $('#listVol');
                    var respContainer = $('#modal-resp-vol');
                    listVol.empty();
                    respContainer.empty();
                    var respVol = $('#modal-resp-vol');
                     
                    console.log(vols);
                      
                    var vols = $.parseJSON(vols);
                    var volListDOM = "";
                      
                    $.each(vols, function(key,volCollection) {
                        
                        $.each(volCollection, function(kk, vol){
                            
                            var dateFR = moment(vol.date).format('dddd Do MMMM YYYY');
                            volListDOM +="<tr>";
                            volListDOM += "<td>"+vol.id+"</td>";
                            volListDOM += "<td>"+dateFR+"</td>";
                            volListDOM += "<td>"+vol.client+"</td>";
                            volListDOM += "<td>"+vol.adresse+"</td>";
                            volListDOM += "<td>"+vol.informations+"</td>";
                            volListDOM += "<td>"+vol.id_tech+"</td>";
                            volListDOM += "<td><a class='edit-vol btn btn-primary btn-sm' data-id='"+vol.id+"'>Mettre à jour les informations</a></td>";
                            volListDOM += "</tr>";
                            
                        });
                        
                    }); 
                    
                   
                      
                      $msg = '<div class="alert alert-success"><strong>intervention vol confirmé mise à jour avec succès</strong></div>';
                  
                      
                      listVol.append(volListDOM);
                      respVol.append($msg);
                    
                  },
                  error: function(err){
                      
                      console.log(err);
                      $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                      respVol.append($msg);
                  }

              })
              
                }else
                {
                     $msg = '<div class="alert alert-danger"><strong>Veuillez remplir tous les champs</strong></div>';
                     $('#modal-resp-vol').append($msg);
                }
                
                
                
              
        
            });
            
            
            
});