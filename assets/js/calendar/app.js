$(function() {


     
     
      var currDate = {
          day: ""
      };

    function buildTechRow(name, prename, techId, interventions) {
    
        var typeInter = ["sav_intrusion", "sav_incendie", "sav_controle_acces", "sav_video", "sav_agression","sav_suite", "maintenance_intrusion", "maintenance_incendie", "maintenance_controle_acces", "maintenance_video", "maintenance_agression","maintenance_suite", "installation_intrusion", "installation_incendie", "installation_controle_acces", "installation_video", "installation_agression","installation_suite"];
        var row = "<tr>";
        var name = name;
        var prename = prename; 
        row += "<td>"+name+" "+prename+"</td>";
        var store = [
            '<td><input type="number" data-id="'+techId+'" name="sav_intrusion" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="sav_incendie" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="sav_controle_acces" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="sav_video" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="sav_agression" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="sav_suite" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="maintenance_intrusion" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="maintenance_incendie" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="maintenance_controle_acces" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="maintenance_video" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="maintenance_agression" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="maintenance_suite" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="installation_intrusion" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="installation_incendie" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="installation_controle_acces" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="installation_video" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="installation_agression" value="0" class="form-control" min="0"></td>',
            '<td><input type="number" data-id="'+techId+'" name="installation_suite" value="0" class="form-control" min="0"></td>'

        ];
    $.each(typeInter,function(cle,val){

        interventions.forEach(function(arrayItem){
            if(arrayItem.inter == val){
               store[cle] = '<td><input type="number" data-id="'+techId+'" name="'+arrayItem.inter+'" value="'+arrayItem.nb_intervention+'" class="form-control" min="0"></td>';
            }
   
        });

    });
    
    
    row += store[0]+store[1]+store[2]+store[3]+store[4]+store[5]+store[6]+store[7]+store[8]+store[9]+store[10]+store[11]+store[12]+store[13]+store[14]+store[15]+store[16]+store[17]+store[18];
    row += "</tr>";
    
    return row;
    
}
      
  
      if ($('#calendar').length != 0) {

          $('#calendar').fullCalendar({
              // put your options and callbacks here
              locale: 'fr',
              dayClick: function(date) {

                  var date = date.format();
                  currDate.day = date;
                  $('#formInjector').empty();
                  $('#formInjectorEdit').empty();
                  $('#modal-resp-edit-success').hide();
                  $('#modal-resp-edit-success').empty();
                  $('#modal-resp-saisie-success').hide();
                  $('#modal-resp-saisie-success').empty();
                  $('#modal-resp-saisie-success').empty();
                  $('#saveEditDate').show();
                  $('#saveNewDate').show();
                  $('#dynamicDayEdit').attr('data-day', '');
                  $('#dynamicDay').attr('data-day', '');
                  $('#dynamicDayEdit').attr('data-day', date);
                  $('#dynamicDay').attr('data-day', date);
                  // console.log("date envoyée : "+ date);

                  $.ajax({
                      type: "POST",
                      data: {
                          testAjax: "verification si la date a deja ete saisie",
                          date: date
                      },
                      url: urlAjax,
                      success: function(data) {
                          try {
                              var response = JSON.parse(data)
                          } catch (e) {
                              console.error(e);
                              console.error('JSON recived :', data)
                          }
                          
                          console.log(response);
                          
                          var dateFR = moment(response.date).format('dddd Do MMMM YYYY');

                          console.log(response);

                          // date deja saisie
                          if (response.statut === true) {

                              // alert(response.date);

                              $('#dynamicDayEdit').html(dateFR);
                              //$('#dynamicDayEdit').attr('data-day', response.date);

                              var formHTMLEdit = "";
                              $.each(response.tech_data, function(key, technicien) {

                                  //pour chaque tech on construit la ligne
                                  // on rÃ©cupÃ©re les interventions du tech:

                                  var name = technicien.nom;
                                  var prename = technicien.prenom;
                                  var interventionTech = [];

                                  $.each(response.tech_inter, function(key, intervention) {
                                      if (intervention.id_technicien == technicien.id) {
                                          var inter = intervention.type_intervention;
                                          var nb_intervention = intervention.nb_intervention;
                                          interventionTech.push({
                                              inter,
                                              nb_intervention
                                          });
                                      }
                                  });

                                  var techId = technicien.id;
                                  var row = buildTechRow(name, prename, techId, interventionTech);
                                  
                                
                                  $('#formInjectorEdit').append(row);
                              });

                              //$('#formInjectorEdit').append(formHTMLEdit);
                              $('#modalEdit').modal();


                          } else {

                              // Mode new day

                              $('#dynamicDay').html(dateFR);

                              //alert("reponse date : "+response.date);
                              // $('#dynamicDay').attr('data-day', response.date);

                              // on réaffiche les boutons etc et on cache la div reponse :
                              $('#modal-resp-saisie-success').empty();
                              $('#formInjector').empty();

                              $('#saveNewDate').show();

                              //console.log('key'+key+' value '+value.id);

                              var formHTML = '';

                               $.each(response.tech_data, function (key, value) {

                                    formHTML += '<tr>';
                                    formHTML += '<td>' + value.nom + " " + value.prenom + '</td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_intrusion" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_incendie" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_controle_acces" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_video" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_agression" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_suite" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_intrusion" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_incendie" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_controle_acces" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_video" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_agression" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_suite" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_intrusion" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_incendie" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_controle_acces" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_video" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_agression" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_suite" class="form-control" value="0" min="0"></td>';
                                    formHTML += '</tr>';
                                });

                              $('#formInjector').append(formHTML);
                              $('#modalSaisie').modal();

                          }

                      }

                  });

                  //if rempli on affiche la popup table prÃ©remplie avec les anciennes valeurs. 

                  //if pas rempli on affiche la popup table

              }

          })


          $('#saveEditDate').click(function() {

              //var currDay = $('#dynamicDayEdit').data('day');
              var currDay = currDate.day;

              // alert("save edit currDay : "+currDay);

              $inputs = $('[type=number]');

              var interventions = $inputs.map(function() {

                  var val = $(this).val();


                  return {

                      techid: $(this).data('id'),
                      name: $(this).attr('name'),
                      value: $(this).val()
                  }

              }).get();

              $.ajax({
                  type: "POST",
                  data: {
                      saveeditedinformation: "update des interventions en bdd",
                      intervention: interventions,
                      date: currDay
                  },
                  url: urlAjax,
                  success: function(data) {

                      // console.log(data);

                      //$('#formInjector').empty();
                      //$('#formInjectorEdit').empty();

                      $success = '<div class="alert alert-success"><strong>Mise à jour des interventions sauvegardés.</strong></div>';
                      // console.log(data);
                      $('#modal-resp-edit-success').append($success);
                      $('#modal-resp-edit-success').show();

                      //$('#formInjector').empty();

                      $('#saveEditDate').hide();
                      $('#dynamicDayEdit').attr('data-day', '');
                      $('#dynamicDay').attr('data-day', '');


                  }


              });


          });

          $('#saveNewDate').click(function() {

              //get curr date :
              //var currDay = $('#dynamicDay').data('day');
              var currDay = currDate.day;
              // alert("save new date currDay : "+currDay);

              //rÃ©cupÃ©rer tous les data-id
              $inputs = $('[type=number]');

              var interventions = $inputs.map(function() {

                  var val = $(this).val();
                  if (val != 0) {

                      return {

                          techid: $(this).data('id'),
                          name: $(this).attr('name'),
                          value: $(this).val()
                      }
                  }
              }).get();


              $.ajax({
                  type: "POST",
                  data: {
                      savenewintervention: "envoi des interventions en bdd",
                      intervention: interventions,
                      date: currDay
                  },
                  url: urlAjax,
                  success: function(data) {
                      
                      console.log(data);
                      $success = '<div class="alert alert-success"><strong>Succès de l\'enregistrement !</strong></div>';
                      // console.log(data);
                      $('#modal-resp-saisie-success').append($success);
                      $('#modal-resp-saisie-success').show();
                      //$('#formInjector').empty();
                      //$('#formInjectorEdit').empty();
                      $('#saveNewDate').hide();
                      $('#dynamicDayEdit').attr('data-day', '');
                      $('#dynamicDay').attr('data-day', '');


                  }

              });

          });

      }

     /* $('#addPresta').click(function() {
           
         $('.alert').hide();
         
         var namePresta = $('#name_presta').val();
         var msgPresta = $('#comment_presta').val();    
         var containerResp = $("#resp_presta");   
        // var listPresta = $('#listPresta');
        // $('#listPresta').empty( );
         
         $.ajax({ 
                  type: "POST",
                  data: {
                      addpresta: "update des interventions en bdd",
                      prestaname: namePresta,
                      prestamsg: msgPresta
                  },
                  url: urlAjax,
                  success: function(data) {
                      
                      console.log(data);
                      if(data != false){
                         $msg = '<div class="alert alert-success"><strong>Prestataire ajouté avec succès</strong></div>';
                         containerResp.append($msg);
                      //   listPresta.append(populateListPresta()); 
                      }else{
                          $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                          containerResp.append($msg);
                      }
                      
                  },
                  error:function(err){
                      
                      console.log(err);
                      $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                      containerResp.append($msg);
                  }

              });
              
      }); */
      
      
       $('#addTech').click(function() {
         
        
         $('.alert').hide();
         
         var nameTech = $('#name_tech').val();
         var prenametech = $('#prename_tech').val();
         var statutTech = $('#statut_tech').val();
         var entrepriseTech = $('#entreprise_tech').val();
         var containerResp = $("#resp_tech");   
         var listTech = $('#listTech');
         listTech.empty( );
         
            if(nameTech !== "" && prenametech !== ""){

                 $.ajax({ 
                  type: "POST",
                  data: {
                      addtech: "add tech in bdd",
                      techname: nameTech,
                      techprename: prenametech,
                      statut : statutTech,
                      entreprise: entrepriseTech
                  },
                  url: urlAjax,
                  success: function(data) {
                      
                      console.log(data);
                      if(data != false){
                         $msg = '<div class="alert alert-success"><strong>Technicien ajouté avec succès</strong></div>';
                         containerResp.append($msg);
                       listTech.append(populateListTech()); 
                      }else{
                          $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde en base du technicien</strong></div>';
                          containerResp.append($msg);
                      }
                      
                  },
                  error:function(err){
                      
                      console.log(err);
                      $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                      containerResp.append($msg);
                  }

              });
              
            }else{
                      $msg = '<div class="alert alert-danger"><strong>Veuillez remplir tous les champs</strong></div>';
                      containerResp.append($msg);
            }
         
        
              
      });
      
      
      /*if($('#listPresta').length > 0){
          
          $('#listPresta').empty();
          
          populateListPresta();
          
      }*/
      
      
      if($('#listTech').length > 0){
          
          
          $('#listTech').empty();
          
          populateListTech();
      }
     
     
     
     function populateListTech(){
         
         var listTech = $('#listTech');
         var containerResp = $("#resp_tech"); 
         
         $.ajax({ 
                  type: "POST",
                  data: {
                      getalltech: "recupere les techniciens"
                  },
                  url: urlAjax,
                  success: function(techniciens) {
                      
                    var techniciens = $.parseJSON(techniciens);
                   
                    var techListDOM = "";
                      
                    $.each(techniciens, function(key,technicienCollection) {
                        
                        $.each(technicienCollection, function(kk, technicien){
                            
                            var dateFR = moment(technicien.date_creation).format('dddd Do MMMM YYYY');
                            techListDOM +="<tr>";
                            techListDOM += "<td>"+technicien.id+"</td>";
                            techListDOM += "<td>"+dateFR+"</td>";
                            techListDOM += "<td>"+technicien.nom+"</td>";
                            techListDOM += "<td>"+technicien.prenom+"</td>";
                            techListDOM += "<td>"+technicien.statut+"</td>";
                            techListDOM += "<td>"+technicien.entreprise+"</td>";
                            techListDOM += "</tr>";
                            
                        });
                        
                    }); 
                    
                   
                      
                      $msg = '<div class="alert alert-success"><strong>Prestataire ajouté avec succès</strong></div>';
                  
                      
                      listTech.append(techListDOM);
                    
                  },
                  error: function(err){
                      
                      console.log(err);
                      $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                      containerResp.append($msg);
                  }

              });
              
         
     }
     
      /*function populateListPresta() {
          
          var listPresta = $('#listPresta');
          var containerResp = $("#resp_presta"); 
          $.ajax({ 
                  type: "POST",
                  data: {
                      getallpresta: "recupere les prestataires"
                  },
                  url: urlAjax,
                  success: function(prestataires) {
                      
                    var prestataires = $.parseJSON(prestataires);
                    console.log(prestataires);
                    var prestaListDOM = "";
                      
                    $.each(prestataires, function(key,prestataireCollection) {
                        
                        $.each(prestataireCollection, function(kk, prestataire){
                            
                            prestaListDOM +="<tr>";
                            prestaListDOM += "<td>"+prestataire.id+"</td>";
                            prestaListDOM += "<td>"+prestataire.nom+"</td>";
                            prestaListDOM += "<td>"+prestataire.message+"</td>";
                            prestaListDOM += "</tr>";
                            
                        });
                        
                    }); 
                    
                   
                      
                      $msg = '<div class="alert alert-success"><strong>Prestataire ajouté avec succès</strong></div>';
                  
                      
                      listPresta.append(prestaListDOM);
                    
                  },
                  error: function(err){
                      
                      console.log(err);
                      $msg = '<div class="alert alert-danger"><strong>Un problème est survenue lors de la sauvegarde</strong></div>';
                      containerResp.append($msg);
                  }

              });

      }*/
     
  });