$(function( ){
     
      urlAjax = 'ajax_save_ca';
      
      console.log(base_url);
      
      $('.alert').hide(); 
     
      
       $('#save_ca_month').click(function( e ) {
          
          $('.alert').find('p').empty();
          $('.alert').hide(); 
          var data = [];
          var date = $('input[name="month_year"]').val( );
          
         $("div[data-id]").each(function(){
         
             console.log($(this).data('id'));
             var id = $(this).data('id');
             var total_loc = $(this).find('input[name="total_loc"]').val( );
             var total_vente = $(this).find('input[name="total_vente"]').val( );
             var total_devis = $(this).find('input[name="total_devis"]').val( );
             
            console.log(total_loc);
            data.push({id:id,total_loc:total_loc,total_vente:total_vente,total_devis:total_devis});
             
         });
         
         
          e.preventDefault( );
            
                 $.ajax({ 
                  type: "POST",
                  data: {
                      chiffre: data,
                      date: date
                  },
                  url: base_url+'index.php/supercommercial/'+urlAjax,
                  success: function(data) {
                      
                      console.log(data);
                      $('#secu .alert-success').show( ).find('p').append(JSON.parse(data));
                      
                      
                  },
                  error:function(err){
                      
                      console.log(err);
                      $('#secu .alert-danger').show( ).find('p').append(JSON.parse(err));
                  }

              });
      });
      
      
      
          $('#save_ca_month_tsip').click(function( e ) {
          
          $('#tsip .alert').find('p').empty();
          $('#tsip .alert').hide(); 
          var data = [];
          var date = $('input[name="month_year"]').val( );
          
         $("#tsip div[data-id]").each(function(){
         
             console.log($(this).data('id'));
             var id = $(this).data('id');
             var total_loc = $(this).find('input[name="total_loc"]').val( );
             var total_vente = $(this).find('input[name="total_vente"]').val( );
             var total_devis = $(this).find('input[name="total_devis"]').val( );
             
            console.log(total_loc);
            data.push({id:id,total_loc:total_loc,total_vente:total_vente,total_devis:total_devis});
             
         });
         
         
          e.preventDefault( );
            
                 $.ajax({ 
                  type: "POST",
                  data: {
                      chiffre: data,
                      date: date
                  },
                  url: base_url+'index.php/supercommercial/ajax_save_ca_tsip',
                  success: function(data) {
                      
                      console.log(data);
                      $('#tsip .alert-success').show( ).find('p').append(JSON.parse(data));
                      
                      
                  },
                  error:function(err){
                      
                      console.log(err);
                      $('#tsip .alert-danger').show( ).find('p').append(JSON.parse(err));
                  }

              });
      });
      
      
      
      });